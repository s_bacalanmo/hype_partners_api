module.exports = {
  transpileDependencies: [
    'vuetify'
  ],

  pluginOptions: {
    apollo: {
      enableMocks: false,
      enableEngine: true,
      lintGQL: false,
      typescript: false,
      serverFolder: './apollo-server',
      cors: '*'
    }
  },
  chainWebpack: config => {
    config.module.rules.delete('eslint')
  }
}
