import axios from 'axios'
import querystring from 'querystring'

export const Http = axios.create({
  baseURL: process.env.VUE_APP_URL
})

Http.interceptors.request.use(function (config) {
  const token = window.localStorage.getItem('apollo-token')

  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

export const getApi = function (url, filters) {
  if (filters) {
    url = `${url}?${querystring.stringify(filters)}`
  }
  return new Promise((resolve, reject) => {
    Http.get(url)
      .then(response => {
        resolve(response.data)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export const asyncDeleteApi = async (url, payload) => {
  return Http.delete(url, payload)
}

export const asyncPostApi = async (url, payload) => {
  return Http.post(url, payload)
}

export const asyncPatchApi = async (url, payload) => {
  return Http.patch(url, payload)
}
