import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import '@fortawesome/fontawesome-free/css/all.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import { Scroll } from 'vuetify/lib/directives'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'mdi' || 'fa' || 'md'
  },
  theme: {
    themes: {
      light: {
        // primary: '#F47927',
        accent: '#F47927',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
        main: '#446597'
      }
    }
  },
  directives: {
    Scroll
  }
})
