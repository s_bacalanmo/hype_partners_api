// ----------------- filters ---------------------------------------------------------------------
const RangeSlider = () => import(/* webpackChunkName: "range-slider" */ '@/components/filters/RangeSlider.vue')
const Steps = () => import(/* webpackChunkName: "steps" */ '@/components/applicants/dashboard/Steps.vue')
const ViewPrograms = () => import(/* webpackChunkName: "view-programs" */ '@/components/applicants/dashboard/ViewPrograms.vue')
const ViewSemester = () => import(/* webpackChunkName: "view-semester" */ '@/components/applicants/dashboard/ViewSemester.vue')
// ----------------- enrollment ---------------------------------------------------------------------
const CareerInfo = () => import(/* webpackChunkName: "career-info" */ '@/components/applicants/enrollment/personal/CareerInfo.vue')
const ChecklistInfo = () => import(/* webpackChunkName: "checklist-info" */ '@/components/applicants/enrollment/personal/ChecklistInfo.vue')
const EducationalData = () => import(/* webpackChunkName: "educational-data" */ '@/components/applicants/enrollment/education/EducationalData.vue')
const EntranceInfo = () => import(/* webpackChunkName: "entrance-info" */ '@/components/applicants/enrollment/EntranceInfo.vue')
const EnrollmentInfo = () => import(/* webpackChunkName: "enrollment-info" */ '@/components/applicants/enrollment/EnrollmentInfo.vue')
const FathersInfo = () => import(/* webpackChunkName: "fathers-info" */ '@/components/applicants/enrollment/family/FathersInfo.vue')
const MothersInfo = () => import(/* webpackChunkName: "mothers-info" */ '@/components/applicants/enrollment/family/MothersInfo.vue')
const GuardiansInfo = () => import(/* webpackChunkName: "guardians-info" */ '@/components/applicants/enrollment/family/GuardiansInfo.vue')
const CurrentCourses = () => import(/* webpackChunkName: "current-courses" */ '@/components/applicants/enrollment/courses/CurrentCourses.vue')
const EntrancePayment = () => import(/* webpackChunkName: "entrance-payment" */ '@/components/applicants/enrollment/payment/EntrancePayment.vue')
const EnrollmentPayment = () => import(/* webpackChunkName: "enrollment-payment" */ '@/components/applicants/enrollment/payment/EnrollmentPayment.vue')
// ----------------- applicants ---------------------------------------------------------------------
const ApplicantsTable = () => import(/* webpackChunkName: "applicants-table" */ '@/components/applicants/ApplicantsTable.vue')
const BasicInfo = () => import(/* webpackChunkName: "basic-info" */ '@/components/applicants/BasicInfo.vue')
const SpdiTab = () => import(/* webpackChunkName: "spdi-tab" */ '@/components/applicants/SpdiTab.vue')
const RequirementsTab = () => import(/* webpackChunkName: "requirements-tab" */ '@/components/applicants/RequirementsTab.vue')
const CoursesTab = () => import(/* webpackChunkName: "courses-tab" */ '@/components/applicants/CoursesTab.vue')
const EvaluationTab = () => import(/* webpackChunkName: "evaluation-tab" */ '@/components/applicants/EvaluationTab.vue')
const PersonalData = () => import(/* webpackChunkName: "personal-data" */ '@/components/applicants/enrollment/personal/PersonalData.vue')
// ----------------- class ---------------------------------------------------------------------
const ClassTable = () => import(/* webpackChunkName: "class-table" */ '@/components/admin/class/ClassTable.vue')
const ClassDialog = () => import(/* webpackChunkName: "class-dialog" */ '@/components/admin/class/ClassDialog.vue')
const SectionDialog = () => import(/* webpackChunkName: "section-dialog" */ '@/components/admin/class/SectionDialog.vue')
const TeacherDialog = () => import(/* webpackChunkName: "teacer-dialog" */ '@/components/admin/class/TeacherDialog.vue')
const RoomDialog = () => import(/* webpackChunkName: "room-dialog" */ '@/components/admin/class/RoomDialog.vue')
const Career = () => import(/* webpackChunkName: "career" */ '@/components/applicants/enrollment/personal/Career.vue')
const Checklist = () => import(/* webpackChunkName: "checklist" */ '@/components/applicants/enrollment/personal/Checklist.vue')
const FamilyData = () => import(/* webpackChunkName: "family-data" */ '@/components/applicants/enrollment/family/FamilyData.vue')
// ----------------- student ---------------------------------------------------------------------
const StudentsTable = () => import(/* webpackChunkName: "students-table" */ '@/components/students/StudentsTable.vue')
const StudentsDialog = () => import(/* webpackChunkName: "students-dialog" */ '@/components/students/StudentsDialog.vue')
const AddAndDropTab = () => import(/* webpackChunkName: "add-and-drop-tab" */ '@/components/students/AddAndDropTab.vue')
const ResourcesTab = () => import(/* webpackChunkName: "resources-tab" */ '@/components/students/ResourcesTab.vue')
const StudentHistory = () => import(/* webpackChunkName: "student-history" */ '@/components/students/StudentHistory.vue')
// ----------------- programs ---------------------------------------------------------------------
const ProgramDialog = () => import(/* webpackChunkName: "program-dialog" */ '@/components/admin/program/ProgramDialog.vue')
// ----------------- admission ---------------------------------------------------------------------
const AdmissionTabs = () => import(/* webpackChunkName: "admission-applicants-tabs" */ '@/components/admission/AdmissionTabs.vue')
// ----------------- clinic ---------------------------------------------------------------------
const ClinicTabs = () => import(/* webpackChunkName: "clinic-tabs" */ '@/components/clinic/ClinicTabs.vue')
const AdminTabs = () => import(/* webpackChunkName: "admin-tabs" */ '@/components/admin/AdminTabs.vue')
// ----------------- department ---------------------------------------------------------------------
const DepartmentTabs = () => import(/* webpackChunkName: "department-tabs" */ '@/components/department/DepartmentTabs.vue')
const SubjectEquivalencyTab = () => import(/* webpackChunkName: "subject-equivalency-tab" */ '@/components/department/SubjectEquivalencyTab.vue')
// ----------------- registrar ---------------------------------------------------------------------
const RegistrarTabs = () => import(/* webpackChunkName: "registrar-tabs" */ '@/components/registrar/RegistrarTabs.vue')
const SubjectOverloadTab = () => import(/* webpackChunkName: "subject-overload-tabs" */ '@/components/registrar/SubjectOverloadTab.vue')
const ExamScheduleDialog = () => import(/* webpackChunkName: "exam-schedule-dialog" */ '@/components/registrar/examination/ExamScheduleDialog.vue')
// ----------------- teacher ---------------------------------------------------------------------
const ClassListGrades = () => import(/* webpackChunkName: "class-list-grades" */ '@/components/teacher/ClassListGrades.vue')
// ----------------- admin ---------------------------------------------------------------------
const CourseDialog = () => import(/* webpackChunkName: "course-dialog" */ '@/components/admin/curriculum/CourseDialog.vue')
const CourseMenuDialog = () => import(/* webpackChunkName: "course-menu-dialog" */ '@/components/admin/course/CourseMenuDialog.vue')
const CourseTable = () => import(/* webpackChunkName: "course-table" */ '@/components/admin/curriculum/CourseTable.vue')
const UserDialog = () => import(/* webpackChunkName: "user-dialog" */ '@/components/admin/user/UserDialog.vue')
const AssignSection = () => import(/* webpackChunkName: "assign-section" */ '@/components/admin/AssignSection.vue')
const ProgramCurriculumDialog = () => import(/* webpackChunkName: "program-curriculum-dialog" */ '@/components/admin/program/ProgramCurriculumDialog.vue')
const ProgramRevisionDialog = () => import(/* webpackChunkName: "program-revision-dialog" */ '@/components/admin/program/ProgramRevisionDialog.vue')
const SchoolYearDialog = () => import(/* webpackChunkName: "school-year-dialog" */ '@/components/admin/schoolYear/SchoolYearDialog.vue')
const OpenCurriculumDialog = () => import(/* webpackChunkName: "open-curriculum-dialog" */ '@/components/admin/schoolYear/OpenCurriculumDialog.vue')
// ----------------- finance ---------------------------------------------------------------------
const FinanceTuitionDialog = () => import(/* webpackChunkName: "finance-tuition-dialog" */ '@/components/finance/fees/FinanceTuitionDialog.vue')
const FinanceFeeDialog = () => import(/* webpackChunkName: "finance-dialog" */ '@/components/finance/fees/FinanceFeeDialog.vue')
const FinanceDiscountDialog = () => import(/* webpackChunkName: "finance-discount-dialog" */ '@/components/finance/FinanceDiscountDialog.vue')
const FinancePayment = () => import(/* webpackChunkName: "finance-payment" */ '@/components/finance/FinancePayment.vue')
const FinanceAssessment = () => import(/* webpackChunkName: "finance-assessment" */ '@/components/finance/FinanceAssessment.vue')
const FinanceExportDialog = () => import(/* webpackChunkName: "finance-export-dialog" */ '@/components/finance/FinanceExportDialog.vue')
const FinanceCharges = () => import(/* webpackChunkName: "finance-charges" */ '@/components/finance/FinanceCharges.vue')
const FinanceDiscountTab = () => import(/* webpackChunkName: "finance-discount-tab" */ '@/components/finance/FinanceDiscountTab.vue')
const FinanceHistory = () => import(/* webpackChunkName: "finance-history" */ '@/components/finance/FinanceHistory.vue')
const FinanceCourses = () => import(/* webpackChunkName: "finance-courses" */ '@/components/finance/history/Courses.vue')
const FinanceDiscounts = () => import(/* webpackChunkName: "finance-discounts" */ '@/components/finance/history/Discounts.vue')
const FinanceOtherCharges = () => import(/* webpackChunkName: "finance-other-charges" */ '@/components/finance/history/OtherCharges.vue')
const FinanceHistoryPayments = () => import(/* webpackChunkName: "finance-payments" */ '@/components/finance/history/Payments.vue')
const FinancePaymentExport = () => import(/* webpackChunkName: "finance-payment-export" */ '@/components/finance/FinancePaymentExport.vue')
const FinanceCodesDialog = () => import(/* webpackChunkName: "finance-codes-dialog" */ '@/components/finance/FinanceCodesDialog.vue')
const FinanceFeeCopy = () => import(/* webpackChunkName: "finance-fee-copy" */ '@/components/finance/fees/FinanceFeeCopy.vue')
// ----------------- dashboard ---------------------------------------------------------------------
const StudentsPopulation = () => import(/* webpackChunkName: "students-population" */ '@/components/dashboard/statistics/StudentsPopulation.vue')
const StudentsPerProgram = () => import(/* webpackChunkName: "students-per-program" */ '@/components/dashboard/statistics/StudentsPerProgram.vue')
const AutoLogout = () => import(/* webpackChunkName: "students-per-program" */ '@/components/AutoLogout.vue')

export default {
  AddAndDropTab,
  AdmissionTabs,
  AutoLogout,
  ApplicantsTable,
  AdminTabs,
  AssignSection,
  BasicInfo,
  Career,
  CareerInfo,
  Checklist,
  ChecklistInfo,
  ClassDialog,
  ClassListGrades,
  ClassTable,
  ClinicTabs,
  CoursesTab,
  CurrentCourses,
  CourseDialog,
  CourseMenuDialog,
  CourseTable,
  DepartmentTabs,
  EducationalData,
  EnrollmentInfo,
  EntranceInfo,
  EntrancePayment,
  EnrollmentPayment,
  EvaluationTab,
  ExamScheduleDialog,
  FamilyData,
  FathersInfo,
  FinanceAssessment,
  FinanceCharges,
  FinanceCourses,
  FinanceCodesDialog,
  FinanceFeeCopy,
  FinanceFeeDialog,
  FinanceDiscounts,
  FinanceDiscountTab,
  FinanceDiscountDialog,
  FinanceExportDialog,
  FinanceHistory,
  FinanceHistoryPayments,
  FinanceOtherCharges,
  FinancePayment,
  FinancePaymentExport,
  FinanceTuitionDialog,
  GuardiansInfo,
  MothersInfo,
  OpenCurriculumDialog,
  PersonalData,
  ProgramRevisionDialog,
  ProgramCurriculumDialog,
  ProgramDialog,
  RangeSlider,
  RegistrarTabs,
  RequirementsTab,
  ResourcesTab,
  RoomDialog,
  SpdiTab,
  SectionDialog,
  SchoolYearDialog,
  Steps,
  StudentsDialog,
  StudentHistory,
  StudentsPopulation,
  StudentsPerProgram,
  StudentsTable,
  SubjectEquivalencyTab,
  SubjectOverloadTab,
  TeacherDialog,
  UserDialog,
  ViewPrograms,
  ViewSemester
}
