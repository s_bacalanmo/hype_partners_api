import Vue from 'vue'
import App from './App.vue'
import { createRouter } from './router'
import { createStore } from './store'
import { createProvider } from './vue-apollo'
import vuetify from './plugins/vuetify'
import Croppa from 'vue-croppa'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import Components from '@/components/index'
import VueEsc from 'vue-esc'

import {
  uppercase,
  capitalize,
  initials,
  date
} from './formats'

// 3rd party library
import 'chart.js'

Object.keys(Components).forEach(key => {
  Vue.component(key, Components[key])
})

const requireComponent = require.context(
  // The relative path of the components folder
  './components/shared',
  // Whether or not to look in subfolders
  false,
  // The regular expression used to match base component filenames
  /[A-Z]\w+\.(vue|js)$/
)
requireComponent.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireComponent(fileName)

  // Get PascalCase name of component
  const componentName = upperFirst(
    camelCase(
      // Strip the leading `./` and extension from the filename
      fileName.replace(/^\.\/(.*)\.\w+$/, '$1')
    )
  )
  // Register component globally
  Vue.component(
    componentName,
    // Look for the component options on `.default`, which will
    // exist if the component was exported with `export default`,
    // otherwise fall back to module's root.
    componentConfig.default || componentConfig
  )
})
/**
 * Register Global Filters
 */
Vue.filter('capitalize', capitalize)
Vue.filter('initials', initials)
Vue.filter('date', date)
Vue.filter('uppercase', uppercase)

Vue.use(Croppa)
Vue.use(VueEsc)

Vue.prototype.$eventHub = new Vue() // Global event bus
global._ = require('lodash') // Global lodash
Vue.config.productionTip = false

export async function createApp ({
  beforeApp = () => {},
  afterApp = () => {}
} = {}) {
  const router = createRouter()
  const store = createStore()
  const apolloProvider = createProvider({
    ssr: process.server
  })

  await beforeApp({
    router,
    store,
    apolloProvider
  })

  const app = new Vue({
    router,
    store,
    apolloProvider,
    vuetify,
    render: h => h(App)
  })

  const result = {
    app,
    router,
    store,
    apolloProvider
  }

  await afterApp(result)

  return result
}
