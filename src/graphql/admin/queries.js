import { programFragment, curriculumFragment, roomFragment, teacherFragment, semesterFragment,
levelFragment } from './fragments'
import gql from 'graphql-tag'

export const GET_USER_LIST = gql`
  query {
    users {
      id
      first_name
    }
  }
` 

export const GET_PROGRAM = gql`
  query {
    program {
      ...programData
    }
  }
  ${programFragment}
`

export const GET_REQUISITE = gql`
  query($course_id: [Int], $keyword: String) {
    courses (course_id: $course_id, keyword: $keyword)
  }
`

export const GET_COURSES = gql`
  query($course_id: Int, $keyword: String) {
    coursesMixed (course_id: $course_id, keyword: $keyword)
  }
`

export const GET_POSITION = gql`
  query {
    userpositions {
      id
      user_type
    }
  }
`

export const GET_CURRICULUM = gql`
  query {
    curriculum {
      ...curriculumData
    }
  }
  ${curriculumFragment}
`

export const GET_CURRICULA = gql`
  query ($input: CurriculaListsWithFilter) {
    curriculaWithFilter (input: $input) 
  }
`

export const GET_PROGRAM_FILTERED = gql `
  query($input: ProgramFilter) {
    programsWithFilter(input: $input) 
  }

`

export const GET_CLASS = gql`
  query ($input: ClassFilter) {
    classWithFilter (input: $input)
  }
`

export const GET_PRESENT_CURRICULUM = gql`
  query {
    curriculumPresentFuture
  }
`

export const GET_SCHOOL_YEAR_PROGRAM = gql `
  query($school_year_id: ID) {
    schoolYearPrograms(school_year_id: $school_year_id)
  }

`

export const GET_CURRICULUM_PROGRAM = gql `
  query($curriculum_id: ID) {
    curriculumPrograms(curriculum_id: $curriculum_id)
  }

`

export const GET_YEAR_LEVEL = gql`
  query {
    yearlevel {
      ...levelData
    }
  }
  ${levelFragment}
` 

export const GET_SEMESTER = gql`
  query {
    semester {
      ...semesterData
    }
  }
  ${semesterFragment}
` 

export const GET_CURRICULUM_COURSES = gql `
  query($input: CurriculumCoursesInput) {
    curriculumCourses(input: $input)
  }

`

export const GET_ROOMS = gql`
  query {
    fetchRooms
  }
` 

export const GET_TEACHERS_COURSE = gql `
  query($course_id: ID) {
    teacherCourse (course_id: $course_id)
  }

`

export const GET_FILTERED_USER = gql `
  query($input: UserFilter) {
    userFilterFetch(input: $input)
  }
`

export const GET_USER_PROGRAM = gql `
  query {
    userprograms {
      id
      program_name
      program_code
    }
  }
`

export const GET_TEACHERS = gql `
  query {
    fetchTeachers
  }
`

export const GET_CLASS_CODE = gql `
  query {
    classCode
  }
`

export const GET_SECTION = gql `
  query {
    fetchSections
  }
`

export const GET_DAY = gql `
  query {
    fetchDays
  }
`

export const GET_UNITS = gql `
  query {
    fetchUnits
  }
`

export const GET_PROGRAM_UNDER_CURRICULUM = gql `
  query($input: CurriculaInput) {
    programsUnderCurriculum(input: $input)
  }
`

export const GET_CURRICULUM_SEMESTER = gql `
  query($input: CurriculumSemesterInput) {
    curriculumSemester (input: $input)
  }
`

export const GET_SCHOOLYEAR_SEMESTER = gql `
  query($input: SYSemesterInput) {
    schoolYearSemester (input: $input)
  }
`

export const GET_CURRICULUM_YEAR_LEVEL = gql `
  query($input: CurriculumYearLevelInput) {
    curriculumYearLevel (input: $input)
  }
`
export const GET_SCHOOLYEAR_YEAR_LEVEL = gql `
  query($input: SYYearLevelInput) {
    schoolYearYearLevel (input: $input)
  }
`

export const GET_CURRENT_PROGRAM = gql `
  query {
    curriculumProgramsYearNow
  }
`

export const GET_COURSES_UNDER_CURRICULUM = gql `
  query($input: CurriculaCourseInput!) {
    coursesUnderCurriculum (input: $input)
  }
`

export const GET_PROSPECTUS = gql `
  query($input: ProgramCurriculumInput) {
    coursesProgramCurriculum (input: $input)
  }
`

export const GET_APPLICANT_REQUIREMENTS = gql `
  query($input: UploadRequirementInput) {
    applicantRequirements (input: $input)
  }
`

export const GET_APPLICANT_INFO = gql `
  query($id: ID) {
    applicantInfo (id: $id)
  }
`

export const GET_COURSE_INFO = gql `
  query($input: CourseListInput) {
    coursesList (input: $input)
  }
`

export const GET_APPLICANT_BY_ID = gql `
  query {
    guidanceApplicants
  }
`

export const GET_TRANSFEREE_BY_ID = gql `
  query ($input: EnrollmentDeanInput) {
    deanApplicants (input: $input)
  }
`

export const GET_SUBMISSION_INFO = gql `
  query($user_id: ID) {
    submitApplicationUser (user_id: $user_id)
  }
`

export const GET_ENROLLMENT_SUBMISSION_INFO = gql `
  query($user_id: ID) {
    submitApplicationEnrollmentUser (user_id: $user_id)
  }
`

export const GET_FILTERED_ROOM = gql `
  query ($input: RoomFilter) {
    roomsFilterFetch (input: $input)
  }
`

export const GET_FILTERED_SECTION = gql `
  query ($input: SectionFilter) {
    sectionsFilterFetch (input: $input)
  }
`

export const CLASS_SCHED_BY_STUDENT = gql `
  query($input: SchedByStudent) {
    classSchedByStudent (input: $input)
  }
`

export const AVAILABLE_CLASSES = gql `
  query($input: SchedAvailabletInput) {
    classSchedAvailable (input: $input)
  }
`

export const GET_ALL_TEACHERS = gql `
  query($keyword: String) {
    teachers (keyword: $keyword)
  }
`

export const GET_SECTION_BY_IDS = gql `
  query($keyword: String, $program_id: ID, $year_level_id: ID) {
    sections (keyword: $keyword, program_id: $program_id, year_level_id: $year_level_id)
  }
`

export const GET_ROOM_BY_IDS = gql `
  query($input: RoomInput) {
    rooms (input: $input)
  }
`

export const GET_SEM_BY_CURRICULUM = gql `
  query ($school_year_id: ID) {
    semesterCurriculum (school_year_id: $school_year_id)
  }
`

export const GET_APPLICANTS_ENROLLMENT= gql `
  query ($input: EnrollmentMISInput) {
    filterEnrollmentMIS (input: $input)
  }
`

export const GET_APPLICANTS_ENTRANCE_EXAM = gql `
  query ($input: EntranceExamMISInput) {
    filterEntranceExamMIS (input: $input)
  }
`

export const GET_EVALUATION_STATUS = gql `
  query($user_id: ID) {
    userEvaluation (user_id: $user_id)
  }
`

export const GET_SORTED_TEACHERS = gql `
  query($input: TeacherFilter) {
    teachersFilterFetch (input: $input)
  }
`

export const GET_STUDENT_HISTORY = gql `
  query($user_id: ID, $program_id: ID) {
    enrollmentHistoryEnrolled (user_id: $user_id, program_id: $program_id)
  }
`

export const GET_SUBJECT_EQUIVALENCY = gql `
  query($user_id: ID) {
    subjectEquivalencyInfo (user_id: $user_id)
  }
`

export const GET_TEACHER_STUDENTS = gql `
  query($user_id: ID, $school_year_id: ID, $program_id: ID, $year_level_id: ID, $semester_id: ID, $class_code_from: Int, $class_code_to: Int, $skip: Int, $take: Int, $keyword: Int) {
    teacherStudents (user_id: $user_id, school_year_id: $school_year_id, program_id: $program_id, year_level_id: $year_level_id, semester_id: $semester_id, class_code_from: $class_code_from, class_code_to: $class_code_to, skip: $skip, take: $take, keyword: $keyword)
  }
`

export const GET_STUDENTS_GRADES = gql `
  query($input: studentGradesInput) {
    studentGrades (input: $input)
  }
`

export const GET_ADD_DROP_HISTORY = gql `
  query($user_id: ID, $school_year_id: ID, $semester_id: ID) {
    addDropHistory (user_id: $user_id, school_year_id: $school_year_id, semester_id: $semester_id)
  }
`

export const GET_STUDENT_EVALUATION = gql `
  query($input: StudentEvaluationInput) {
    studentEvaluation (input: $input)
  }
`

export const GET_STUDENT_CLASS_SCHEDULE = gql `
  query($input: StudentClassScheduleInput) {
    studentClassSchedule (input: $input)
  }
`

export const GET_DEPARTMENT_ENTRANCE_APPLICANTS = gql `
  query($input: EntranceExamDeanInput) {
    deanApplicantsEntranceExam (input: $input)
  }
`

export const GET_GRADING_POINTS = gql `
  query($gwa: String) {
    studentGWA (gwa: $gwa)
  }
`

export const GET_DEPARTMENT_CURRICULUM = gql `
  query($input: CurriculaListsWithFilterDean) {
    curriculaWithFilterDean (input: $input)
  }
`

export const GET_COURSE_DETAILS = gql `
  query($curriculum_course_id: ID) {
    obtDetails (curriculum_course_id: $curriculum_course_id)
  }
`

export const GET_COURSE_NOTES = gql `
  query($curriculum_course_id: ID) {
    obtNotes (curriculum_course_id: $curriculum_course_id)
  }
`

export const GET_MY_GRADES = gql `
  query($skip: Int, $take: Int, $student_id: ID, $school_year_id: ID, $semester_id: ID) {
    myGrades (skip: $skip, take: $take, student_id: $student_id, school_year_id: $school_year_id, semester_id: $semester_id)
  }
`

export const GET_SCHOOL_YEAR = gql `
  query($input: syWithFilterInput) {
    syWithFilter (input: $input)
  }
`

export const GET_SY = gql`
  query {
    syLists
  }
` 

export const GET_PRORAM_HAS_CURRICULUM = gql`
  query {
    programHasCurriculum
  }
` 

export const GET_DISCOUNTS = gql `
  query($skip: Int, $take: Int, $keyword: String) {
    discountQuery (skip: $skip, take: $take, keyword: $keyword)
  }
`

export const GET_FEE_CODES = gql `
  query($skip: Int, $take: Int, $keyword: String, $fee_type_id: ID) {
    feeCodeQuery (skip: $skip, take: $take, keyword: $keyword, fee_type_id: $fee_type_id)
  }
`

export const GET_FEE_TYPES = gql `
  query {
    feeTypesQuery
  }
`

export const GET_ACCOUNTS = gql `
  query($skip: Int, $take: Int, $keyword: String, $type: String) {
    accountCodeQuery (skip: $skip, take: $take, keyword: $keyword, type: $type)
  }
`

export const GET_FEE_CURRICULUM_COURSES = gql `
  query($keyword: String) {
    feeCourses(keyword: $keyword)
  }

`

export const GET_TUITION_AND_OTHER_FEES = gql `
  query($skip: Int, $take: Int, $keyword: String, $sy_program_id: ID, $semester_id: ID, $fee_type_id: ID) {
    feesQuery(skip: $skip, take: $take, keyword: $keyword, sy_program_id: $sy_program_id, semester_id: $semester_id, fee_type_id: $fee_type_id)
  }

`

export const GET_REVISIONS = gql `
  query($keyword: String, $curriculum_id: ID, $skip: Int, $take: Int) {
    revisionLists (keyword: $keyword, curriculum_id: $curriculum_id, skip: $skip, take: $take)
  }
`

export const GET_SY_PROGRAMS = gql `
  query($school_year_id: ID, $program_id: ID) {
    curriculumBySyProgram (school_year_id: $school_year_id, program_id: $program_id)
  }
`

export const GET_CURR_REVISIONS = gql `
  query($keyword: String, $curriculum_id: ID) {
    revisions (keyword: $keyword, curriculum_id: $curriculum_id)
  }
`

export const GET_STUDENT_DISCOUNTS = gql `
  query($school_year_id: ID, $semester_id: ID, $discount_id,: [Int], $skip: Int, $take: Int, $user_id: ID) {
    studentDiscountQuery (school_year_id: $school_year_id, semester_id: $semester_id, discount_id,: $discount_id, skip: $skip, take: $take, user_id: $user_id)
  }
`

export const GET_STUDENT_CHARGES = gql `
  query($sy_id: ID, $semester_id: ID, $skip: Int, $take: Int, $user_id: ID) {
    studentChargesQuery (sy_id: $sy_id, semester_id: $semester_id, skip: $skip, take: $take, user_id: $user_id)
  }
`

export const GET_ROOM_LIST = gql `
  query($keyword: String) {
    roomLists(keyword: $keyword)
  }
`

export const GET_EXAM_SCHEDULE = gql `
  query($school_year_id: ID, $skip: Int, $take: Int) {
    studentExamSchedQuery(school_year_id: $school_year_id, skip: $skip, take: $take)
  }
`

export const GET_ASSESSMENT = gql `
  query($skip: Int, $take: Int, $user_id: ID, $school_year_id: ID, $semester_id: ID, $program_id: ID, $year_level_id: ID, $type: String, $revision_id: ID) {
    studentAssessmentQuery(skip: $skip, take: $take, user_id: $user_id, school_year_id: $school_year_id, semester_id: $semester_id, program_id: $program_id, year_level_id: $year_level_id, type: $type, revision_id: $revision_id)
  }
`

export const GET_CURRICULUM_BY_PROGRAM = gql `
  query($program_id: ID) {
    curriculumByProgram(program_id: $program_id)
  }
`

export const GET_PROGRAM_LIST = gql `
  query($keyword: String) {
    programsLists(keyword: $keyword)
  }
`

export const GET_ROOM_LOCATION = gql `
  query($keyword: String) {
    roomLocationLists(keyword: $keyword)
  }
`

export const GET_ROOM_DIVISION = gql `
  query($keyword: String) {
    roomDivisionLists(keyword: $keyword)
  }
`

export const GET_COURSE_CURRICULUM = gql `
  query($course_code: String ,$program_id: ID ,$revision_id: ID ,$curriculum_id: ID) {
    courseCurriculum(course_code: $course_code, program_id: $program_id, revision_id: $revision_id, curriculum_id: $curriculum_id)
  }
`
export const GET_STUD_POPULATION = gql`
  query {
    studentPopulation
  }
`

export const GET_TOTAL_STUDENTS = gql `
  query {
    totalStudents
  }
`

export const GET_TOTAL_TEACHERS = gql `
  query {
    totalTeachers
  }
`

export const GET_TOTAL_STUDENTS_PER_PROGRAM = gql `
  query {
    studentPerProgram
  }
`
