import gql from 'graphql-tag'

export const curriculumFragment = gql`
  fragment curriculumData on Curriculum {
    id
    curriculum_code
    program_id
    status
    scheme
    cmo_no
    series_year
    subtitle
  }
`

export const courseFragment = gql`
  fragment courseData on Courses {
    id
    course_code
    course_status
    course_description
  }
`

export const userFragment = gql`
  fragment userData on AuthPayload {
    first_name
    middle_name
    last_name
    department
    position
    email
    password
    confirm_password
  }
`

export const programFragment = gql`
  fragment programData on Program {
    id
    program_name
    program_no
    program_code
    program_description
    program_status
  }
`
export const roomFragment = gql`
  fragment roomData on Room {
    id
    room_name
  }
`

export const teacherFragment = gql`
  fragment teacherData on Teacher {
    id
    first_name
    middle_name
    last_name
    name_extension
    email
    birth_date
    gender
  }
`

export const semesterFragment = gql`
  fragment semesterData on Semester {
    id
    semester
  }
`

export const levelFragment = gql`
  fragment levelData on YearLevel {
    id
    year_level
  }
`
