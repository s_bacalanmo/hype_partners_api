import { userFragment, curriculumFragment, roomFragment,
teacherFragment, semesterFragment, levelFragment, courseFragment } from './fragments'
import gql from 'graphql-tag'

export const ADD_CURRICULUM = gql`
  mutation createCurriculum ($input: CurriculumInput!) {
    createCurriculum (input: $input){
      id
      effective_school_year_from
      effective_school_year_to
      scheme
    }
  }
`

export const UPDATE_CURRICULUM = gql`
  mutation updateCurriculum ($input: CurriculumInput!) {
    updateCurriculum (input: $input){
      id
      effective_school_year_from
      effective_school_year_to
      scheme
    }
  }

`

export const DELETE_CURRICULUM = gql`
  mutation deleteCurriculum ($input: CurriculumInput!) {
    deleteCurriculum (input: $input){
      id
      curriculum{
        id
        effective_school_year_from
        effective_school_year_to
        scheme
      }
    }
  }
`

export const CREATE_PROGRAM_CURRICULUM = gql`
  mutation createProgramCurriculum ($input: ProgramCurriculumInput!) {
    createProgramCurriculum (input: $input){
      id
      curriculum_id
      program_id
      year_level_id
      semester_id
      course_id
      status
      programs {
        id
        program_no
        program_code
        program_description
        program_name
        program_status
      }
    }
  }
`

export const DELETE_PROGRAM_CURRICULUM = gql`
  mutation deleteProgramCurriculum ($id: ID!) {
    deleteProgramCurriculum (id: $id){
      id
      curriculum{
        id
        effective_school_year_from
        effective_school_year_to
        scheme
      }
    }
  }
`

export const ADD_COURSE = gql`
  mutation createCourseCurriculum ($input: CourseCurriculumInput!) {
    createCourseCurriculum (input: $input){
      course_id
      curricula_lists_id
      course_code
      course_description
      prerequisite
      lecture_schedule
      laboratory_schedule
      units
      courses {
        id
        course_name
        course_description
        course_status
      }
    }
  }
`

export const UPDATE_COURSE = gql`
  mutation updateCourse ($input: CourseCurriculumInput!) {
    updateCourseCurriculum (input: $input){
      course_id
      curricula_lists_id
      course_code
      course_description
      prerequisite
      lecture_schedule
      laboratory_schedule
      units
      courses {
        id
        course_name
        course_description
        course_status
      }
    }
  }
`

export const DELETE_COURSE = gql`
  mutation deleteCourse ($input: CourseCurriculumInput!) {
    deleteCourseCurriculum (input: $input){
      course_id
      curricula_lists_id
      course_code
      course_description
      prerequisite
      lecture_schedule
      laboratory_schedule
      units
      courses {
        id
        course_name
        course_description
        course_status
      }
    }
  }
`

export const ADD_USER = gql`
  mutation register ($input: RegisterInput!) {
    register (input: $input) {
      ...userData
    }
  }
  ${userFragment}

`

export const ADD_PROGRAM = gql`
  mutation createProgram ($input: ProgramInput!) {
    createProgram (input: $input) {
      id
      program_no
      program_code
      program_name
      program_status
      program_description
    }
  }

`

export const UPDATE_PROGRAM = gql`
  mutation updateProgram($id: ID!, $input: ProgramInput!) {
    updateProgram(id: $id, input: $input) {
      program_no
      program_code
      program_description
      program_name
      program_status
    }
  }
`

export const DELETE_PROGRAM = gql`
  mutation deleteProgram ($id: ID!) {
    deleteProgram(id: $id) {
      id
    }
  }
`

export const ADD_CLASS = gql`
  mutation createClassSchedule ($input: ClassScheduleInput!) {
    createClassSchedule(input: $input) {
      id
      class_code
      section
      day
      time_from
      time_to
      curriculalists {
        curriculum {
          ...curriculumData
        }
        program {
          id
          program_code
        }
        semester {
          ...semesterData
        }
        yearlevel {
          ...levelData
        }
        course{
          ...courseData
        }
        curriculumcourse{
          id
          units
          laboratory_schedule
          lecture_schedule
          prerequisite
          curricula_lists_id
        }
      }
      room {
        ...roomData
      }
      teacher {
        ...teacherData
      }
    }
  }
  ${curriculumFragment}
  ${courseFragment}
  ${roomFragment}
  ${teacherFragment}
  ${semesterFragment}
  ${levelFragment}
`   

export const EDIT_CLASS = gql`
  mutation updateClassSchedule ($input: ClassScheduleInput!) {
    updateClassSchedule(input: $input) {
      id
      class_code
      section
      day
      time_from
      time_to
      curriculalists {
        curriculum {
          ...curriculumData
        }
        program {
          id
          program_code
        }
        semester {
          ...semesterData
        }
        yearlevel {
          ...levelData
        }
        course{
          ...courseData
        }
        curriculumcourse{
          id
          units
          laboratory_schedule
          lecture_schedule
          prerequisite
          curricula_lists_id
        }
      }
      room {
        ...roomData
      }
      teacher {
        ...teacherData
      }
    }
  }
  ${curriculumFragment}
  ${courseFragment}
  ${roomFragment}
  ${teacherFragment}
  ${semesterFragment}
  ${levelFragment}
`

export const DELETE_CLASS = gql`
  mutation deleteClassSchedule ($input: ClassScheduleInput!) {
    deleteClassSchedule(input: $input) {
      id
    }
  }
`
