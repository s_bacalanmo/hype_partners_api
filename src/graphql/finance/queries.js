import gql from 'graphql-tag'

export const GET_FEE_PROGRAM_SY = gql `
  query($skip: Int, $take: Int, $program_id: ID) {
    feeSyProgramQuery(skip: $skip, take: $take, program_id: $program_id)
  }

`

export const GET_SY_BY_PROGRAM = gql `
  query($program_id: ID, $semester_id: ID) {
    syByProgram (program_id: $program_id, semester_id: $semester_id)
  }
`

export const GET_PERIOD = gql `
  query {
    getPeriod
  }
`

export const GET_STUDENT_LEDGER = gql `
  query($skip: Int, $take: Int, $user_id: ID, $school_year_id: ID, $program_id: ID, $semester_id: ID, $year_level_id: ID, $type: String, $revision_id: ID) {
    studentLedgerQuery(skip: $skip, take: $take, user_id: $user_id, school_year_id: $school_year_id, program_id: $program_id,
        semester_id: $semester_id, year_level_id: $year_level_id, type: $type, revision_id: $revision_id)
  }
`

export const GET_ACCOUNT_LIST = gql `
  query($keyword: String) {
    accountCodeLists (keyword: $keyword)
  }
`

export const GET_REG_SUMMARY = gql `
  query($user_id: ID, $school_year_id: ID, $program_id: ID, $semester_id: ID, $year_level_id: ID, $type: String) {
    paymentRegSummary (user_id: $user_id, school_year_id: $school_year_id, program_id: $program_id, semester_id: $semester_id, year_level_id: $year_level_id, type: $type)
  }
`

export const GET_TUITION_SUMMARY = gql `
  query($user_id: ID, $school_year_id: ID, $program_id: ID, $semester_id: ID, $year_level_id: ID, $type: String, $revision_id: ID) {
    paymentTuiOtherSummary (user_id: $user_id, school_year_id: $school_year_id, program_id: $program_id, semester_id: $semester_id, year_level_id: $year_level_id, type: $type, revision_id: $revision_id)
  }
`
