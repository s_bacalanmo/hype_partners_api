import gql from 'graphql-tag'

export const userFragment = gql`
  fragment UserData on AuthPayload {
    user {
      id
      avatar
      first_name
      last_name
      middle_name
      birth_date
      gender
      email
      user_type_id
      usertypes {
        id
        user_type
      }
    }
  }  
`
