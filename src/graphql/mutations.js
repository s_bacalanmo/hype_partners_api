import { userFragment } from './fragments'
import { programFragment } from './admin/fragments'
import gql from 'graphql-tag'

export const ADD_USER = gql`
  mutation register ($input: RegisterInput!) {
    register (input: $input) {
      tokens {
        access_token
        user {
          id
          first_name
          middle_name
          last_name
          name_extension
          email
          birth_date
          gender
          user_type_id
          usertypes {
            user_type
          }
        }
      }
      status
    }
  }
`

export const UPDATE_USER = gql`
  mutation($input: UpdateUserInput!) {
    updateUser(input: $input) {
      id
      first_name
      middle_name
      last_name
      name_extension
      email
      birth_date
      gender
      user_type_id
      position {
        id
        user_type
      }
    }
  }
`

export const DELETE_USER = gql`
  mutation($input: DeleteUserInput!) {
    deleteUser(input: $input){
      id
      first_name
      middle_name
      last_name
      name_extension
      email
      birth_date
      gender
      user_type_id
      position {
        id
        user_type
      }
    }
  }
`

export const FORGOT_PASS = gql`
  mutation($input: ForgotPasswordInput!) {
    forgotPassword(input: $input)
    {
      message
    }
  }
`

export const LOGIN = gql`
  mutation login ($input: LoginInput!) {
    login (input: $input) {
      access_token
      ...UserData
    }
  }
  ${userFragment}

`

export const UPLOAD_FILE = gql`
  mutation upload ($input: UploadInput!) {
    upload (input: $input) {
      ...UserData
    }
  }
  ${userFragment}

`

export const ADD_STUDENT = gql`
  mutation createStudent ($input: CreateStudentInput!) {
    createStudent (input: $input) {
      id
      student_id_number
      user_id
      program_id
      curriculum_id
      year_level_id
      semester_id
      status
      student_status
      studentUser {
        id
        first_name
        last_name
        user_type_id
        usertypes {
          id
          user_type
        }
      }
    }
  }

`

export const UPDATE_STUDENT = gql`
  mutation updateStudent ($input: UpdateDeleteStudentInput!) {
    updateStudent (input: $input) {
      student_id
      student_id_number
      user_id
      program_id
      curriculum_id
      year_level_id
      semester_id
      status
      student_status
        studentUser {
          id
          first_name
          last_name
          user_type_id
          usertypes {
            id
            user_type
          }
      }
    }
  }

`

export const DELETE_STUDENT = gql`
  mutation deleteStudent ($input: UpdateDeleteStudentInput!) {
    deleteStudent (input: $input) {
      id
    }
  }

`

export const ADD_ENTRANCE_EXAM = gql`
  mutation createEntranceExamInfo ($input: EntranceExamInput!) {
    createEntranceExamInfo (input: $input){
      id
      user_id
      status
      applicant_status
      submission_type
      created_at
    }
  }

`
