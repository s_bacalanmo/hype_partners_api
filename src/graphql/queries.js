import gql from 'graphql-tag'

export const GET_USER = gql`
  query {
    user {
      id
      avatar
      first_name
      last_name
      middle_name
      birth_date
      gender
      email
      password
      password_confirmation
      user_type_id
      usertypes {
        id
        user_type
      }
    }
  }
`

export const GET_STUDENT_FILTERED = gql `
  query($input: StudentFilterMIS) {
    studentWithFilterMIS (input: $input)
  }

`

export const GET_ENTRANCE_EXAM = gql`
  query ($input: EntranceExamInput!) {
    fetchEntranceExamInfo (input: $input)
  }

`

export const GET_ENROLLMENT_EXAM = gql`
  query ($id: ID) {
    studentInfo (id: $id)
  }

`

export const GET_FILENAMES = gql`
  query ($input: UploadRequirementInput) {
    requirementFileNames (input: $input)
  }

`

export const CHECK_ENROLLMENT_PERIOD = gql`
  query ($input: checkEnrollmentPeriodInput) {
    checkEnrollmentPeriod(input: $input)
  }

`

export const STUDENT_ENROLLMENT_HISTORY = gql`
  query ($user_id: ID) {
    userEnrollmentHistory(user_id: $user_id)
  }
  
`

export const GET_DEANS_STUDENTS = gql `
  query($input: StudentFilter) {
    studentWithFilter (input: $input)
  }

`

export const CHECK_APPLICANT_ENROLLMENT_PERIOD = gql `
  query ($user_id: ID) {
    checkApplicantEnrollment (user_id: $user_id)
  }

`
