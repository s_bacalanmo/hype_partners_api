import { asyncPostApi, asyncDeleteApi } from '@/http'
import { GET_APPLICANT_INFO } from '@/graphql/admin/queries'
import { mapMutations, mapGetters } from 'vuex'
export default {
  data: () => ({
    errorMessage: '',
    errorDialog: false
  }),
  methods: {
    ...mapMutations('applicants', ['setCourses', 'setSubmissionType', 'setPersonal', 'setEducation', 'setFamily', 'setProgram', 'setStatus']),
    ...mapMutations('form', ['setIcons']),
    setFile (val) {
      if (process.env.NODE_ENV === 'development') {
        if (val) return `${process.env.VUE_APP_URL}/storage/${val}`
        return
      } else {
        return ''
      }
    },
    addFormItem () {
      const data = {...this.formItem }
      this.form.push(data)
    },
    sortItems (items) {
      const program = this.programChoices.filter(item => Number(item.id) === Number(items.id))
      const progItem = global._.omit(...program, 'program_status', 'id', 'program_code', '__typename')
      return global._.merge(items, progItem)
    },
    snackbar (message, color) {
      this.isSnackbar = true
      this.color = color
      this.text = message
    },
    ChangeItem (val, component) {
      switch(component) {
        case 'user':
          if (Number(global._.trim(val)) === 5 || Number(global._.trim(val)) === 11) {
            // for dean and assistant dean
            this.programKey[0].hide = false
            this.programKey[0].fieldTitle = false
          }
          else if (Number(global._.trim(val)) === 3) {
            // for teacher
            this.programKey[0].hide = true
            this.programKey[0].fieldTitle = true
          }
          else {
            this.programKey[0].hide = true
            this.programKey[0].fieldTitle = true
          }
          break
        case 'course':
          break
        default:
      }
    },
    getData () {
      return this.form.filter(item => {
        let hasValue = false
        global._.each(item, (value, key) => {
          if (value) {
            hasValue = true
            return
          }
        })
        return hasValue
      })
    },
    resetFormData () {
      // Resets form data
      this.errors = {}
      if (Array.isArray(this.form)) {
        this.form = []
        if (this.formItem) {
          this.addFormItem()
        }
      } else {
        this.form.id = null
        this.form = {...this.initialData}
      }
    },
    setErrors (errors) {
      this.errors = errors
    },
    saveForm (inputs, mutation_name) {
      this.$apollo.mutate({
        mutation: mutation_name,
        variables: {
          input: { ...inputs }
        }
      }).then((data) => {
        this.form = data
        this.close()
      })
      .catch(error => {
        this.close()
      })
    },
    async saveData (payload) {
      try {
        let response = await asyncPostApi(this.postApi, payload)
        .catch(err => {
          if (err.response.status === 404) {
            throw new Error(`${err.config.url} not found`)
          }
          if (err.response.status === 422) {
            throw err.response.data
          }
          if (err.response.status === 500) {
            this.saveLoading = false
            this.deleteLoading = false
            throw err.response.data
          }
          throw err.response.data
        })
        if (response.data) {
          this.form = response.data
        }
        this.setErrors({})
        this.close()
      } catch (err) {
        if (err) {
          this.saveLoading = false
          this.deleteLoading = false
          this.setErrors(err.errors)
          this.close()
        }
      }
    },
    async deleteData () {
      try {
        let response = await asyncDeleteApi(this.deleteApi)
        .catch(err => {
          if (err.response.status === 404) {
            throw new Error(`${err.config.url} not found`)
          }
          if (err.response.status === 422) {
            throw err.response.data
          }
          if (err.response.status === 500) {
            throw err.response.data
          }
          throw err.response.data
        })
        if (response.data) {
          this.form = response.data
        }
        this.setErrors({})
        this.close()
      }
      catch ({error}) {
        if (error) {
          this.deleteLoading = false
          this.setErrors(err.errors)
          this.close()
        }
      }
    },
    fetchIcon (key, value) {
      let items = {
        key: key,
        value: value
      }
      this.setIcons(items)
    }
  },
  computed: {
    ...mapGetters('registration', ['enrollment', 'entrance']),
    ...mapGetters('applicants', ['submissionType']),
    profileImage () {
      if (this.user.avatar) {
        if (this.user.update) return this.user.avatar
        else return `${process.env.VUE_APP_URL}/storage/${this.user.avatar}`
      }
    },
    programKey () {
      return this.fields.filter(item => item.key === 'program_id')
    },
    enrollmentData () {
      if (this.enrollment) {
        return this.enrollment
      } else {
        return {}
      }
    },
    mode () {
      return this.$route.params.status
    }
  },
  apollo: {
    applicantInfo: {
      query: GET_APPLICANT_INFO,
      variables () {
        return {
          id: global._.isEmpty(this.user) ? 0 : this.user.id
        }
      },
      result ({ data }) {
        let { submissionType } = this
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.applicantInfo)) {
            if (this.mode === 'entrance-exam') {
              this.setStatus(`${data.applicantInfo.status} / ${data.applicantInfo.applicant_status}`)
              this.setProgram(data.applicantInfo.program.program_name)
            }
            this.setSubmissionType(data.applicantInfo.submission_type)
            let description = this.enrollmentData.applicant_program ? this.enrollmentData.applicant_program.program_name : ''
            let lowerDescription = description.toLowerCase()
            let isMarine = lowerDescription.includes('marine')
            let isTransferee = this.enrollmentData.applicant_status === 'Transferee'
            let isFreshman = this.enrollmentData.applicant_status === 'Freshman'
            if (data.applicantInfo.spdi_checklist) {
              this.setPersonal(data.applicantInfo.spdi_checklist)
              this.fetchIcon('personal_info', data.applicantInfo.spdi_checklist.icon)
            } else {
              this.setPersonal({})
              this.fetchIcon('personal_info', '')
            }

            if (data.applicantInfo.spdi_education_bg) {
              this.setEducation(data.applicantInfo.spdi_education_bg)
              this.fetchIcon('educational_info', data.applicantInfo.spdi_education_bg.icon)
            } else {
              this.setEducation({})
              this.fetchIcon('educational_info', '')
            }

            if (data.applicantInfo.spdi_family_bg) {
              this.setFamily(data.applicantInfo.spdi_family_bg)
              this.fetchIcon('family_info', data.applicantInfo.spdi_family_bg.icon)
            } else {
              this.setFamily({})
              this.fetchIcon('family_info', '')
            }
            if (!global._.isUndefined(data.applicantInfo.requirement_icon)) {
              if (submissionType === 'Entrance Exam') {
                if (data.applicantInfo.applicant_status === 'Transferee') {
                  const countItems = data.applicantInfo.requirements.find(item => item.file_name === 'Transcript Of Records')
                  if (!global._.isUndefined(countItems)) {
                    this.fetchIcon('transferee_requirement', data.applicantInfo.requirement_icon.icon)
                  }
                }
              } else {
                let isAdmissions = data.applicantInfo.requirements.filter(item => item.file_name === 'Authenticated Birth Certificate'|| item.file_name === 'Good Moral Character Certificate')
                let isClinic = data.applicantInfo.requirements.filter(item => {
                  if (isMarine) {
                    return item.file_name === 'Ishihara Test' || item.file_name === 'Hearing Test' || item.file_name === 'Blood Typing' || item.file_name === 'Complete Blood Count' || item.file_name === 'Chest X Ray' || item.file_name === 'Drug Test' || item.file_name === 'Electrocardiogram' || item.file_name === 'Hepatitis B Antegen' || item.file_name === 'Physical Exam' || item.file_name === 'Medical Certificate' || item.file_name === 'Stool Examination' || item.file_name === 'Urinalysis' || item.file_name === 'Visual Acuity' || item.file_name === 'Dental Certificate'
                  } else {
                    return item.file_name === 'Blood Typing' || item.file_name === 'Complete Blood Count' || item.file_name === 'Chest X Ray' || item.file_name === 'Drug Test' || item.file_name === 'Electrocardiogram' || item.file_name === 'Hepatitis B Antegen' || item.file_name === 'Physical Exam' || item.file_name === 'Medical Certificate' || item.file_name === 'Stool Examination' || item.file_name === 'Urinalysis'
                  }
                })
                let waiverFile = data.applicantInfo.requirements.find(item => item.file_name === 'Waiver Temporary Enrollment')
                let affidavitFile = data.applicantInfo.requirements.find(item => item.file_name === 'Affidavit Of Undertaking')
                if (isMarine && isTransferee) {
                const countItems = data.applicantInfo.requirements.filter(item => item.file_name ===  'Entrance Exam Result' || item.file_name === 'Transcript Of Records' || item.file_name === 'Certificate Of Honorable Dismissal' || item.file_name === 'Exam Result')
                  if (global._.size(countItems) >= 3) {
                    if (isAdmissions.length < 2 && isClinic.length < 14) {
                      if (!global._.isUndefined(waiverFile) && !global._.isUndefined(affidavitFile)) {
                        if (waiverFile.isAccepted === 'true' && affidavitFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else if (isAdmissions.length >= 2 && isClinic.length < 14) {
                      if (!global._.isUndefined(affidavitFile)) {
                        if (affidavitFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else if (isAdmissions.length < 2 && isClinic.length >= 14) {
                    if (!global._.isUndefined(waiverFile)) {
                        if (waiverFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else {
                      this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                    }
                  } else {
                  this.fetchIcon('requirements', '')
                  }
                } else if ((!isMarine && isTransferee)) {
                  const countItems = data.applicantInfo.requirements.filter(item => item.file_name === 'Entrance Exam Result' || item.file_name === 'Transcript Of Records' || item.file_name === 'Certificate Of Honorable Dismissal')
                  if (global._.size(countItems) >= 2) {
                    if (isAdmissions.length < 2 && isClinic.length < 10) {
                      if (!global._.isUndefined(waiverFile) && !global._.isUndefined(affidavitFile)) {
                        if (waiverFile.isAccepted === 'true' && affidavitFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else if (isAdmissions.length >= 2 && isClinic.length < 10) {
                      if (!global._.isUndefined(affidavitFile)) {
                        if (affidavitFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else if (isAdmissions.length < 2 && isClinic.length >= 10) {
                      if (!global._.isUndefined(waiverFile)) {
                        if (waiverFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else {
                      this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                    }
                  } else {
                    this.fetchIcon('requirements', '')
                  }
                } else if (isMarine && isFreshman) {
                  const countItems = data.applicantInfo.requirements.filter(item => item.file_name === 'Entrance Exam Result' || item.file_name === 'High School Report Card' || item.file_name === 'Exam Result')
                  if (global._.size(countItems) >= 3) {
                    if (isAdmissions.length < 2 && isClinic.length < 14) {
                      if (!global._.isUndefined(waiverFile) && !global._.isUndefined(affidavitFile)) {
                        if (waiverFile.isAccepted === 'true' && affidavitFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else if (isAdmissions.length >= 2 && isClinic.length < 14) {
                      if (!global._.isUndefined(affidavitFile)) {
                        if (affidavitFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else if (isAdmissions.length < 2 && isClinic.length >= 14) {
                      if (!global._.isUndefined(waiverFile)) {
                        if (waiverFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else {
                      this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                    }
                  } else {
                    this.fetchIcon('requirements', '')
                  }
                } else if (!isMarine && isFreshman) {
                  const countItems = data.applicantInfo.requirements.filter(item => item.file_name ===  'Entrance Exam Result' || item.file_name === 'High School Report Card')
                  if (global._.size(countItems) >= 2) {
                    if (isAdmissions.length < 2 && isClinic.length < 10) {
                      if (!global._.isUndefined(waiverFile) && !global._.isUndefined(affidavitFile)) {
                        if (waiverFile.isAccepted === 'true' && affidavitFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else if (isAdmissions.length >= 2 && isClinic.length < 10) {
                      if (!global._.isUndefined(affidavitFile)) {
                        if (affidavitFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else if (isAdmissions.length < 2 && isClinic.length >= 10) {
                      if (!global._.isUndefined(waiverFile)) {
                        if (waiverFile.isAccepted === 'true') {
                          this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                        }
                      } else {
                        this.fetchIcon('requirements', '')
                      }
                    } else {
                      this.fetchIcon('requirements', data.applicantInfo.requirement_icon.icon)
                    }
                  } else {
                    this.fetchIcon('requirements', '')
                  }
                }
              }
            } else {
              this.fetchIcon('requirements', '')
            }

            if (data.applicantInfo.requirement_icon) {
              this.fetchIcon('transferee_requirement', data.applicantInfo.requirement_icon.icon)
            }
            // if (data.applicantInfo.payment) {
            //   this.fetchIcon('entrance_payment', data.applicantInfo.payment.icon)
            // }
            this.fetchIcon('courses', 'mdi-check-circle')
            this.fetchIcon('payment', 'mdi-check-circle')
            this.fetchIcon('entrance_payment', 'mdi-check-circle')
            this.fetchIcon('application_info', 'mdi-check-circle')
          } else {
            this.setPersonal({})
            this.setEducation({})
            this.setFamily({})
            this.fetchIcon('application_info', 'mdi-check-circle')
            this.fetchIcon('personal_info', '')
            this.fetchIcon('family_info', '')
            this.fetchIcon('educational_info', '')
            this.fetchIcon('transferee_requirement', '')
            this.fetchIcon('requirements', '')
            this.fetchIcon('entrance_payment', '')
            this.fetchIcon('payment', 'mdi-check-circle')
            this.fetchIcon('courses', 'mdi-check-circle')
          }
        }
      },
      skip () {
        return this.skipQuery
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      },
      error ({ graphQLErrors, networkError }) {
        if (graphQLErrors) {
          console.log('graphQLErrors:', graphQLErrors)
        }
        if (networkError) {
          console.log('networkError:', networkError)
        }
      }
    }
  }
}
