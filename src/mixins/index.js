import { mapGetters } from 'vuex'
import { format } from 'date-fns'
export default {
  computed: {
    ...mapGetters('user', ['user']),
    hideCard () {
      return this.$vuetify.breakpoint.smAndUp
    },
    isArrowEnabled () {
      switch (this.$vuetify.breakpoint.name) {
        case 'xs': return true
        case 'sm': return true
        case 'md': return false
      }
    },
    userType () {
      if (this.user) return Number(global._.trim(this.user.user_type_id))
      return ''
    },
    cardFontSize () {
      if (this.$vuetify.breakpoint.xsOnly) return 'caption'
      return ''
    },
    paramType () {
      return this.$route.params.type
    }
  },
  methods: {
    sortSkipped (value, currPage, pageCount) {
      if (global._.isEqual(value, 0)) {
        return value
      } else {
        return (value * currPage) - pageCount
      }
    },
    getValuesByKey (objList, key) {
      if (objList.length > 0) {
        return objList.map((item) => item[key])
      } else {
        return []
      }
    },
    textCapitalize (value) {
      if (!value) return ''
      value = global._.capitalize(value)
      let str = value.split(' ')
       for (var i = 0; i < str.length; i++) {
        str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1)
      }
      return str.join(' ')
    },
    getTabIndex (val) {
      const item = this.items.find(item => item.title === val)
      const itemIndex = this.items.indexOf(item)
      return itemIndex
    },
    numberFormat (number) {
      if (number) {
        const decimaNumber = Number(number).toFixed(2)
        return decimaNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      } else {
        return 0
      }
    },
    roundOf (number) {
      return global._.round(number, 2)
    },
    formatDate (val) {
      return format(new Date(val), 'yyyy-MM-dd')
    },
    setColor(item) {
      return item == 'Active' ? 'green' : 'red'
    },
    updatedFunc (title) {
      let items = this.tabItems[0].tabs
      let isUpdate = items.find(item => item.title === title)
      return isUpdate
    },
    isEmpty (item) {
      return global._.isEmpty(item)
    },
    getIndex (element, scrolled) {
      return {
        active: element === scrolled,
        'white--text': element === scrolled,
        tabColor: element === scrolled
      }
    },
    getColor (value) {
      return {
        'green--text': value === 'mdi-check-circle',
        'red--text': value !== 'mdi-check-circle'
      }
    },
    getTopPage () {
      window.scrollTo(0, 0)
    },
    scrollTo (element) {
      const options = {
        duration: 1000,
        offset: 100,
        easing: 'easeInQuad'
      }
      this.$vuetify.goTo(element, options)
      window.location.hash = element
      this.getTopPage()

    },
    initialLetter (middlename) {
      if (!global._.isNull(middlename)) {
        const splitName = middlename.split(' ')
        const initial = splitName.map((word) => {
          return word[0].toUpperCase()
        })
        return `${initial.join(' ')}. `
      } else {
        return ''
      }
    },
    capitalLeter (val) {
      if (!global._.isNull(val)) {
        const lowerLabel = val.toLowerCase()
        const splitLabel = lowerLabel.split(' ')
        const upFirstChar = splitLabel.map((word) => {
          return word[0].toUpperCase() + word.slice(1)
        })
        const joinLabel = upFirstChar.join(' ')
        return joinLabel
      } else {
        return ''
      }
    },
    removeItem (title) {
      const currItem = this.checklist.find(item => item.title === title)
      const itemIndex = this.checklist.indexOf(currItem)
      if (itemIndex !== -1) {
        this.$delete(this.checklist, itemIndex)
      }
    },
    addItem (title) {
      const hasItem = this.checklist.filter(item => item.title === title)
      if (global._.isEmpty(hasItem)) {
        this.checklist.push({
          title: title,
          disabled: false
        })
      }
    },
    checkItem (data, index) {
      if (index !== -1) {
        if (this.approvalId === 1) {
          this.checklist[index].title = data.title
          this.checklist[index].disabled = data.disabled ? false : true
        }
      }
    }
  }
}
