export default {
  methods: {
    getValuesByKey (objList, key) {
      if (objList.length > 0) {
        return objList.map((item) => item[key])
      } else {
        return []
      }
    }
  }
}
