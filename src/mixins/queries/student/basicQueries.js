import { GET_CURRICULUM_BY_PROGRAM, GET_CURR_REVISIONS } from '@/graphql/admin/queries'
import { mapGetters, mapMutations } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('user', ['user']),
  },
  apollo: {
    curriculumByProgram: {
      query: GET_CURRICULUM_BY_PROGRAM,
      variables () {
        return {
          program_id: this.registrar.program_id
        }
      },
      result ({ data }) {
        const curriculums = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.curriculumByProgram, function (value) {
            curriculums.push({
              ...value,
              curriculum_id: Number(value.id)
            })
          })
          this.curriculums = curriculums
        }
      }
    },
    revisions: {
      query: GET_CURR_REVISIONS,
      variables () {
        return {
          keyword: null,
          curriculum_id: this.curriculumId
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.revisions, function (value) {
            items.push({
              revision: `Revision No. ${Number(value.revision_no)}`,
              revision_id: Number(value.id)
            })
          })
          this.revisions = items
        }
      },
      skip () {
        return global._.isNull(this.curriculumId)
      }
    }
  }
}
