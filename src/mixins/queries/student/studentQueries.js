import { GET_ENTRANCE_EXAM, GET_ENROLLMENT_EXAM } from '@/graphql/queries'
import { GET_CURRICULUM, GET_CURRICULUM_PROGRAM, GET_CURRICULUM_YEAR_LEVEL, GET_CURRICULUM_SEMESTER, GET_CURR_REVISIONS } from '@/graphql/admin/queries'
import { mapGetters, mapMutations } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('user', ['user']),
    mode () {
      return this.$route.params.status
    }
  },
  methods: {
    ...mapMutations('applicants', ['setProgram', 'setSemester', 'setStatus', 'setStudentApplicant', 'setPersonal', 'setEducation', 'setFamily']),
    ...mapMutations('form', ['setStudentIcons']),
    ...mapMutations('registration', ['setEntrance', 'setEnrollment']),
    fetchStudentIcon (key, value) {
      let items = {
        key: key,
        value: value
      }
      this.setStudentIcons(items)
    }
  },
  apollo: {
    studentInfo: {
      query: GET_ENROLLMENT_EXAM,
      variables () {
        return {
          id: this.user.id
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.studentInfo)) {
            let description = data.studentInfo.program ? data.studentInfo.program.program_name : ''
            let lowerDescription = description.toLowerCase()
            let isMarine = lowerDescription.includes('marine')
            let isTransferee = data.studentInfo.applicant_status === 'Transferee'
            let isFreshman = data.studentInfo.applicant_status === 'Freshman'
            this.fetchStudentIcon('student_application_info', data.studentInfo.icon)
            this.setStudentApplicant(data.studentInfo)
            this.setEnrollment(data.studentInfo)
            if (!global._.isNull(data.studentInfo.program)) {
              this.setProgram(data.studentInfo.program.program_name)
            } else {
              this.setProgram('')
            }
            if (!global._.isNull(data.studentInfo.semester)) {
              this.setSemester(data.studentInfo.semester.semester)
            }
            if (this.mode === 'enrollment' || global._.isUndefined(this.mode)) {
              const statusInfo = `${data.studentInfo.status} / ${data.studentInfo.applicant_status}`
              this.setStatus(statusInfo)
            }

            this.fetchStudentIcon('student_application_info', 'mdi-check-circle')
            if (data.studentInfo.spdi_checklist) {
              this.setPersonal(data.studentInfo.spdi_checklist)
              this.fetchStudentIcon('student_personal_info', data.studentInfo.spdi_checklist.icon)
            } else {
              this.fetchStudentIcon('student_personal_info', '')
              this.setPersonal({})
            }
            if (data.studentInfo.spdi_education_bg) {
              this.setEducation(data.studentInfo.spdi_education_bg)
              this.fetchStudentIcon('student_educational_info', data.studentInfo.spdi_education_bg.icon)
            } else {
              this.fetchStudentIcon('student_educational_info', '')
              this.setEducation({})
            }
            if (data.studentInfo.spdi_family_bg) {
              this.setFamily(data.studentInfo.spdi_family_bg)
              this.fetchStudentIcon('student_family_info', data.studentInfo.spdi_family_bg.icon)
            } else {
              this.fetchStudentIcon('student_family_info', '')
              this.setFamily({})
            }
            if (!global._.isEmpty(data.studentInfo.requirements)) {
              if (isMarine && isTransferee) {
                const countItems = data.studentInfo.requirements.filter(item => item.file_name ===  'Entrance Exam Result' || item.file_name === 'Transcript Of Records' || item.file_name === 'Certificate Of Honorable Dismissal' || item.file_name === 'Exam Result')
                if (global._.size(countItems) >= 3) {
                  this.fetchStudentIcon('student_requirements', 'mdi-check-circle')
                } else {
                  this.fetchStudentIcon('student_requirements', '')
                }
              } else if ((!isMarine && isTransferee)) {
                const countItems = data.studentInfo.requirements.filter(item => item.file_name === 'Entrance Exam Result' || item.file_name === 'Transcript Of Records' || item.file_name === 'Certificate Of Honorable Dismissal')
                if (global._.size(countItems) >= 2) {
                  this.fetchStudentIcon('student_requirements', 'mdi-check-circle')
                } else {
                  this.fetchStudentIcon('student_requirements', '')
                }
              } else if (isMarine && isFreshman) {
                const countItems = data.studentInfo.requirements.filter(item => item.file_name === 'Entrance Exam Result' || item.file_name === 'High School Report Card' || item.file_name === 'Exam Result')
                if (global._.size(countItems) >= 3) {
                  this.fetchStudentIcon('student_requirements', 'mdi-check-circle')
                } else {
                  this.fetchStudentIcon('student_requirements', '')
                }
              } else if (!isMarine && isFreshman) {
                const countItems = data.studentInfo.requirements.filter(item => item.file_name ===  'Entrance Exam Result' || item.file_name === 'High School Report Card')
                if (global._.size(countItems) >= 2) {
                  this.fetchStudentIcon('student_requirements', 'mdi-check-circle')
                } else {
                  this.fetchStudentIcon('student_requirements', '')
                }
              }
            } else {
              this.fetchStudentIcon('student_requirements', '')
            }
            if (data.studentInfo.payment) {
              this.fetchStudentIcon('student_payment', data.studentInfo.payment.icon)
            } else {
              this.fetchStudentIcon('student_payment', 'mdi-check-circle')
            }
            this.fetchStudentIcon('student_courses', 'mdi-check-circle')
          }
        } else {
          this.setProgram('')
          this.setSemester('')
          this.setStatus('')
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      },
      skip () {
        return this.skipUserQuery
      }
    },
    fetchEntranceExamInfo: {
      query: GET_ENTRANCE_EXAM,
      variables () {
        return {
          input: {
            user_id: this.user.id
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (data.fetchEntranceExamInfo) {
            this.setEntrance(data.fetchEntranceExamInfo)
          } else {
            this.setEntrance({})
          }
        }
      }
    },
    curriculum: {
      query: GET_CURRICULUM,
      result ({ data }) {
        const curriculums = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.curriculum, function (value) {
            curriculums.push({
              curriculum_id: value.id,
              curriculum_code: value.curriculum_code
            })
          })
          this.years = curriculums
        }
      }
    },
    curriculumPrograms: {
      query: GET_CURRICULUM_PROGRAM,
      variables () {
        return {
          curriculum_id: global._.isEmpty(this.formData) ? '' : this.formData[0].curriculum_id
        }
      },
      result ({ data }) {
        const programs = []
        if (!global._.isUndefined(data)) {
          this.skipYearQuery = false
          const programsItems = data.curriculumPrograms.filter(item => item.program_status === 'Active')
          global._.forEach(programsItems, function (value) {
            programs.push({
              program_id: value.id,
              program_code: value.program_code
            })
          })
          this.programs = programs
        }
      },
      skip () {
        return this.skipStudentQuery
      }
    },
    curriculumYearLevel: {
      query: GET_CURRICULUM_YEAR_LEVEL,
      variables () {
        return {
          input: {
            curriculum_id: global._.isEmpty(this.formData) ? '' : this.formData[0].curriculum_id,
            program_id: global._.isEmpty(this.formData) ? '' : this.formData[0].program_id
          }
        }
      },
      result ({ data }) {
        const levels = []
        if (!global._.isUndefined(data)) {
          this.skipSemQuery = false
          global._.forEach(data.curriculumYearLevel, function (value) {
            levels.push({
              year_level_id: value.id,
              year_level: value.year_level
            })
          })
          this.levels = levels
        }
      },
      skip () {
        return this.skipYearQuery
      }
    },
    curriculumSemester: {
      query: GET_CURRICULUM_SEMESTER,
      variables () {
        return {
          input: {
            curriculum_id: this.formData[0].curriculum_id,
            year_level_id: this.formData[0].year_level_id,
            program_id: this.formData[0].program_id
          }
        }
      },
      result ({ data }) {
        const semesters = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.curriculumSemester, function (value) {
            semesters.push({
              semester_id: value.id,
              semester: value.semester
            })
          })
          this.semesters = semesters
        }
      },
      skip () {
        return this.skipSemQuery
      }
    },
    revisions: {
      query: GET_CURR_REVISIONS,
      variables () {
        return {
          keyword: null,
          curriculum_id: this.formData[0].curriculum_id
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.revisions, function (value) {
            items.push({
              revision_no: `Revision No. ${Number(value.revision_no)}`,
              revision_id: Number(value.id)
            })
          })
          this.revisions = items
        }
      },
      skip () {
        return this.skipCurrRevisionQuery
      }
    }
  }
}