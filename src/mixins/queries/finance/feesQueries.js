import { GET_STUDENT_DISCOUNTS, GET_SEM_BY_CURRICULUM, GET_SY, GET_DISCOUNTS, GET_FEE_CODES, GET_FEE_TYPES, GET_ACCOUNTS } from '@/graphql/admin/queries'
import { GET_SY_BY_PROGRAM, GET_FEE_PROGRAM_SY } from '@/graphql/finance/queries'
import defaultMixins from '@/mixins/index'
import { mapGetters } from 'vuex'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('filters', ['academic_year_id', 'semester_id', 'fee_type_id']),
    ...mapGetters('programs', ['semester']),
  },
  apollo: {
    syLists: {
      query: GET_SY,
      result ({ data }) {
        const school_years = []
        if (data) {
          global._.forEach(data.syLists, function (value) {
            school_years.push({
              school_year_id: value.id,
              school_year: `${value.school_year_from} - ${value.school_year_to}`
            })
          })
          this.years = school_years
        }
      },
      skip () {
        return this.skipSYQuery
      }
    },
    semesterCurriculum: {
      query: GET_SEM_BY_CURRICULUM,
      variables () {
        return {
          school_year_id: this.formData.school_year_id
        }
      },
      result ({ data }) {
        let curr_semester = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.semesterCurriculum, function (value) {
            curr_semester.push({
              semester_id: value.id,
              semester: value.semester
            })
          })
          this.currSemesterItems = curr_semester
        }
      },
      skip () {
        return this.sySemesterQuery
      }
    },
    discountQuery: {
      query: GET_DISCOUNTS,
      variables () {
        return {
          skip: this.skip,
          take: this.take,
          keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch
        }
      },
      result ({ data }) {
        let discounts = []
        if (!global._.isUndefined(data)) {
          this.discountCount = data.discountQuery.count
          global._.forEach(data.discountQuery.discounts, function (value) {
            discounts.push(value)
          })
          let activeDiscount = discounts.filter(item => item.status === 'Active')
          this.discounts = activeDiscount
          this.discountItems = discounts
        }
      },
      skip () {
        return this.skipDiscountQuery
      },
      watchLoading (isLoading) {
        this.discountLoading = isLoading
      }
    },
    feeCodeQuery: {
      query: GET_FEE_CODES,
      variables () {
        return {
          skip: 0,
          take: this.take,
          keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
          fee_type_id: this.fee_type_id
        }
      },
      result ({ data }) {
        let items = []
        if (!global._.isUndefined(data)) {
          this.feeCount = data.feeCodeQuery.count
          if (!global._.isEmpty(data.feeCodeQuery.feeCodes)) {
            global._.forEach(data.feeCodeQuery.feeCodes, function (val) {
              items.push({
                ...val,
                id: val.id,
                code: val.code,
                description: val.description,
                fee_type: global._.isNull(val.fee_type) ? '' : val.fee_type.name,
                fee_item: {
                  name: global._.isNull(val.fee_type) ? '' : val.fee_type.name,
                  fee_type_id: global._.isNull(val.fee_type) ? '' : val.fee_type.id
                },
                course_item: {
                  curriculum_course_id: val.curriculum_course_id,
                  course_code: global._.isNull(val.curriculum_course) ? '' : val.curriculum_course.course_code
                },
                course_code: global._.isNull(val.curriculum_course) ? '' : val.curriculum_course.course_code
              })
            })
          }
          this.feeCodesItems = items
        }
      },
      skip () {
        return this.skipFeeCodeQuery
      },
      watchLoading (isLoading) {
        this.feeLoading = isLoading
      }
    },
    feeTypesQuery: {
      query: GET_FEE_TYPES,
      result ({ data }) {
        let fees = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.feeTypesQuery, function (value) {
            fees.push({
              fee_type_id: value.id,
              name: value.name
            })
          })
          this.feeTypes = fees
        }
      },
      skip () {
        return this.skipFeeTypesQuery
      }
    },
    accountCodeQuery: {
      query: GET_ACCOUNTS,
      variables () {
        return {
          skip: this.skip,
          take: this.take,
          keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
          type: this.account_type
        }
      },
      result ({ data }) {
        let accounts = []
        if (!global._.isUndefined(data)) {
          this.accountsCount = data.accountCodeQuery.count
          global._.forEach(data.accountCodeQuery.accountCodes, function (value) {
            accounts.push(value)
          })
          this.accountItems = accounts
        }
      },
      skip () {
        return this.skipAccountQuery
      },
      watchLoading (isLoading) {
        this.accountCodeLoading = isLoading
      }
    },
    studentDiscountQuery: {
      query: GET_STUDENT_DISCOUNTS,
      variables () {
        return {
          skip: this.skip,
          take: this.take,
          school_year_id: this.academic_year_id,
          semester_id: this.semester_id,
          discount_id: this.discount_type,
          keyword: null,
          user_id: global._.isUndefined(this.registrar) ? '' : this.registrar.user_info.id
        }
      },
      result ({ data }) {
        let items = []
        if (!global._.isUndefined(data)) {
          this.studDiscountDount = Number(data.studentDiscountQuery.count)
          global._.forEach(data.studentDiscountQuery.studentDiscount, function (val) {
            let discountItem = {
              ...val.discount,
              percentage_name: val.percentage
            }
            let item = {
              ...val,
              discount: discountItem,
              school_year: val.sy ? `${val.sy.school_year_from} - ${val.sy.school_year_to}` : '',
              semester: val.semester ? val.semester.semester : '',
              name: val.discount ? val.discount.name : ''
            }
            items.push(item)
          })
          this.studentDiscountItems = items
        }
      },
      skip () {
        return global._.isUndefined(this.registrar) || global._.isEmpty(this.registrar)
      },
      watchLoading (isLoading) {
        this.studDiscountLoading = isLoading
      }
    },
    feeSyProgramQuery: {
      query: GET_FEE_PROGRAM_SY,
      variables () {
        return {
          skip: this.skip,
          take: this.take,
          program_id: this.programs ? this.programs.id : null
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          this.feesycount = Number(data.feeSyProgramQuery.count)
          global._.forEach(data.feeSyProgramQuery.sy_program, function (val) {
            items.push({
              sy_program_id: val.id,
              ...val,
              ...val.schoolyear,
              semester_fee: val.semester,
              semester: Boolean(val.schoolyear.first_sem || val.schoolyear.second_sem || val.schoolyear.third_sem),
              school_year: val.schoolyear ? `${val.schoolyear.school_year_from} - ${val.schoolyear.school_year_to}` : ''
            })
          })
          this.financeyearsItems = items
        }
      },
      skip () {
        return global._.isEmpty(this.programs)
      }
    },
    syByProgram: {
      query: GET_SY_BY_PROGRAM,
      variables () {
        return {
          program_id: this.programs ? this.programs.id : null,
          semester_id: this.semester ? this.semester.semester_id : null
        }
      },
      result ({ data }) {
        let items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.syByProgram.syprogram, function (val) {
            let first_sem_fee = val.semester.find(item => item === val.schoolyear.first_sem)
            let second_sem_fee = val.semester.find(item => item === val.schoolyear.second_sem)
            let third_sem_fee = val.semester.find(item => item === val.schoolyear.third_sem)
            if (!global._.isUndefined(first_sem_fee) || !global._.isUndefined(second_sem_fee) || !global._.isUndefined(third_sem_fee)) {
              items.push({
                first_sem_fee: !global._.isUndefined(first_sem_fee) ? true : false,
                second_sem_fee: !global._.isUndefined(second_sem_fee) ? true : false,
                third_sem_fee: !global._.isUndefined(third_sem_fee) ? true : false,
                first_sem: val.schoolyear ? val.schoolyear.first_sem : '',
                second_sem: val.schoolyear ? val.schoolyear.second_sem : '',
                third_sem: val.schoolyear ? val.schoolyear.third_sem : '',
                sy_program_id: Number(val.id),
                school_year: val.schoolyear ? `${val.schoolyear.school_year_from} - ${val.schoolyear.school_year_to}` : ''
              })
            }
          })
          this.copyYears = items
        }
      },
      skip () {
        return this.skipcopyfeeQuery
      }
    }
  }
}
