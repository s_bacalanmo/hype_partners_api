
import { GET_ACCOUNTS, GET_SY, GET_SEMESTER } from '@/graphql/admin/queries'
import { GET_REG_SUMMARY, GET_TUITION_SUMMARY } from '@/graphql/finance/queries'
import { mapGetters } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('user', ['student_data', 'registrar', 'user']),
    ...mapGetters('filters', ['academic_year_id', 'semester_id']),
    skipstudentclassQuery () {
      return !Boolean(this.registrar.user_info.id && this.registrar.semester_id && this.registrar.program_id && this.registrar.curriculum_id && this.registrar.year_level_id && this.user.revision_id)
    }
  },
  apollo: {
    syLists: {
      query: GET_SY,
      result ({ data }) {
        const school_years = []
        if (data) {
          global._.forEach(data.syLists, function (value) {
            school_years.push({
              id: value.id,
              school_year: `${value.school_year_from} - ${value.school_year_to}`
            })
          })
          this.years = school_years
        }
      }
    },
    semester: {
      query: GET_SEMESTER,
      result ({ data }) {
        if (data) {
          let items = []
          global._.forEach(data.semester, function (value) {
            items.push({
              id: Number(value.id),
              semester: value.semester
            })
          })
          this.semesters = items
        }
      }
    },
    paymentRegSummary: {
      query: GET_REG_SUMMARY,
      variables () {
        return {
          user_id: this.registrar.user_info.id,
          school_year_id: this.academic_year_id ? this.academic_year_id : this.registrar.school_year_id,
          program_id: this.registrar.program_id ? this.registrar.program_id : null,
          semester_id: this.semester_id ? this.semester_id : this.registrar.semester_id,
          year_level_id: this.registrar.year_level_id || !global._.isUndefined(this.registrar.year_level_id) ? this.registrar.year_level_id : null,
          type: !this.registrar.scheme ? 'Fee' : this.registrar.scheme === '311' ? 'CSC' : 'NSA'
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.paymentRegSummary)) {
            this.reg_summary = data.paymentRegSummary
          }
        }
      },
      skip () {
        return this.skipRegSummaryQuery
      }
    },
    paymentTuiOtherSummary: {
      query: GET_TUITION_SUMMARY,
      variables () {
        return {
          user_id: this.registrar.user_info.id,
          school_year_id: this.academic_year_id ? this.academic_year_id : this.registrar.school_year_id,
          program_id: this.registrar.program_id ? this.registrar.program_id : null,
          semester_id: this.semester_id ? this.semester_id : this.registrar.semester_id,
          year_level_id: this.registrar.year_level_id || !global._.isUndefined(this.registrar.year_level_id) ? this.registrar.year_level_id : null,
          type: !this.registrar.scheme ? 'Fee' : this.registrar.scheme === '311' ? 'CSC' : 'NSA',
          revision_id: this.registrar.revision_id ? this.registrar.revision_id : null
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.paymentTuiOtherSummary)) {
            this.tuition_summary = data.paymentTuiOtherSummary
          }
        }
      },
      skip () {
        return this.skipOtherSummaryQuery
      }
    }
  },
  methods: {
    querySelections (val) {
      this.$apollo.query({
        query: GET_ACCOUNTS,
        variables: {
          skip: 0,
          take: this.take,
          keyword: val ? val : null,
          type: this.account_type
        }
      }).then(({ data, loading }) => {
        let accounts = []
        if (!global._.isUndefined(data)) {
          this.accountsCount = data.accountCodeQuery.count
          global._.forEach(data.accountCodeQuery.accountCodes, function (value) {
            accounts.push(value)
          })
          this.accountCodes = accounts
        }
      })
      .catch(error => {
        if (error.graphQLErrors) {
          console.log('graphQLErrors:', error.graphQLErrors)
        }
        if (error.networkError) {
          console.log('networkError:', error.networkError)
        }
        if (error.ServerError) {
          console.log('ServerError:', error.ServerError)
        }
      })
    }
  }
}