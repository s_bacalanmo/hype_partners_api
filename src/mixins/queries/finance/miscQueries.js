import { GET_SEM_BY_CURRICULUM, GET_SCHOOL_YEAR_PROGRAM, GET_FEE_CODES, GET_TUITION_AND_OTHER_FEES } from '@/graphql/admin/queries'
import { mapGetters } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('user', ['student_data', 'registrar', 'user']),
    ...mapGetters('filters', ['academic_year_id', 'semester_id']),
    skipstudentclassQuery () {
      return !Boolean(this.registrar.user_info.id && this.registrar.semester_id && this.registrar.program_id && this.registrar.curriculum_id && this.registrar.year_level_id && this.user.revision_id)
    }
  },
  apollo: {
    semesterCurriculum: {
      query: GET_SEM_BY_CURRICULUM,
      variables () {
        return {
          school_year_id: global._.isUndefined(this.formData.school_year_id) ? null : this.formData.school_year_id
        }
      },
      result ({ data }) {
        let curr_semester = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.semesterCurriculum, function (value) {
            curr_semester.push({
              semester_id: value.id,
              semester: value.semester
            })
          })
          this.semesters = curr_semester
        }
      },
      skip () {
        return global._.isUndefined(this.formData.school_year_id)
      }
    },
    schoolYearPrograms: {
      query: GET_SCHOOL_YEAR_PROGRAM,
      variables () {
        return {
          school_year_id: this.formData.school_year_id
        }
      },
      result ({ data }) {
        const programs = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.schoolYearPrograms, function (value) {
            programs.push({
              program_id: value.id,
              program_code: value.program_code
            })
          })
          this.programsLists = programs
        }
      },
      skip () {
        return !this.formData.school_year_id
      }
    },
    feesQuery: {
      query: GET_TUITION_AND_OTHER_FEES,
      variables () {
        return {
          skip: this.skip,
          take: this.take,
          keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
          fee_type_id: global._.isUndefined(this.fee_type_id) ? '' : this.fee_type_id,
          sy_program_id: this.semester ? this.semester.sy_program_id : null,
          semester_id: this.semester ? this.semester.semester_id : null
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          let count = 0
          if (!global._.isEmpty(data.feesQuery.fees)) {
            this.feesCount = Number(data.feesQuery.count)
            let val = data.feesQuery.fees.fee_bridge
            let valIds = data.feesQuery.fees
            global._.forEach(val, function (itemVal) {
              let item = global._.omit(itemVal, 'id')
              let itemCode = !global._.isNull(item.fee_code) ? global._.omit(item.fee_code, 'id', 'percentage') : null
              let itemType = !global._.isNull(item.fee_code) ? global._.omit(item.fee_code.fee_type, 'id') : null
              let feeOneItem = item.fee_sy.find(item => item.year_level_id === 1 && item.type === 'Fee')
              let feeTwoItem = item.fee_sy.find(item => item.year_level_id === 2 && item.type === 'Fee')
              let feeThirdItem = item.fee_sy.find(item => item.year_level_id === 3 && item.type === 'Fee')
              let feeFourthItem = item.fee_sy.find(item => item.year_level_id === 4 && item.type === 'Fee')
              let nsaOneItem = item.fee_sy.find(item => item.year_level_id === 1 && item.type === 'NSA')
              let nsaTwoItem = item.fee_sy.find(item => item.year_level_id === 2 && item.type === 'NSA')
              let nsaThirdItem = item.fee_sy.find(item => item.year_level_id === 3 && item.type === 'NSA')
              let nsaFourthItem = item.fee_sy.find(item => item.year_level_id === 4 && item.type === 'NSA')
              let cscOneItem = item.fee_sy.find(item => item.year_level_id === 1 && item.type === 'CSC')
              let cscTwoItem = item.fee_sy.find(item => item.year_level_id === 2 && item.type === 'CSC')
              let cscThirdItem = item.fee_sy.find(item => item.year_level_id === 3 && item.type === 'CSC')
              let cscFourthItem = item.fee_sy.find(item => item.year_level_id === 4 && item.type === 'CSC')
              items.push({
                ...item,
                ...itemCode,
                ...itemType,
                fee_code_id: itemVal.fee_code_id,
                fee_bridge_id: itemVal.id,
                delete_id: itemVal.id,
                sy_program_id: valIds.sy_program_id,
                semester_id: valIds.semester_id,
                description: global._.isNull(itemVal.fee_code) ? '' : itemVal.fee_code.description,
                code: global._.isNull(itemVal.fee_code) ? '' : itemVal.fee_code.code,
                status: item.status,
                first_year: global._.isUndefined(feeOneItem) ? null : global._.round(feeOneItem.amount, 2),
                first_amount_id: global._.isUndefined(feeOneItem) ? null : feeOneItem.id,
                second_year: global._.isUndefined(feeTwoItem) ? null : global._.round(feeTwoItem.amount, 2),
                second_amount_id: global._.isUndefined(feeTwoItem) ? null : feeTwoItem.id,
                third_year: global._.isUndefined(feeThirdItem) ? null : global._.round(feeThirdItem.amount, 2),
                third_amount_id: global._.isUndefined(feeThirdItem) ? null : feeThirdItem.id,
                fourth_year: global._.isUndefined(feeFourthItem) ? null : global._.round(feeFourthItem.amount, 2),
                fourth_amount_id: global._.isUndefined(feeFourthItem) ? null : feeFourthItem.id,
                nsa_first_year: global._.isUndefined(nsaOneItem) ? null : global._.round(nsaOneItem.amount, 2),
                nsa_first_amount_id: global._.isUndefined(nsaOneItem) ? null : nsaOneItem.id,
                nsa_second_year: global._.isUndefined(nsaTwoItem) ? null : global._.round(nsaTwoItem.amount, 2),
                nsa_second_amount_id: global._.isUndefined(nsaTwoItem) ? null : nsaTwoItem.id,
                nsa_third_year: global._.isUndefined(nsaThirdItem) ? null : global._.round(nsaThirdItem.amount, 2),
                nsa_third_amount_id: global._.isUndefined(nsaThirdItem) ? null : nsaThirdItem.id,
                nsa_fourth_year: global._.isUndefined(nsaFourthItem) ? null : global._.round(nsaFourthItem.amount, 2),
                nsa_fourth_amount_id: global._.isUndefined(nsaFourthItem) ? null : nsaFourthItem.id,
                csc_first_year:  global._.isUndefined(cscOneItem) ? null : global._.round(cscOneItem.amount, 2),
                csc_first_amount_id:  global._.isUndefined(cscOneItem) ? null : cscOneItem.id,
                csc_second_year: global._.isUndefined(cscTwoItem) ? null : global._.round(cscTwoItem.amount, 2),
                csc_second_amount_id: global._.isUndefined(cscTwoItem) ? null : cscTwoItem.id,
                csc_third_year: global._.isUndefined(cscThirdItem) ? null : global._.round(cscThirdItem.amount, 2),
                csc_third_amount_id: global._.isUndefined(cscThirdItem) ? null : cscThirdItem.id,
                csc_fourth_year: global._.isUndefined(cscFourthItem) ? null : global._.round(cscFourthItem.amount, 2),
                csc_fourth_amount_id: global._.isUndefined(cscFourthItem) ? null : cscFourthItem.id,
                course_code: global._.isNull(itemCode) ? null : global._.isNull(itemCode.curriculum_course) || global._.isUndefined(itemCode.curriculum_course)? null : global._.isNull(itemCode.curriculum_course.course) ? null : itemCode.curriculum_course.course.course_code
              })
            })
          }
          this.feeItems = items
        }
      },
      skip () {
        return this.skipFeesQuery
      },
      watchLoading (isLoading) {
        this.tuitionfeeLoading = isLoading
      }
    },
  },
  methods: {
    // to fix
    querySelections (val) {
      this.$apollo.query({
        query: GET_FEE_CODES,
        variables: {
          skip: 0,
          take: 500,
          keyword: val ? val : null
        }
      }).then(({ data, loading }) => {
        let items = []
        if (!global._.isUndefined(data)) {
          if (!global._.isEmpty(data.feeCodeQuery.feeCodes)) {
            global._.forEach(data.feeCodeQuery.feeCodes, function (val) {
              items.push({
                ...val,
                fee_code_id: Number(val.id),
                fee_type: global._.isNull(val.fee_type) ? '' : val.fee_type.name
              })
            })
            this.feeCodes = items
          } else {
            this.feeCodes = []
          }
        }
      })
      .catch(error => {
        if (error.graphQLErrors) {
          console.log('graphQLErrors:', error.graphQLErrors)
        }
        if (error.networkError) {
          console.log('networkError:', error.networkError)
        }
        if (error.ServerError) {
          console.log('ServerError:', error.ServerError)
        }
      })
    }
  }
}
