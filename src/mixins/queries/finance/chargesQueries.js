import { GET_STUDENT_CHARGES, GET_SY, GET_SEM_BY_CURRICULUM } from '@/graphql/admin/queries'
import { GET_ACCOUNT_LIST } from '@/graphql/finance/queries'
import { mapGetters } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
  },
  apollo: {
    studentChargesQuery: {
      query: GET_STUDENT_CHARGES,
      variables () {
        return {
          skip: 0,
          take: this.take,
          user_id: global._.isUndefined(this.registrar) ? null : this.registrar.user_info.id,
          sy_id: null,
          semester_id: null
        }
      },
      result ({ data }) {
        let items = []
        if (!global._.isUndefined(data)) {
          this.chargesCount = data.studentChargesQuery.count
          global._.forEach(data.studentChargesQuery.studentCharge, function (val) {
            items.push({
              ...val,
              account_code: val.accountcode ? val.accountcode.account_code : '',
              account_code_id: Number(val.account_code_id),
              account_item: {
                ...val.accountcode,
                account_code_id: Number(val.account_code_id)
              },
              semester: val.sem ? val.sem.semester : '',
              school_year: val.school_year ? `${val.school_year.school_year_from} - ${val.school_year.school_year_to}` : ''
            })
          })
          this.chargesItems = items
        }
      },
      skip () {
        return this.skipChargesQuery
      },
      watchLoading (isLoading) {
        this.chargesLoading = isLoading
      }
    },
    syLists: {
      query: GET_SY,
      result ({ data }) {
        const school_years = []
        if (data) {
          global._.forEach(data.syLists, function (value) {
            school_years.push({
              school_year_id: value.id,
              school_year: `${value.school_year_from} - ${value.school_year_to}`
            })
          })
          this.years = school_years
        }
      }
    },
    semesterCurriculum: {
      query: GET_SEM_BY_CURRICULUM,
      variables () {
        return {
          school_year_id: this.formData.school_year_id
        }
      },
      result ({ data }) {
        let curr_semester = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.semesterCurriculum, function (value) {
            curr_semester.push({
              semester_id: value.id,
              semester: value.semester
            })
          })
          this.currSemesterItems = curr_semester
        }
      },
      skip () {
        return this.sySemesterQuery
      }
    }
  },
  methods: {
    querySelections (val) {
      this.$apollo.query({
        query: GET_ACCOUNT_LIST,
        variables: {
          keyword: val ? val : null
        }
      }).then(({ data, loading }) => {
        let accounts = []
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.accountCodeLists)) {
            global._.forEach(data.accountCodeLists, function (val) {
              accounts.push({
                ...val,
                account_code: val.account_code,
                account_code_id: Number(val.id)
              })
            })
            this.accounts = accounts
          } else {
            this.accounts = []
          }
        }
      })
      .catch(error => {
        if (error.graphQLErrors) {
          console.log('graphQLErrors:', error.graphQLErrors)
        }
        if (error.networkError) {
          console.log('networkError:', error.networkError)
        }
        if (error.ServerError) {
          console.log('ServerError:', error.ServerError)
        }
      })
    }
  }
}