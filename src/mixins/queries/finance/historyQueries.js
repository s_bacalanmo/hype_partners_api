
import { CLASS_SCHED_BY_STUDENT } from '@/graphql/admin/queries'
import { mapGetters } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('user', ['student_data', 'registrar', 'user']),
    ...mapGetters('filters', ['academic_year_id', 'semester_id']),
    skipstudentclassQuery () {
      return !Boolean(this.registrar.user_info.id && this.registrar.semester_id && this.registrar.program_id && this.registrar.curriculum_id && this.registrar.year_level_id && this.user.revision_id)
    }
  },
  apollo: {
    classSchedByStudent: {
      query: CLASS_SCHED_BY_STUDENT,
      variables () {
        return {
          input: {
            user_id: this.registrar.user_info.id,
            semester_id: this.semester_id ? this.semester_id : this.registrar.semester_id,
            program_id: this.registrar.program_id,
            curriculum_id: this.academic_year_id ? this.academic_year_id : this.registrar.curriculum_id,
            year_level_id: this.registrar.year_level_id,
            revision_id: this.user.revision_id
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let coursetab = []
          this.items = []
          if (!global._.isNull(data.classSchedByStudent)) {
            this.count = Number(data.classSchedByStudent.schedules.length)
            this.total_units = Number(data.classSchedByStudent.total_units)
            this.units_added = Number(data.classSchedByStudent.credit_units)
            global._.forEach(data.classSchedByStudent.schedules, function (val) {
              coursetab.push({
                ...val,
                course_code: global._.isNull(val.course) ? '' : val.course.course_code

              })
            })
            this.items = coursetab
          }
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      },
      skip () {
        return this.skipstudentclassQuery
      }
    }
  }
}