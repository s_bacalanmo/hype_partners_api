
import { GET_ASSESSMENT } from '@/graphql/admin/queries'
import { GET_PERIOD, GET_STUDENT_LEDGER  } from '@/graphql/finance/queries'
import { mapGetters } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('user', ['student_data', 'registrar']),
  },
  apollo: {
    studentAssessmentQuery: {
      query: GET_ASSESSMENT,
      variables () {
        return {
          user_id: this.registrar.user_info.id,
          semester_id: this.semester_id ? this.semester_id : this.registrar.semester_id,
          program_id: this.registrar.program_id ? this.registrar.program_id : null,
          year_level_id: this.registrar.year_level_id || !global._.isUndefined(this.registrar.year_level_id) ? this.registrar.year_level_id : null,
          school_year_id: this.academic_year_id ? this.academic_year_id : this.registrar.school_year_id,
          revision_id: this.registrar.revision_id ? this.registrar.revision_id : null,
          skip: 0,
          take: this.take,
          type: !this.registrar.scheme ? 'Fee' : this.registrar.scheme === '311' ? 'CSC' : 'NSA'
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          if (!global._.isEmpty(data.studentAssessmentQuery)) {
            if (!global._.isNull(data.studentAssessmentQuery.studentAssessment)) {
              this.studentFeeID = data.studentAssessmentQuery.studentAssessment.student_fee_id
              let total_tuition = Number(data.studentAssessmentQuery.tuition_lec) + Number(data.studentAssessmentQuery.tuition_lab)  + Number(data.studentAssessmentQuery.tuition_units)
              items.push({
                fee_description: 'Tuition Fee',
                fee_type: 'Tuition Fee',
                fee_amount: global._.round(total_tuition, 2)
              })
              this.due_per_exam = Number(data.studentAssessmentQuery.due_per_exam)
              this.assessCount = Number(data.studentAssessmentQuery.studentAssessment.student_fees.fees.fee_student_bridge)
              let studentItem = data.studentAssessmentQuery.studentAssessment.student_fees.fees.fee_student_bridge.filter(item => item.status === 'Active')
              let schemeType = this.registrar.scheme === '311' ? 'Regular' : this.registrar.scheme === '211' ? 'Company' : 'Regular'
              if (!global._.isNull(studentItem) || !global._.isUndefined(studentItem) ) {
                // miscellaneous fees
                let sortMiscItems = studentItem.filter(item => item.fee_code ? item.fee_code.type === 'Standard School Fees' && !global._.includes(item.fee_code.description, schemeType) : '')
                const miscFees = sortMiscItems.map(item => !global._.isNull(item.fee_student_sy) ? Number(item.fee_student_sy.amount) : 0)
                let totalMisc = global._.reduce(miscFees, function (sum, num) {
                   return sum + num
                  })
                items.push({
                  fee_description: 'Miscellaneous Fees',
                  fee_type: 'Standard School Fees',
                  fee_amount: global._.round(totalMisc, 2)
                })
                global._.forEach(studentItem, function (val) {
                  // registration fee
                  if (!global._.isNull(val.fee_code)) {
                    if (global._.includes(val.fee_code.description, schemeType) && global._.includes(val.fee_code.code, 'reg')) {
                      items.push({
                        fee_description: !global._.isNull(val.fee_code) ? val.fee_code.description : '',
                        fee_type: !global._.isNull(val.fee_code) ? val.fee_code.type : '',
                        fee_amount: !global._.isNull(val.fee_student_sy) ? global._.round(val.fee_student_sy.amount, 2) : 0.00
                      })
                    // tuition fee
                    } else if (val.fee_code.type === 'Other School Fees') {
                      items.push({
                        fee_description: !global._.isNull(val.fee_code) ? val.fee_code.description : '',
                        fee_type: !global._.isNull(val.fee_code) ? val.fee_code.type : '',
                        fee_amount: !global._.isNull(val.fee_student_sy) ? global._.round(val.fee_student_sy.amount, 2) : 0.00
                      })
                    }
                  }
                })

                this.assessmentItems = items
              } else {
                this.assessmentItems = []
              }
            } else {
              this.assessmentItems = []
              this.due_per_exam = 0
            }
          } else {
            this.assessmentItems = []
          }
        }
      },
      watchLoading (isLoading) {
        this.assessLoading = isLoading
      },
      skip () {
        return this.skipAssessmentQuery
      }
    },
    getPeriod: {
      query: GET_PERIOD,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.getPeriod)) {
            this.countExamPeriods = Number(data.getPeriod.length)
          }
        }
      },
      skip () {
        return this.skipPeriodQuery
      }
    },
    studentLedgerQuery: {
      query: GET_STUDENT_LEDGER,
      variables () {
        return {
          user_id: this.registrar.user_info.id,
          semester_id: this.semester_id ? this.semester_id : this.registrar.semester_id,
          program_id: this.registrar.program_id,
          year_level_id: this.registrar.year_level_id,
          school_year_id: this.academic_year_id ? this.academic_year_id : this.registrar.school_year_id,
          revision_id: this.registrar.revision_id ? this.registrar.revision_id : null,
          skip: 0,
          take: this.ledgerTake,
          type: !this.registrar.scheme ? 'Fee' : this.registrar.scheme === '311' ? 'CSC' : 'NSA'
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.studentLedgerQuery)) {
            this.ledgerCount = data.studentLedgerQuery.count
            this.totalBalanceItem = Number(data.studentLedgerQuery.total_balance)
            if (!global._.isNull(data.studentLedgerQuery.ledger)) {
              this.ledgerItems = data.studentLedgerQuery.ledger
            } else {
              this.ledgerItems = []
            }
          } else {
            this.ledgerItems = []
          }
        }
      },
      watchLoading (isLoading) {
        this.ledgerLoading = isLoading
      },
      skip () {
        return this.skipLedgerQuery
      }
    }
  }
}