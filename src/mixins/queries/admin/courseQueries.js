import { GET_COURSES_UNDER_CURRICULUM, GET_COURSE_INFO  } from '@/graphql/admin/queries'
import { mapGetters, mapMutations } from 'vuex'
export default {
  computed: {
    ...mapGetters('curricula', ['curricula', 'per_program', 'revisions']),
  },
  methods: {
    ...mapMutations('form', ['setCourseCount'])
  },
  apollo: {
    coursesUnderCurriculum: {
      query: GET_COURSES_UNDER_CURRICULUM,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            year_level_id: global._.isUndefined(this.year_level_id) ? null : this.year_level_id,
            semester_id: global._.isUndefined(this.semester_id) ? null : this.semester_id,
            curriculum_id: this.per_program.id,
            revision_id: this.revisions.id,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let courses = []
          if (!global._.isNull(data.coursesUnderCurriculum)) {
            this.count = Number(data.coursesUnderCurriculum.count)
            this.setCourseCount(this.count)
            global._.forEach(data.coursesUnderCurriculum.courses, function (val) {
              let corequisite = val.requisites.filter(item => item.type === 1)
              let prerequisite = val.requisites.filter(item => item.type === 2)
              courses.push({
                ...val,
                level: global._.isEmpty(val.year_level) ? '' : val.year_level.year_level,
                semester_name: global._.isEmpty(val.semester) ? '' : val.semester.semester,
                course_code: global._.isEmpty(val.course) ? '' : val.course.course_code,
                pre_requisite: global._.isEmpty(prerequisite) ? '' : prerequisite.map(item => item.requisite_id),
                co_requisite: global._.isEmpty(corequisite) ? '' : corequisite.map(item => item.requisite_id),
                pre_requisite_name: global._.isEmpty(prerequisite) ? '' : prerequisite.map(item => item.course ? item.course.course_code : ''),
                co_requisite_name: global._.isEmpty(corequisite) ? '' : corequisite.map(item => item.course ? item.course.course_code : ''),
                course_item: {
                  course_description: global._.isEmpty(val) ? '' : val.course_description,
                  course_id: val.course_id,
                  course_code: global._.isEmpty(val.course) ? '' : val.course.course_code
                }
              })
            })
            this.dataList = courses
          }
        }
      },
      skip () {
        return this.skipCourseQuery
      },
      watchLoading (isLoading) {
        this.courseLoading = isLoading
      }
    },
    coursesList: {
      query: GET_COURSE_INFO,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? null : this.setSearch,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let courses = []
          this.count = Number(data.coursesList.count)
          global._.forEach(data.coursesList.courses, function (val) {
            courses.push(val)
          })
          this.courseMenuItems = courses
        }
      },
      watchLoading (isLoading) {
        this.courseMenuLoading = isLoading
      },
      skip () {
        return this.skipCourseMenuQuery
      }
    }
  }
}