import { GET_TOTAL_STUDENTS_PER_PROGRAM, GET_TOTAL_STUDENTS, GET_TOTAL_TEACHERS, GET_APPLICANTS_ENROLLMENT, GET_CLASS, GET_SCHOOL_YEAR, GET_STUD_POPULATION } from '@/graphql/admin/queries'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [defaultMixins],
  apollo: {
    totalTeachers: {
      query: GET_TOTAL_TEACHERS,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          this.teacherCount = Number(data.totalTeachers)
        }
      },
      skip () {
        return this.skipTeacherQuery
      }
    },
    totalStudents: {
      query: GET_TOTAL_STUDENTS,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          this.studentCount = Number(data.totalStudents)
        }
      },
      skip () {
        return this.skipStudentsQuery
      }
    },
    filterEnrollmentMIS: {
      query: GET_APPLICANTS_ENROLLMENT,
      variables () {
        return {
          input: {
            keyword: null,
            school_year_id: 0,
            program_id: 0,
            year_level_id: 0,
            semester_id: 0,
            status: null,
            section_id: 0,
            skip: 0,
            take: 0
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          this.applicantCount = Number(data.filterEnrollmentMIS.count)
        }
      },
      skip () {
        return this.skipEnrolleesQuery
      }
    },
    classWithFilter: {
      query: GET_CLASS,
      variables () {
        return {
          input: {
            keyword: null,
            school_year_id: 0,
            program_id: 0,
            year_level_id: 0,
            semester_id: 0,
            room_id: [],
            teacher_id: [],
            course_id: [],
            units: [],
            section_id: [],
            day: [],
            time_from: null,
            time_to: null,
            class_code_from: 0,
            class_code_to: 0,
            skip: 0,
            take: 0
          }
        }
      },
      result ({ data }) {
        let self = this
        if (!global._.isUndefined(data)) {
          this.classCount = Number(data.classWithFilter.count)
        }
      },
      skip () {
        return this.skipclassQuery
      }
    },
    studentPerProgram: {
      query: GET_TOTAL_STUDENTS_PER_PROGRAM,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          if (!global._.isNull(data.studentPerProgram)) {
            global._.forEach(data.studentPerProgram, function (val, key) {
              let color = Math.floor(Math.random()*16777215).toString(16)
               items.push({
                ...val,
                color: `#${color}`
               })
            })
            this.programs = items
          } else {
            this.programs = []
          }
        }
      },
      skip () {
        return this.skipstudPerProgramQuery
      }
    },
    studentPopulation: {
      query: GET_STUD_POPULATION,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (data.studentPopulation) {
            let items = []
            this.allStudents = data.studentPopulation.x_axis
            const sortItems = data.studentPopulation.data.filter(item => item.school_year !== '-')
            this.sortData(items, sortItems)
            this.years = items.map(item => item.school_year)
            this.countEnrolled = items.map(item => item.enrolled)
            this.countPotential = items.map(item => item.potential)
          }
        }
      },
      skip () {
        return this.skipstudPopulationQuery
      }
    }
  },
  methods: {
    sortData (items, iteratees) {
      let sortYearArray = global._.orderBy(iteratees, ['school_year', 'count'], ['asc', 'desc'])
      let groupItems = global._.groupBy(sortYearArray, 'school_year')
      global._.forEach(groupItems, function (val, key) {
        let enrolled = val.filter(item => item.count && item.type === 'enrolled')
        let potential = val.filter(item => item.count && item.type === 'potential')
        items.push({
          school_year: key,
          enrolled: !global._.isEmpty(enrolled.map(item => item.count)) ? Number(enrolled.map(item => item.count)) : 0,
          potential: !global._.isEmpty(potential.map(item => item.count)) ? Number(potential.map(item => item.count)) : 0
        })
      })
      return items
    }
  }
}
