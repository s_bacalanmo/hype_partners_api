
import { GET_SUBJECT_EQUIVALENCY, GET_CURRICULUM_COURSES, GET_YEAR_LEVEL, GET_SEMESTER, GET_COURSE_CURRICULUM } from '@/graphql/admin/queries'
export default {
  apollo: {
    subjectEquivalencyInfo: {
      query: GET_SUBJECT_EQUIVALENCY,
      variables () {
        return {
          user_id: this.registrar.user_info.id
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          global._.forEach(data.subjectEquivalencyInfo, function (val) {
            items.push({
              ...val,
              dcsp_course_item: {
                dcsp_course_description: val.dcsp_course_description,
                dcsp_course_equivalency: val.dcsp_course_equivalency,
                dcsp_units: val.dcsp_units
              },
              dcsp_course_description: val.dcsp_course_description
            })
          })
          this.items = items
          this.itemsLength = this.items.length
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      }
    },
    courseCurriculum: {
      query: GET_COURSE_CURRICULUM,
      variables () {
        return {
          curriculum_id: this.registrar.curriculum_id,
          program_id: this.registrar.program_id,
          revision_id: this.registrar.revision_id,
          course_code: this.search_course
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          global._.forEach(data.courseCurriculum, function (val) {
            items.push({
              ...val,
              dcsp_course_item: {
                dcsp_course_description: val.dcsp_course_description,
                dcsp_course_equivalency: val.code ? global._.trim(val.code.dcsp_course_equivalency) : '',
                dcsp_units: val.dcsp_units
              },
              dcsp_course_equivalency: val.code ? global._.trim(val.code.dcsp_course_equivalency) : ''
            })
          })
          this.courses = items
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      },
      skip () {
        return !this.isskipCourseQuery
      }
    }
  },
  computed: {
    isskipCourseQuery () {
      return Boolean(!global._.isUndefined(this.registrar.curriculum_id) && !global._.isUndefined(this.registrar.revision_id) && !global._.isUndefined(this.registrar.program_id))
    }
  },
  methods: {
    fetchYearLevels () {
      this.$apollo.query({
        query: GET_YEAR_LEVEL
      }).then((data) => {
        if (data) {
          let items = []
          global._.forEach(data.data.yearlevel, function(val) {
            items.push(val)
          })
          this.levels = items
        }
      })
      .catch(error => {
        console.log(error)
      })
    },
    fetchSemesters () {
      this.$apollo.query({
        query: GET_SEMESTER
      }).then((data) => {
        if (data) {
          let items = []
          global._.forEach(data.data.semester, function(val) {
            items.push(val)
          })
          this.semesters = items
        }
      })
      .catch(error => {
        console.log(error)
      })
    }
  }
}
