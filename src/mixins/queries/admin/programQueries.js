import { GET_PROGRAM_FILTERED } from '@/graphql/admin/queries'
export default {
  apollo: {
    programsWithFilter: {
      query: GET_PROGRAM_FILTERED,
      variables () {
        return {
          input: {
            skip: this.skip,
            take: this.take,
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          this.count = Number(data.programsWithFilter.count)
          global._.forEach(data.programsWithFilter.programs, function (val) {
            items.push(val)
          })
          this.dataList = items
        }
      },
      skip () {
        return this.skipProgramFilteredQuery
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      }
    }
  }
}
