import { GET_CLASS, GET_SCHOOL_YEAR, GET_ROOM_DIVISION, GET_ROOM_LOCATION, GET_YEAR_LEVEL, GET_PROGRAM_LIST, GET_ROOM_LIST, GET_CURR_REVISIONS, GET_SY_PROGRAMS, GET_SEM_BY_CURRICULUM, GET_SECTION_BY_IDS, GET_FILTERED_SECTION, GET_ALL_TEACHERS, GET_CURRICULUM_COURSES, GET_SY, GET_SCHOOL_YEAR_PROGRAM, GET_SCHOOLYEAR_YEAR_LEVEL, GET_SCHOOLYEAR_SEMESTER } from '@/graphql/admin/queries'
import defaultMixins from '@/mixins/index'
import { getYear } from 'date-fns'
export default {
  mixins: [
    defaultMixins
  ],
  apollo: {
    syLists: {
      query: GET_SY,
      result ({ data }) {
        const years = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.syLists, function (value) {
            years.push({
              school_year_id: Number(value.id),
              school_year: `${value.school_year_from} - ${value.school_year_to}`
            })
          })
          this.years = years
        }
      }
    },
    yearlevel: {
      query: GET_YEAR_LEVEL,
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.yearlevel, function (value) {
            items.push({
              year_level_id: Number(value.id),
              year_level: value.year_level
            })
          })
          this.defaultYearlevel = items
        }
      }
    },
    roomLists: {
      query: GET_ROOM_LIST,
      variables () {
        return {
          keyword: null
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.roomLists, function (val) {
            items.push({
              capacity: val.capacity,
              room_id: Number(val.id),
              room_name: val.number
            })
          })
          this.defaultRooms = items
        }
      },
      skip () {
        return this.skiproomListsQuery
      }
    },
    programsLists: {
      query: GET_PROGRAM_LIST,
      variables () {
        return {
          keyword: this.search_program
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.programsLists, function (value) {
            items.push({
              program_id: Number(value.id),
              program_code: value.program_code
            })
          })
          this.defaultPrograms = items
        }
      },
      skip () {
        return this.skipprogramListsQuery
      }
    },
    roomLocationLists: {
      query: GET_ROOM_LOCATION,
      variables () {
        return {
          keyword: this.search_location
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.roomLocationLists, function (value) {
            items.push({
              room_location_id: Number(value.id),
              location: value.location
            })
          })
          this.locations = items
        }
      },
      skip () {
        return this.skiproomlocationQuery
      }
    },
    roomDivisionLists: {
      query: GET_ROOM_DIVISION,
      variables () {
        return {
          keyword: this.search_division
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.roomDivisionLists, function (value) {
            items.push({
              room_division_id: Number(value.id),
              name: value.name
            })
          })
          this.divisions = items
        }
      },
      skip () {
        return this.skiproomdivisionQuery
      }
    },
    teachers: {
      query: GET_ALL_TEACHERS,
      variables () {
        return {
          keyword: null
        }
      },
      result ({ data, loading }) {
        if (!global._.isUndefined(data)) {
          let teachers = []
          let self = this
          global._.forEach(data.teachers, function (value) {
            let item = value.user
            if (value.user) {
              teachers.push({
                name: `${item.first_name} ${self.initialLetter(item.middle_name)}${item.last_name}`,
                teacher_id: item.id
              })
            }
          })
          let sortTeachers = teachers.map(item => item.name)
          this.sortNames = sortTeachers
          this.instructors = teachers
        }
      },
      skip () {
        return this.skipTeacherQuery
      }
    },
    sectionsFilterFetch: {
      query: GET_FILTERED_SECTION,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            program_id: global._.isUndefined(this.program_id) ? 0 : this.program_id,
            year_level_id: global._.isUndefined(this.year_level_id) ? 0 : this.year_level_id,
            section_id: global._.isUndefined(this.section) ? 0 : this.section,
            semester_id: global._.isUndefined(this.semester_id) ? 0 : this.semester_id,
            school_year_id: global._.isUndefined(this.academic_year_id) ? 0 : this.academic_year_id,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        const sections = []
        if (!global._.isUndefined(data)) {
          this.count = Number(data.sectionsFilterFetch.count)
          global._.forEach(data.sectionsFilterFetch.sections, function (val) {
            sections.push({
              ...val,
              program_code: global._.isNull(val.program) ? '' : val.program.program_code,
              year_level: global._.isNull(val.year_level) ? '' : val.year_level.year_level
            })
          })
          this.sectionItems = sections
        }
      },
      watchLoading (isLoading) {
        this.sectionLoading = isLoading
      },
      skip () {
        return this.skipSectionQuery
      }
    },
    sections: {
      query: GET_SECTION_BY_IDS,
      variables () {
        return {
          keyword: null,
          year_level_id: this.ids.year_level_id,
          program_id: this.ids.program_id
        }
      },
      result ({ data }) {
        const sections = []
        if (!global._.isUndefined(data)) {
          let sections = []
          global._.forEach(data.sections, function (value) {
            sections.push({
              section_id: Number(value.id),
              section: value.section
            })
          })
          this.classSectionItems = sections
        }
      },
      watchLoading (isLoading) {
        this.sectionBySyLoading = isLoading
      },
      skip () {
        return this.skipSectionBySyQuery
      }
    },
    curriculumBySyProgram: {
      query: GET_SY_PROGRAMS,
      variables () {
        return {
          school_year_id: this.ids.school_year_id,
          program_id: this.ids.program_id
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.curriculumBySyProgram, function (value) {
            items.push({
              curriculum_id: Number(value.id),
              curriculum_code: value.curriculum_code,
              program_id: value.program_id
            })
          })
          this.curriculums = items
        }
      },
      watchLoading (isLoading) {
        this.syProgramsLoading = isLoading
      },
      skip () {
        return this.skipSyProgramsQuery
      },
      error ({ graphQLErrors, networkError }) {
        if (graphQLErrors) {
          console.log('graphQLErrors:', graphQLErrors)
        }
        if (networkError) {
          console.log('networkError:', networkError)
        }
      }
    },
    revisions: {
      query: GET_CURR_REVISIONS,
      variables () {
        return {
          keyword: null,
          curriculum_id: this.ids.curriculum_id
        }
      },
      result ({ data }) {
        const items = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.revisions, function (value) {
            items.push({
              ...value,
              revision: `Revision No. ${Number(value.revision_no)}`,
              revision_id: value.id
            })
          })
          this.setRevisionItem(items)
          this.revisions = items
        }
      },
      skip () {
        return this.skipCurrRevisionsQuery
      }
    },
    schoolYearPrograms: {
      query: GET_SCHOOL_YEAR_PROGRAM,
      variables () {
        return {
          school_year_id: this.ids.school_year_id
        }
      },
      result ({ data }) {
        const programs = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.schoolYearPrograms, function (value) {
            programs.push({
              program_id: value.id,
              program_code: value.program_code
            })
          })
          this.programs = programs
        }
      },
      skip () {
        return this.skipcurriculumProgramQuery
      }
    },
    schoolYearYearLevel: {
      query: GET_SCHOOLYEAR_YEAR_LEVEL,
      variables () {
        return {
          input: {
            school_year_id: this.ids.school_year_id,
            program_id: this.ids.program_id
          }
        }
      },
      result ({ data }) {
        const levels = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.schoolYearYearLevel, function (value) {
            levels.push({
              year_level_id: value.id,
              year_level: value.year_level
            })
          })
          this.levels = levels
        }
      },
      skip () {
        return this.skipcurriculumYearLevelQuery
      }
    },
    schoolYearSemester: {
      query: GET_SCHOOLYEAR_SEMESTER,
      variables () {
        return {
          input: {
            school_year_id: this.ids.school_year_id,
            program_id: this.ids.program_id,
            year_level_id: this.ids.year_level_id
          }
        }
      },
      result ({ data }) {
        const semesters = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.schoolYearSemester, function (value) {
            semesters.push({
              semester_id: value.id,
              semester: value.semester
            })
          })
          this.semesters = semesters
        }
      },
      skip () {
        return this.skipcurriculumSemesterQuery
      }
    },
    curriculumCourses: {
      query: GET_CURRICULUM_COURSES,
      variables () {
        return {
          input: {
            curriculum_id: this.ids.curriculum_id,
            revision_id: this.ids.revision_id,
            year_level_id: this.ids.year_level_id,
            semester_id: this.ids.semester_id ? this.ids.semester_id : null
          }
        }
      },
      result ({ data }) {
        let courses = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.curriculumCourses, function (value) {
            courses.push({
              course_code: global._.trim(value.course_code),
              course_id: value.course_id,
              lec_hours: value.lecture_schedule,
              lab_hours: value.laboratory_schedule
            })
          })
          this.courseItems = courses
        }
      },
      skip () {
        return this.skipcurriculumCoursesQuery
      }
    },
    semesterCurriculum: {
      query: GET_SEM_BY_CURRICULUM,
      variables () {
        return {
          school_year_id: this.ids.school_year_id
        }
      },
      result ({ data }) {
        let curr_semester = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.semesterCurriculum, function (value) {
            curr_semester.push({
              semester_id: value.id,
              semester: value.semester
            })
          })
          this.currSemesterItems = curr_semester
        }
      },
      skip () {
        return this.skipsemesterCurriculumQuery
      }
    },
    syWithFilter: {
      query: GET_SCHOOL_YEAR,
      variables () {
        return {
          input: {
            skip: 0,
            take: this.take,
            school_year_id: this.ids.school_year_id
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.syWithFilter)) {
            let item = data.syWithFilter.sy.find(item  => item.id === this.ids.school_year_id)
            if (!global._.isUndefined(item)) {
              this.curricula = item
            }
          }
        }
      },
      skip () {
        return this.skipcalendarQuery
      }
    },
    classWithFilter: {
      query: GET_CLASS,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            school_year_id: global._.isUndefined(this.academic_year_id) ? null : this.academic_year_id,
            program_id: global._.isUndefined(this.program_id) ? null : this.program_id,
            year_level_id: global._.isUndefined(this.year_level_id) ? null : this.year_level_id,
            semester_id: global._.isUndefined(this.semester_id) ? null : this.semester_id,
            room_id: this.room,
            teacher_id: this.teacher,
            course_id: this.course,
            units: this.units,
            section_id: this.section,
            day: this.day,
            time_from: global._.isEmpty(this.time_from) ? '' : this.time_from,
            time_to: global._.isEmpty(this.time_to) ? '' : this.time_to,
            class_code_from: global._.isEmpty(this.class_code) ? 0 : this.class_code[0],
            class_code_to: global._.isEmpty(this.class_code) ? 0 : this.class_code[1],
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        let self = this
        if (!global._.isUndefined(data)) {
          const classes = []
          this.getCurriculum({ school_year_id: this.academic_year_id })
          this.classCount = Number(data.classWithFilter.count)
          if (!global._.isEmpty(data.classWithFilter.class)) {
            global._.forEach(data.classWithFilter.class, function (val) {
              let sortItem = global._.omit(val, 'section_id')
              let sortLecTime = val.lec_schedules_sorted.map(item => {
                return `${item.time_from_meridian} - ${item.time_to_meridian}`
              })
              let sortLabTime = val.lab_schedules_sorted.map(item => {
                return `${item.time_from_meridian} - ${item.time_to_meridian}`
              })
              const sectionLec = val.lec_schedules_sorted.map((item, idx) => {
                let sections = item.section.map(item => item.section.section)
                if (item.section) return sections
                return ''
              })
              const sectionLab = val.lab_schedules_sorted.map((item, idx) => {
                let sections = item.section.map(item => item.section.section)
                if (item.section) return sections
                return ''
              })
              const daysLab = val.lab_schedules_sorted.map(item => item.day)
              const daysLec = val.lec_schedules_sorted.map(item => item.day)
              const roomLec = val.lec_schedules_sorted.map(item => {
                if (item.room) return item.room.number
                return ''
              })
              const roomLab = val.lab_schedules_sorted.map(item => {
                if (item.room) return item.room.number
                return ''
              })
              const instructorLab = val.lab_schedules_sorted.map(item => item.teacher_fullname)
              const instructorLec = val.lec_schedules_sorted.map(item => item.teacher_fullname)
              classes.push({
                ...sortItem,
                section_id: global._.compact(global._.uniq(val.section_id)),
                class_code: val.class_code,
                course_code: global._.isNull(val.course) ? '' : val.course.course_code,
                course_id: val.course ? val.course.id : null,
                year_level_id: val.year_level_id,
                semester_id: val.semester_id,
                no_data: '',
                curriculum_id: val.curriculum_id,
                program_id: val.program_id,
                section: Number(val.lec_hours) !== 0 ? global._.uniq(global._.flatten(sectionLec)) : global._.uniq(global._.flatten(sectionLab)),
                program_code: global._.isNull(val.program) ? '' : val.program.program_code,
                lec_time: global._.uniq(sortLecTime),
                lab_time: global._.uniq(sortLabTime),
                lec_days: global._.uniq(daysLec),
                lab_days: global._.uniq(daysLab),
                lec_room: global._.uniq(roomLec),
                lab_room: global._.uniq(roomLab),
                lec_instructor: global._.uniq(instructorLec),
                lab_instructor: global._.uniq(instructorLab),
                type: global._.isEmpty(val.lab_schedules) && global._.isEmpty(val.lec_schedules) ? 'OJT' : 'Lec/Lab'
              })
            })
            this.items = classes
          } else {
            this.items = []
          }
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      },
      skip () {
        return this.skipclassWithFilterQuery
      },
      error ({ graphQLErrors, networkError, ServerError }) {
        if (graphQLErrors) {
          console.log('graphQLErrors:', graphQLErrors)
        }
        if (networkError) {
          console.log('networkError:', networkError)
        }
        if (ServerError) {
          console.log('ServerError:', ServerError)
        }
      }
    },
  },
  methods: {
    getCurriculum (ids) {
      // for calendar
      if (ids.school_year_id) {
        this.ids = ids
        this.skipcalendarQuery = false
        this.$apollo.queries.syWithFilter.refetch()
      } else {
        this.skipcalendarQuery = true
      }
    },
    getSybyProgram (ids) {
      if (ids.school_year_id && ids.program_id) {
        this.ids = ids
        this.skipSyProgramsQuery = false
        this.$apollo.queries.curriculumBySyProgram.refetch()
      } else {
        this.skipSyProgramsQuery = true
      }
    },
    getCurrRevisions (ids) {
      if (ids.curriculum_id) {
        this.ids = ids
        this.skipCurrRevisionsQuery = false
        this.$apollo.queries.revisions.refetch()
      } else {
        this.skipCurrRevisionsQuery = true
      }
    },
    getSection (ids) {
      if (ids.school_year_id && ids.program_id && ids.year_level_id && ids.semester_id) {
        this.ids = ids
        this.skipSectionBySyQuery = false
        this.$apollo.queries.sections.refetch()
      } else {
        this.skipSectionBySyQuery = true
      }
    },
    getcurrPrograms (ids) {
      if (ids.school_year_id) {
        this.ids = ids
        this.skipcurriculumProgramQuery = false
        this.$apollo.queries.schoolYearPrograms.refetch()
      } else {
        this.skipcurriculumProgramQuery = true
      }
    },
    getcurrYearLevel (ids) {
      if (ids.school_year_id && ids.program_id) {
        this.ids = ids
        this.skipcurriculumYearLevelQuery = false
        this.$apollo.queries.schoolYearYearLevel.refetch()
      } else {
        this.skipcurriculumYearLevelQuery = true
      }
    },
    getcurrSemester (ids) {
      if (ids.school_year_id && ids.program_id && ids.year_level_id) {
        this.ids = ids
        this.skipcurriculumSemesterQuery = false
        this.$apollo.queries.schoolYearSemester.refetch()
      } else {
        this.skipcurriculumSemesterQuery = true
      }
    },
    getcurrCourses (ids) {
      if (ids.curriculum_id && ids.revision_id && ids.year_level_id && ids.semester_id) {
        this.ids = ids
        this.skipcurriculumCoursesQuery = false
        this.$apollo.queries.curriculumCourses.refetch()
      } else {
        this.skipcurriculumCoursesQuery = true
      }
    },
    getsemesterByCurr (ids) {
      if (ids.school_year_id) {
        this.ids = ids
        this.skipsemesterCurriculumQuery = false
        this.$apollo.queries.semesterCurriculum.refetch()
      } else {
        this.skipsemesterCurriculumQuery = true
      }
    },
    setIds (form) {
      let formItem = form.find(item => item)
      if (!global._.isUndefined(formItem)) {
        this.getSybyProgram(formItem)
        this.getCurrRevisions(formItem)
        this.getcurrPrograms(formItem)
        this.getcurrYearLevel(formItem)
        this.getcurrSemester(formItem)
        this.getcurrCourses(formItem)
        this.getSection(formItem)
      }
    },
    setRevisionItem (items) {
      if (items) {
        let item = this.curriculums.find(item => Number(item.curriculum_id) === Number(this.formData[0].curriculum_id))
        let yearNow = getYear(new Date())
        let revisionItem = items.find(item => Number(item.effective_school_year_to) <= Number(yearNow))
        if (!global._.isUndefined(revisionItem)) {
          this.fields[3].disabled = true
          this.fields[3].clearable = false
          if (global._.isUndefined(item)) {
            this.$set(this.formData[0], 'revision_id', null)
          } else {
            this.$set(this.formData[0], 'revision_id', revisionItem.revision_id)
          }
        } else {
          this.fields[3].disabled = false
        }
      } else {
        this.fields[3].disabled = false
        this.$set(this.formData[0], 'revision_id', null)
      }
    },
    defaultSchedules () {
      this.createLab = false
      this.createLec = false
      this.courseItems = []
      this.type = ''
      this.dayItems = []
      this.selectedLec = []
      this.dayLabItems = []
      this.selectedLab = []
      this.lecErrors = []
      this.labErrors = []
      this.errorMessages = []
    },
    setSchedules () {
      let lecHours = []
      let labHours = []
      if (!global._.isEmpty(this.formData[0].lec_schedules)) {
        global._.forEach(this.formData[0].lec_schedules, function (val) {
          lecHours.push({
            type: val.type,
            day: val.day,
            from: val.time_from,
            to: val.time_to,
            teacher_id: val.teacher_id,
            room_id: val.room_id,
            id: val.id,
            capacity: val.capacity
          })
        })
        this.dayItems = lecHours
        this.selectedLec = this.formData[0].lec_schedules.map(item => item.day)
        if (this.selectedLec.length > 0) {
          this.createLec = true
        } else {
          this.createLec = false
        }
      }
      if (!global._.isEmpty(this.formData[0].lab_schedules)) {
        global._.forEach(this.formData[0].lab_schedules, function (val) {
          labHours.push({
            type: val.type,
            day: val.day,
            from: val.time_from,
            to: val.time_to,
            teacher_id: val.teacher_id,
            room_id: val.room_id,
            id: val.id,
            capacity: val.capacity
          })
        })
        this.dayLabItems = labHours
        this.selectedLab = this.formData[0].lab_schedules.map(item => item.day)
        if (this.selectedLab.length > 0) {
          this.createLab = true
        } else {
          this.createLab = false
        }
      }
    },
    AddItem (size, dayItem, index) {
      const hasItem = this.selectedLec.filter(item => item === dayItem)
      if (hasItem.length === 1) {
        this.dayItems.push({
          type: 'Lec',
          day: dayItem,
          from: '',
          to: '',
          teacher_id: '',
          room_id: '',
          capacity: ''
        })
      } else {
        if (global._.isEmpty(hasItem)) {
          const currItem = this.dayItems.find(item => item.day === dayItem)
          const itemIndex = this.dayItems.indexOf(currItem)
          if (itemIndex !== -1) {
            this.$delete(this.dayItems, itemIndex)
          }
        }
      }
      if (size > 1) {
        this.sameSchedFunc(this.dayItems)
        this.schedule = this.sameSchedFunc(this.dayItems)
      }
    },
    AddLabItem (size, dayItem, index) {
      const hasItem = this.selectedLab.filter(item => item === dayItem)
      if (hasItem.length === 1) {
        this.dayLabItems.push({
          type: 'Lab',
          day: dayItem,
          from: '',
          to: '',
          teacher_id: '',
          room_id: '',
          capacity: ''
        })
      } else {
        if (global._.isEmpty(hasItem)) {
          const currItem = this.dayLabItems.find(item => item.day === dayItem)
          const itemIndex = this.dayLabItems.indexOf(currItem)
          if (itemIndex !== -1) {
            this.$delete(this.dayLabItems, itemIndex)
          }
        }
      }
      if (size > 1) {
        this.scheduleLab = this.sameSchedFunc(this.dayLabItems)
      }
    },
    classErrorFunc (error_data) {
      if (error_data.class_schedule_conflicts) {
        this.errorDialog = true
        this.errorMessages = this.conflictFunc('conflict', error_data.class_schedule_conflicts, '')
      } else {
        const lec = error_data.lec_schedules
        const lab = error_data.lab_schedules
        if (!global._.isUndefined(lec) && global._.isUndefined(lab)) {
          const isLecError = lec.map(item => item.error)
          if (!global._.isEmpty(isLecError)) {
            this.errorDialog = true
            this.errorMessages = this.conflictFunc('conflict', global._.compact(isLecError), '')
          }
          const sortItem = lec.map(item => item[0])
          const isLecHours = sortItem.map(item => item.hours)
          const isLecFromTime = sortItem.map(item => item.from)
          if (!global._.isEmpty(global._.compact(isLecHours))) {
            this.errorDialog = true
            this.errorMessages = this.conflictFunc('schedule', global._.uniq(isLecHours), '')
          }
          if (!global._.isEmpty(global._.compact(isLecFromTime))) {
            this.labErrors = []
            this.lecErrors = lec[0]
          }
        } else if (global._.isUndefined(lec) && !global._.isUndefined(lab)) {
          const isLabError = lab.map(item => item.error)
          if (!global._.isEmpty(isLabError)) {
            this.errorDialog = true
            this.errorMessages = this.conflictFunc('conflict', global._.compact(isLabError), '')
          }
          const sortLabItem = lab.map(item => item[0])
          const isLabHours = sortLabItem.map(item => item.hours)
          const isLabFromTime = sortLabItem.map(item => item.from)

          if (!global._.isEmpty(global._.compact(isLabHours))) {
            this.errorDialog = true
            return this.errorMessages = this.conflictFunc('schedule', '', global._.uniq(isLabHours))
          }
          if (!global._.isEmpty(global._.compact(isLabFromTime))) {
            this.lecErrors = []
            this.labErrors = lab[0]
          }
        } else {
          const isLecError = lec.map(item => item.error)
          const isLabError = lab.map(item => item.error)
          if (!global._.isEmpty(global._.compact(isLecError)) || !global._.isEmpty(global._.compact(isLabError))) {
            this.errorDialog = true
            this.errorMessages = this.conflictFunc('conflict', global._.compact(isLecError), global._.compact(isLabError))
          }
          const sortItem = lec.map(item => item[0])
          const sortLabItem = lab.map(item => item[0])
          const isLecFromTime = sortItem.map(item => item.from)
          const isLabFromTime = sortLabItem.map(item => item.from)
          const isLecHours = sortItem.map(item => item.hours)
          const isLabHours = sortLabItem.map(item => item.hours)
          if (!global._.isEmpty(global._.compact(isLecHours)) || !global._.isEmpty(global._.compact(isLabHours))) {
            this.errorDialog = true
            this.errorMessages = this.conflictFunc('schedule', global._.uniq(isLecHours), global._.uniq(isLabHours))
          }
          if (!global._.isEmpty(global._.compact(isLecFromTime)) && global._.isEmpty(global._.compact(isLabFromTime))) {
            this.labErrors = []
            this.lecErrors = lec[0]
          } else if (global._.isEmpty(global._.compact(isLecFromTime)) && !global._.isEmpty(global._.compact(isLabFromTime))) {
            this.lecErrors = []
            this.labErrors = lab[0]
          } else {
            this.lecErrors = lec[0]
            this.labErrors = lab[0]
          }
        }
      }
    },
    conflictFunc (type, errorItem, otherErrors) {
      if (type === 'conflict') {
        let errorMessages = []
        global._.forEach(errorItem, function (val) {
          if (val) {
            errorMessages.push({
              message: val
            })
          }
        })
        return errorMessages
      } else {
        let labMessages = []
        let lecMessages = []
        if (!global._.isEmpty(errorItem)) {
          global._.forEach(errorItem, function (val) {
            if (val) {
              lecMessages.push({
                message: val
              })
            }
          })
        }
        if (!global._.isEmpty(otherErrors)) {
          global._.forEach(otherErrors, function (val) {
            if (val) {
              labMessages.push({
                message: val
              })
            }
          })
        }
        let errorMessages = global._.concat(labMessages, lecMessages)
        return errorMessages
      }
    },
    sameSchedFunc (val) {
      let schedItems = {}
      schedItems = val.find(item => item)
      return schedItems
    },
    doSameSchedFunc (iteratee, item) {
      let items = []
      global._.forEach(iteratee, function (val, key) {
        if (val.id) {
          items.push({
            type: val.type,
            day: val.day,
            from: item.from,
            to: item.to,
            teacher_id: item.teacher_id,
            room_id: item.room_id ? item.room_id.room_id : null,
            id: val.id,
            capacity: item.capacity
          })
        } else {
          items.push({
            type: val.type,
            day: val.day,
            from: item.from,
            to: item.to,
            teacher_id: item.teacher_id,
            room_id: item.room_id ? item.room_id.room_id : null,
            capacity: item.capacity
          })
        }
      })
      return items
    }
  }
}