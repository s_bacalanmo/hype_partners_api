import { GET_ENROLLMENT_EXAM } from '@/graphql/queries'
export default {
  apollo: {
    studentInfo: {
      query: GET_ENROLLMENT_EXAM,
      variables () {
        return {
          id: this.registrar.user_info.id
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let payload = {
            id: this.registrar.user_info.id,
            icon: 'mdi-check-circle'
          }
          if (!global._.isNull(data.studentInfo)) {
            if (data.studentInfo.student_info) {
              let personalItems = global._.merge(data.studentInfo.spdi_checklist, data.studentInfo.student_info)
              this.setPersonalData(payload, personalItems)
            } else {
              this.setPersonalData(payload, data.studentInfo.spdi_checklist)
            }
            this.setFamData(payload, data.studentInfo.spdi_family_bg)
            this.setEducData(payload, data.studentInfo.spdi_education_bg)
          } else {
            this.fathersForm[0] = { ...payload, ...this.fathersItem }
            this.mothersForm[0] = { ...payload, ...this.mothersItem }
            this.guardiansForm[0] = { ...payload, ...this.guardiansItem }
            this.educationForm[0] = { ...payload, ...this.educationItem }
            this.personalForm[0] = { ...payload, ...this.personalItem, student_id_number: '' }
            this.careerForm[0] = { ...payload, ...this.careerItem }
            this.checklistForm[0] = { ...payload, ...this.checklistItem }
          }
        }
      },
      watchLoading (isLoading) {
        this.isLoading = isLoading
      },
      skip () {
        return global._.isUndefined(this.registrar.user_info.id)
      },
      error ({ graphQLErrors, networkError }) {
        if (graphQLErrors) {
          console.log('graphQLErrors:', graphQLErrors)
        }
        if (networkError) {
          console.log('networkError:', networkError)
        }
      }
    }
  }
}
