import { GET_FILTERED_ROOM } from '@/graphql/admin/queries'
export default {
  apollo: {
    roomsFilterFetch: {
      query: GET_FILTERED_ROOM,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            school_year_id: global._.isUndefined(this.academic_year_id) ? 0 : this.academic_year_id,
            semester_id: global._.isUndefined(this.semester_id) ? 0 : this.semester_id,
            room_id: global._.isEmpty(this.room) ? '' : this.room,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let rooms = []
          this.count = Number(data.roomsFilterFetch.count)
          global._.forEach(data.roomsFilterFetch.rooms, function (val) {
            rooms.push({
              ...val
            })
          })
          this.dataList = rooms
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      }
    }
  }
}
