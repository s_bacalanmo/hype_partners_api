import { GET_SCHOOL_YEAR, GET_PRORAM_HAS_CURRICULUM } from '@/graphql/admin/queries'
export default {
  apollo: {
    syWithFilter: {
      query: GET_SCHOOL_YEAR,
      variables () {
        return {
          input: {
            skip: this.skip,
            take: this.take,
            school_year_id: global._.isUndefined(this.academic_year_id) ? null : this.academic_year_id
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          this.count = data.syWithFilter.count
          if (!global._.isNull(data.syWithFilter)) {
            let syItems = []
            global._.forEach(data.syWithFilter.sy, function (val) {
              syItems.push(val)
            })
            this.asyItems = syItems
          }
        }
      },
      skip () {
        return this.skipSYQuery
      },
      watchLoading (isLoading) {
        this.syLoading = isLoading
      }
    },
    programHasCurriculum: {
      query: GET_PRORAM_HAS_CURRICULUM,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          global._.forEach(data.programHasCurriculum, function (val) {
            let item = global._.omit(val, 'has_enrollee_class')
            items.push({
              ...item,
              has_enrollee_class: null,
              program_id: null,
              school_year_id: null,
              sy_program_id: null
            })
          })
          this.programItems = items
        }
      },
      watchLoading (isLoading) {
        this.programLoading = isLoading
      }
    }
  }
}
