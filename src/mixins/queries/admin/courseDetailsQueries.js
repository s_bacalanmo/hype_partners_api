import { GET_YEAR_LEVEL, GET_SEMESTER, GET_REQUISITE, GET_COURSE_DETAILS, GET_COURSE_NOTES, GET_COURSES  } from '@/graphql/admin/queries'
export default {
  apollo: {
    yearlevel: {
      query: GET_YEAR_LEVEL,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          global._.forEach(data.yearlevel, function (val) {
            items.push({
              year_level_id: Number(val.id),
              level: val.year_level
            })
          })
          this.yearlevels = items
        }
      }
    },
    semester: {
      query: GET_SEMESTER,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let self = this
          global._.forEach(data.semester, function (val) {
            self.semestersItems.push({
              semester_id: Number(val.id),
              semester_name: val.semester
            })
          })
        }
      }
    },
    obtDetails: {
      query: GET_COURSE_DETAILS,
      variables () {
        return {
          curriculum_course_id: this.id
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          global._.forEach(data.obtDetails, function (val) {
            items.push(val)
          })
          this.requirementItems = items
        }
      },
      watchLoading (isLoading) {
        this.detailsLoading = isLoading
      },
      skip () {
        return this.skipDetailsQuery
      }
    },
    obtNotes: {
      query: GET_COURSE_NOTES,
      variables () {
        return {
          curriculum_course_id: this.id
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          global._.forEach(data.obtNotes, function (val) {
            items.push(val)
          })
          this.noteItems = items
        }
      },
      watchLoading (isLoading) {
        this.noteLoading = isLoading
      },
      skip () {
        return this.skipNoteQuery
      }
    }
  },
  methods: {
    querySelections (type, val) {
      let hasCourseId = !global._.isUndefined(type === 'pre' ? this.formData.pre_requisite : type === 'co' ? this.formData.co_requisite : [])
      this.$apollo.query({
        query: GET_REQUISITE,
        variables: {
          keyword: val ? val : null,
          course_id: hasCourseId ? (type === 'pre' ? this.formData.pre_requisite : type === 'co' ? this.formData.co_requisite : []) : null
        }
      }).then(({ data, loading }) => {
        this.isLoading = loading
        if (!global._.isUndefined(data)) {
           let items = []
           if (data.courses) {
            global._.forEach(data.courses.courses, function (val) {
              if (val.course_status === 'Active') {
                items.push({
                  course_description: val.course_description,
                  course_id: Number(val.id),
                  course_code: global._.trim(val.course_code)
                })
              }
            })
            if (type === 'pre') {
              this.preLoading = loading
              this.prerequisitesItems = items
            } else {
              this.coLoading = loading
              this.corequisitesItems = items
            }
          }
        }
      })
      .catch(error => {
        if (error.graphQLErrors) {
          console.log('graphQLErrors:', error.graphQLErrors)
        }
        if (error.networkError) {
          console.log('networkError:', error.networkError)
        }
        if (error.ServerError) {
          console.log('ServerError:', error.ServerError)
        }
      })
    },
    queryCourseSelections (val) {
      this.$apollo.query({
        query: GET_COURSES,
        variables: {
          keyword: val ? val : null,
          course_id: !global._.isUndefined(this.formData.course_item) ? this.formData.course_item.course_id : null
        }
      }).then(({ data, loading }) => {
        this.isLoading = loading
        if (!global._.isUndefined(data)) {
           let items = []
           if (data.coursesMixed) {
            global._.forEach(data.coursesMixed.courses, function (val) {
              if (val.course_status === 'Active') {
                items.push({
                  course_description: val.course_description,
                  course_id: Number(val.id),
                  course_code: global._.trim(val.course_code)
                })
              }
            })
            this.courseItems = items
          }
        }
      })
      .catch(error => {
        if (error.graphQLErrors) {
          console.log('graphQLErrors:', error.graphQLErrors)
        }
        if (error.networkError) {
          console.log('networkError:', error.networkError)
        }
        if (error.ServerError) {
          console.log('ServerError:', error.ServerError)
        }
      })
    }
  }
}