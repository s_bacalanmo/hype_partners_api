import { GET_REVISIONS } from '@/graphql/admin/queries'
export default {
  apollo: {
    revisionLists: {
      query: GET_REVISIONS,
      variables () {
        return {
          skip: this.skip,
          take: this.take,
          curriculum_id: this.per_program.id,
          keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let revisions = []
          this.count = data.revisionLists.count
          if (!global._.isNull(data.revisionLists.revisions)) {
            global._.forEach(data.revisionLists.revisions, function (val) {
              revisions.push(val)
            })
            this.items = revisions
          }
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      }
    }
  }
}
