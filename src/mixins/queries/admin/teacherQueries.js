import { GET_SORTED_TEACHERS } from '@/graphql/admin/queries'
export default {
  apollo: {
    teachersFilterFetch: {
      query: GET_SORTED_TEACHERS,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? null : this.setSearch,
            school_year_id: global._.isUndefined(this.academic_year_id) ? null : this.academic_year_id,
            semester_id: global._.isUndefined(this.semester_id) ? null : this.semester_id,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let teachers = []
          let self = this
          this.count = Number(data.teachersFilterFetch.count)
          global._.forEach(data.teachersFilterFetch.teachers, function (val) {
            let acad = self.acadFunc(val)
            let sem = self.semesterFunc(val)
            teachers.push({
              name: val.user ? `${val.user.first_name} ${self.initialLetter(val.user.middle_name)}${val.user.last_name}` : '',
              academic_year: acad,
              semester: sem,
              count_class: val.count_class,
              count_student: val.count_student
            })
          })
          this.dataList = teachers
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      }
    }
  },
  methods: {
    acadFunc (item) {
      if (item.class_sched) {
        if (item.class_sched.curriculum_course_school_year) {
          if (item.class_sched.curriculum_course_school_year.school_year) {
            if (item.class_sched.curriculum_course_school_year.school_year) {
              return `${item.class_sched.curriculum_course_school_year.school_year.school_year_from} - ${item.class_sched.curriculum_course_school_year.school_year.school_year_to}`
            }
          }
        }
      } else {
        return ''
      }
    },
    semesterFunc (item) {
      if (item.class_sched) {
        if (item.class_sched.curriculum_course_school_year) {
          if (item.class_sched.curriculum_course_school_year.curriculum_course) {
            return item.class_sched.curriculum_course_school_year.curriculum_course.semester.semester
          }
        }
      } else {
        return ''
      }
    }
  }
}
