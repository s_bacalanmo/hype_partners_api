import { GET_CURRICULA, GET_PROGRAM_UNDER_CURRICULUM, GET_PROGRAM } from '@/graphql/admin/queries'
import { mapGetters, mapMutations } from 'vuex'
export default {
  computed: {
    ...mapGetters('form', ['curriculum']),
    ...mapGetters('user', ['user']),
    ...mapGetters('curricula', ['curricula'])
  },
  apollo: {
    curriculaWithFilter: {
      query: GET_CURRICULA,
      variables () {
        return {
          input: {
            program_id: this.curricula ? this.curricula.id : null,
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let curricula = []
          this.count = Number(data.curriculaWithFilter.count)
          let val = []
          for(let key in data.curriculaWithFilter.curricula) {
            val = data.curriculaWithFilter.curricula[key]
            curricula.push(val)
          }
          this.items = curricula
        }
      },
      skip () {
        return this.skipCurriculumQuery
      },
      watchLoading (isLoading) {
        this.currLoading = isLoading
      }
    },
    programsUnderCurriculum: {
      query: GET_PROGRAM_UNDER_CURRICULUM,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            skip: 0,
            take: this.tableData.input.take,
            curriculum_id: this.curriculum.curriculum_id
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          this.dataList = []
          let programs = []
          if (this.user.department) {
            this.programItem = data.programsUnderCurriculum.programs.find(item => {
              let program = item.programs.find(item => item)
              if (!global._.isUndefined(program)) {
                if (program.program_code === this.user.department) {
                  return item
                }
              }
            })
          }
          this.tableData.maxData = Number(data.programsUnderCurriculum.count)
          let val=[]
          let self = this
          for(let key in data.programsUnderCurriculum.programs) {
            val = data.programsUnderCurriculum.programs[key]
            if (val.programs[0]) {
              programs.push({
                curricula_id: val.id,
                curriculum_id: val.curriculum_id,
                program_id: val.program_id,
                course_count: val.programs[0].count_course,
                program_no: val.programs[0].program_no,
                program_name: val.programs[0].program_name,
                program_code: val.programs[0].program_code,
                programs: val.programs[0],
                scheme: val.scheme,
                series_year: val.series_year,
                subtitle: val.subtitle,
                cmo_no: val.cmo_no
              })
            }
          }
          this.dataList = programs
        }
      },
      skip () {
        return this.skipCurriculaProgramQuery
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      }
    },
    program: {
      query: GET_PROGRAM,
      result ({ data }) {
        if(!global._.isUndefined(data)) {
          this.departmentItems = []
          this.programItems = []
          let self = this
          this.programItems = data.program.filter(item => item.program_status === 'Active')
          global._.forEach(this.programItems, function (value) {
            self.departmentItems.push({
              program_name: value.program_name,
              program_code: value.program_code,
              program_id: value.id
            })
          })
        }
      },
      skip () {
        return this.skipProgramQuery
      },
      watchLoading (isLoading) {
        this.programLoading = isLoading
      }
    }
  }
}
