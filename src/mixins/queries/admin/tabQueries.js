
import { GET_CURRENT_PROGRAM, CLASS_SCHED_BY_STUDENT, AVAILABLE_CLASSES, GET_APPLICANT_REQUIREMENTS, GET_ADD_DROP_HISTORY } from '@/graphql/admin/queries'
import { mapGetters } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('user', ['user', 'student_data']),
    skipStudent () {
      return !Boolean(this.registrar.semester_id && this.registrar.user_info.id && this.registrar.program_id && this.registrar.curriculum_id && this.registrar.year_level_id && this.registrar.school_year_id && this.registrar.revision_id)
    },
    skipApplicant () {
      return !Boolean(this.user.id && this.student_data.semester_id && this.student_data.program_id  && this.user.curriculum_id && this.student_data.year_level_id && this.student_data.school_year_id && this.user.revision_id)
    }
  },
  apollo: {
    curriculumProgramsYearNow: {
      query: GET_CURRENT_PROGRAM,
      result ({ data }) {
        if (data) {
          this.programItems = data.curriculumProgramsYearNow.filter(item => item.program_status === 'Active')
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      }
    },
    classSchedByStudent: {
      query: CLASS_SCHED_BY_STUDENT,
      variables () {
        return {
          input: {
            user_id: this.user.user_type !== 'Student' ? this.registrar.user_info.id : this.user.id,
            semester_id: this.user.user_type !== 'Student' ? this.registrar.semester_id : this.student_data.semester_id,
            program_id: this.user.user_type !== 'Student' ? this.registrar.program_id : this.student_data.program_id,
            curriculum_id: this.user.user_type !== 'Student' ? this.registrar.curriculum_id : this.user.curriculum_id,
            year_level_id: this.user.user_type !== 'Student' ? this.registrar.year_level_id : this.student_data.year_level_id,
            school_year_id: this.user.user_type !== 'Student' ? this.registrar.school_year_id : this.student_data.school_year_id,
            revision_id: this.user.user_type !== 'Student' ? this.registrar.revision_id : this.user.revision_id
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          this.selectedItems = []
          let coursetab = []
          if (!global._.isNull(data.classSchedByStudent)) {
            this.tableData.maxData = Number(data.classSchedByStudent.schedules.length)
            this.tableData.total_units = Number(data.classSchedByStudent.total_units)
            this.tableData.units_added = Number(data.classSchedByStudent.credit_units)
            global._.forEach(data.classSchedByStudent.schedules, function (val) {
              let sortLecTime = val.lec_schedules_sorted.map(item => {
                return `${item.time_from_meridian} - ${item.time_to_meridian}`
              })
              let sortLabTime = val.lab_schedules_sorted.map(item => {
                return `${item.time_from_meridian} - ${item.time_to_meridian}`
              })
              const sectionLec = val.lec_schedules_sorted.map(item => {
                if (item.section) return item.section.section
                return ''
              })
              const sectionLab = val.lab_schedules_sorted.map(item => {
                if (item.section) return item.section.section
                return ''
              })
              const daysLab = val.lab_schedules_sorted.map(item => item.day)
              const daysLec = val.lec_schedules_sorted.map(item => item.day)
              const roomLec = val.lec_schedules_sorted.map(item => {
                if (item.room) return item.room.number
                return ''
              })
              const roomLab = val.lab_schedules_sorted.map(item => {
                if (item.room) return item.room.number
                return ''
              })
              const instructorLab = val.lab_schedules_sorted.map(item => item.teacher_fullname)
              const instructorLec = val.lec_schedules_sorted.map(item => item.teacher_fullname)
              coursetab.push({
                ...val,
                class_code: val.class_code,
                course_code: global._.isNull(val.course) ? '' : val.course.course_code,
                course_id: val.course.id,
                year_level_id: val.year_level_id,
                semester_id: val.semester_id,
                curriculum_id: val.curriculum_id,
                program_id: val.program_id,
                section: Number(val.lec_hours) !== 0 ? `${global._.uniq(sectionLec)}` : `${global._.uniq(sectionLab)}`,
                program_code: global._.isNull(val.program) ? '' : val.program.program_code,
                lec_time: global._.uniq(sortLecTime),
                lab_time: global._.uniq(sortLabTime),
                lec_days: global._.uniq(daysLec),
                lab_days: global._.uniq(daysLab),
                lec_room: global._.uniq(roomLec),
                lab_room: global._.uniq(roomLab),
                lec_instructor: global._.uniq(instructorLec),
                lab_instructor: global._.uniq(instructorLab)
              })
            })
            this.selectedItems = coursetab
            this.itemsLength = this.selectedItems.length
          }
        }
      },
      watchLoading (isLoading) {
        this.selectedloading = isLoading
      },
      skip () {
        return this.skipSelectedSchedQuery
      }
    },
    classSchedAvailable: {
      query: AVAILABLE_CLASSES,
      variables () {
        return {
          input: {
            user_id:  this.user.user_type !== 'Student' ? this.registrar.user_info.id : this.user.id,
            semester_id: this.user.user_type !== 'Student' ? this.registrar.semester_id : this.student_data.semester_id,
            curriculum_id: this.user.user_type !== 'Student' ? this.registrar.curriculum_id : this.user.curriculum_id,
            school_year_id: this.user.user_type !== 'Student' ? this.registrar.school_year_id : this.student_data.school_year_id,
            program_id: this.select ? this.select : '',
            course_code: this.search ? this.search : '',
            revision_id: this.user.user_type !== 'Student' ? this.registrar.revision_id : this.student_data.revision_id,
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          global._.forEach(data.classSchedAvailable.class, function (val) {
            let sortLecTime = val.lec_schedules_sorted.map(item => {
              return `${item.time_from_meridian} - ${item.time_to_meridian}`
            })
            let sortLabTime = val.lab_schedules_sorted.map(item => {
              return `${item.time_from_meridian} - ${item.time_to_meridian}`
            })
            const sectionLec = val.lec_schedules_sorted.map(item => {
              if (item.section) return item.section.section
              return ''
            })
            const sectionLab = val.lab_schedules_sorted.map(item => {
              if (item.section) return item.section.section
              return ''
            })
            const daysLab = val.lab_schedules_sorted.map(item => item.day)
            const daysLec = val.lec_schedules_sorted.map(item => item.day)
            const roomLec = val.lec_schedules_sorted.map(item => {
              if (item.room) return item.room.number
              return ''
            })
            const roomLab = val.lab_schedules_sorted.map(item => {
              if (item.room) return item.room.number
              return ''
            })
            const instructorLab = val.lab_schedules_sorted.map(item => item.teacher_fullname)
            const instructorLec = val.lec_schedules_sorted.map(item => item.teacher_fullname)
            items.push({
              ...val,
              class_code: val.class_code,
              course_code: global._.isNull(val.course) ? '' : val.course.course_code,
              course_id: val.course.id,
              year_level_id: val.year_level_id,
              semester_id: val.semester_id,
              curriculum_id: val.curriculum_id,
              program_id: val.program_id,
              section: Number(val.lec_hours) !== 0 ? `${global._.uniq(sectionLec)}` : `${global._.uniq(sectionLab)}`,
              program_code: global._.isNull(val.program) ? '' : val.program.program_code,
              lec_time: global._.uniq(sortLecTime),
              lab_time: global._.uniq(sortLabTime),
              lec_days: global._.uniq(daysLec),
              lab_days: global._.uniq(daysLab),
              lec_room: global._.uniq(roomLec),
              lab_room: global._.uniq(roomLab),
              lec_instructor: global._.uniq(instructorLec),
              lab_instructor: global._.uniq(instructorLab),
              period_status: global._.isNull(val.add_drop) ? '' : val.add_drop.type === 'Drop' ? 'Dropped' : 'Available'
            })
          })
          this.availableItems = items
        }
      },
      watchLoading (isLoading) {
        this.availableLoading = isLoading
      },
      skip () {
        return this.skipAvailableSchedQuery
      }
    },
    addDropHistory: {
      query: GET_ADD_DROP_HISTORY,
      variables () {
        return {
          user_id: this.registrar.user_info ? this.registrar.user_info.id : null,
          school_year_id: this.registrar.school_year_id ? this.registrar.school_year_id : null,
          semester_id: this.registrar.semester_id ? this.registrar.semester_id : null
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let coursetab = []
          if (!global._.isNull(data.addDropHistory)) {
            if (!global._.isEmpty(data.addDropHistory.added_history)) {
              const sortUnits = data.addDropHistory.added_history.map(item => Number(item.units))
              let currentUnits = global._.reduce(global._.mapValues(sortUnits, function (o) { return o }), function (sum, num) {
                 return sum + num
                })
              this.tableData.add_drop_total_units = currentUnits
              global._.forEach(data.addDropHistory.added_history, function (val) {
                let sortLecTime = val.lec_schedules_sorted.map(item => {
                  return `${item.time_from_meridian} - ${item.time_to_meridian}`
                })
                let sortLabTime = val.lab_schedules_sorted.map(item => {
                  return `${item.time_from_meridian} - ${item.time_to_meridian}`
                })
                const sectionLec = val.lec_schedules_sorted.map(item => {
                  if (item.section) return item.section.section
                  return ''
                })
                const sectionLab = val.lab_schedules_sorted.map(item => {
                  if (item.section) return item.section.section
                  return ''
                })
                const daysLab = val.lab_schedules_sorted.map(item => item.day)
                const daysLec = val.lec_schedules_sorted.map(item => item.day)
                const roomLec = val.lec_schedules_sorted.map(item => {
                  if (item.room) return item.room.room_name
                  return ''
                })
                const roomLab = val.lab_schedules_sorted.map(item => {
                  if (item.room) return item.room.room_name
                  return ''
                })
                const instructorLab = val.lab_schedules_sorted.map(item => item.teacher_fullname)
                const instructorLec = val.lec_schedules_sorted.map(item => item.teacher_fullname)
                coursetab.push({
                  ...val,
                  class_code: val.class_code,
                  course_code: global._.isNull(val.course) ? '' : val.course.course_code,
                  course_id: val.course.id,
                  year_level_id: val.year_level_id,
                  semester_id: val.semester_id,
                  curriculum_id: val.curriculum_id,
                  program_id: val.program_id,
                  section: !global._.isNull(val.lec_hours) ? `${global._.uniq(sectionLec)}` : `${global._.uniq(sectionLab)}`,
                  program_code: global._.isNull(val.program) ? '' : val.program.program_code,
                  lec_time: global._.uniq(sortLecTime),
                  lab_time: global._.uniq(sortLabTime),
                  lec_days: global._.uniq(daysLec),
                  lab_days: global._.uniq(daysLab),
                  lec_room: global._.uniq(roomLec),
                  lab_room: global._.uniq(roomLab),
                  lec_instructor: global._.uniq(instructorLec),
                  lab_instructor: global._.uniq(instructorLab)
                })
              })
              this.addedItems = coursetab
            } else {
              this.addedItems = []
              this.tableData.add_drop_total_units = 0
            }
          }
        }
      },
      watchLoading (isLoading) {
        this.addDroploading = isLoading
      },
      skip () {
        return this.skipAddDropQuery
      }
    },
  },
  methods: {
    getFiles (reqs, id, file_name) {
      if (!global._.isEmpty(file_name) && !global._.isNull(id)) {
        this.$apollo.query({
          query: GET_APPLICANT_REQUIREMENTS,
          variables: {
            input: {
              id: id,
              file_name: file_name
            }
          }
        }).then((data) => {
          if (!global._.isNull(data.data.applicantRequirements)) {
            if (global._.includes(reqs, this.fileTitle(data.data.applicantRequirements.file_name))) {
              this.addItem(this.fileTitle(data.data.applicantRequirements.file_name))
            }
          }
        })
        .catch(error => {
          console.log(error)
        })
      }
    },
    fileTitle (file) {
      if (global._.isEqual(file, 'Hepatitis B Antibodies')) {
        return `${file} (Optional)`
      } else if (global._.isEqual(file, 'Authenticated Birth Certificate')) {
        return `PSA ${file} (original)`
      } else if (global._.isEqual(file, 'Certificate Of Honorable Dismissal') || global._.isEqual(file, 'High School Report Card') || global._.isEqual(file, 'Good Moral Character Certificate')) {
        return `${file} (original)`
      } else if (global._.isEqual(file, 'Exam Result')) {
        return `APRO ${file}`
      } else {
        return file
      }
    }
  }
}