import { GET_DEPARTMENT_CURRICULUM } from '@/graphql/admin/queries'
import { mapGetters } from 'vuex'
export default {
  computed: {
    ...mapGetters('user', ['user'])
  },
  apollo: {
    curriculaWithFilterDean: {
      query: GET_DEPARTMENT_CURRICULUM,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            dean_program_id: this.user.department_id,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let curricula = []
          this.count = Number(data.curriculaWithFilterDean.count)
          let val = []
          for(let key in data.curriculaWithFilterDean.curricula) {
            val = data.curriculaWithFilterDean.curricula[key]
            curricula.push(val)
          }
          this.items = curricula
        }
      },
      skip () {
        return this.skipdepartmentCurriculumQuery
      },
      watchLoading (isLoading) {
        this.currLoading = isLoading
      }
    }
  }
}
