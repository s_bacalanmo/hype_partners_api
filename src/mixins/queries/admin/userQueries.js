import { GET_PROGRAM, GET_FILTERED_USER, GET_POSITION } from '@/graphql/admin/queries'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [defaultMixins],
  apollo: {
    program: {
      query: GET_PROGRAM,
      result ({ data }) {
        if(data) {
          let programs = []
          this.programItems = data.program.filter(item => item.program_status === 'Active')
          global._.forEach(data.program, function (value) {
            programs.push({
              program_name: value.program_name,
              program_id: Number(value.id)
            })
          })
          this.departmentItems = programs
        }
      },
      skip () {
        return this.skipProgramQuery
      },
      watchLoading (isLoading) {
        this.programLoading = isLoading
      }
    },
    userFilterFetch: {
      query: GET_FILTERED_USER,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            position: this.position,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          const { formatDate, initialLetter, textCapitalize } = this
          let users = []
          this.count = Number(data.userFilterFetch.count)
          global._.forEach(data.userFilterFetch.user, function (val) {
            let item = global._.omit(val, 'birth_date', 'gender')
            const programName = global._.isUndefined(val.program) ? '' : global._.isNull(val.program) ? '' : val.program.program_name
            const programId = global._.isUndefined(val.program) ? '' : global._.isNull(val.program) ? '' : Number(val.program.id)
            const courseCode = global._.isUndefined(val.course) ? '' : global._.isNull(val.course) ? '' : val.course.course_code
            const courseId = global._.isUndefined(val.course) ? '' : global._.isNull(val.course) ? '' : Number(val.course.id)
            const firstname = val.first_name ? textCapitalize(val.first_name) : ''
            const lastname = val.last_name ? textCapitalize(val.last_name) : ''
            const middlename = val.middle_name ? initialLetter(val.middle_name) : ''
            const extension = val.name_extension && global._.lowerCase(val.name_extension) !== 'n a' ? initialLetter(val.name_extension) : ''
            users.push({
              ...item,
              birth_date: formatDate(val.birth_date),
              gender: global._.capitalize(val.gender),
              email: item.email,
              name: `${lastname}, ${firstname} ${middlename} ${extension}`,
              position_id: item.user_type_id,
              position_name: global._.isEmpty(item.position) ? '' : item.position.user_type,
              program_name: programName,
              program_id: programId,
              course_code: courseCode,
              course_id: courseId
            })
          })
          this.dataList = users
        }
      },
      skip () {
        return this.skipUserQuery
      },
      watchLoading (isLoading) {
        this.userLoading = isLoading
      }
    },
    userpositions: {
      query: GET_POSITION,
      result ({ data, loading }) {
        if (!global._.isUndefined(data)) {
          let userPosition = []
          global._.forEach(data.userpositions, function (value) {
            userPosition.push({
              position_name: value.user_type,
              user_type_id: Number(value.id)
            })
          })
          this.positionItems = userPosition
        }
      },
      skip () {
        return this.skipPositionQuery
      }
    }
  }
}