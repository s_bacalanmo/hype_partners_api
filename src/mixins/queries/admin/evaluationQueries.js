import { GET_STUDENT_EVALUATION } from '@/graphql/admin/queries'

export default {
  apollo: {
    studentEvaluation: {
      query: GET_STUDENT_EVALUATION,
      variables () {
        return {
          input: {
            user_id: this.userId,
            semester_id: this.ids.semester_id,
            year_level_id: this.ids.year_level_id,
            program_id: this.ids.program_id
          }
        }
      },
      result ({ data }) {
        if (data) {
          let years = []
          if (!global._.isUndefined(data)) {
            global._.forEach(data.studentEvaluation, function (valItem, keyItem) {
                global._.forEach(valItem, function (val, key) {
                years.push({
                  key: key === 'First Year' ? 1 : key === 'Second Year' ? 2 : key === 'Third Year' ? 3 : 4,
                  year_level: '',
                  year: `${key} - ${keyItem}`,
                  semesters: val
                })
              })
            })
            let sortKeys = years.sort((a, b) => a.key - b.key)
            this.items = sortKeys
          }
        }
      },
      deep: true,
      skip () {
        return !this.isEvaluation
      }
    }
  }
}