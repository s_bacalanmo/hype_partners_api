
  import { GET_STUDENTS_GRADES, GET_GRADING_POINTS } from '@/graphql/admin/queries'
export default {
  apollo: {
    studentGrades: {
      query: GET_STUDENTS_GRADES,
      variables () {
        return {
          input: {
            class_code: this.classData.class_code,
            teacher_id: this.user.id,
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            skip: 0,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let self = this
          let items = []
          this.count = Number(data.studentGrades.count)
          global._.forEach(data.studentGrades.grades, function (val, key) {
            let gradeSystem = global._.omit(val.grade_system, 'id')
            items.push({
              ...val,
              ...gradeSystem,
              name: val.user ? `${val.user.first_name} ${self.initialLetter(val.user.middle_name)} ${val.user.last_name} `: '',
              student_id: val.user ? val.user.id : ''
            })
          })
          this.items = items
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      },
      skip () {
        return !this.classData.class_code && !this.user.id
      },
      error ({ graphQLErrors, networkError, ServerError }) {
        if (graphQLErrors) {
          console.log('graphQLErrors:', graphQLErrors)
        }
        if (networkError) {
          console.log('networkError:', networkError)
        }
        if (ServerError) {
          console.log('ServerError:', ServerError)
        }
      }
    },
    studentGWA: {
      query: GET_GRADING_POINTS,
      variables () {
        return {
          gwa: String(this.editedItem.gwa)
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.studentGWA)) {
            this.editedItem.grade_desc = data.studentGWA.rating_description
            this.editedItem.grade_eqv = data.studentGWA.grade_point
            this.editedItem.letter_grade = data.studentGWA.letter_grade
            this.editedItem.remarks = data.studentGWA.remarks
          }
        }
      },
      watchLoading (isLoading) {
        this.gradingLoading = isLoading
      },
      skip () {
        return String(this.editedItem.gwa) === ''
      },
      error ({ graphQLErrors, networkError, ServerError }) {
        if (graphQLErrors) {
          console.log('graphQLErrors:', graphQLErrors)
        }
        if (networkError) {
          console.log('networkError:', networkError)
        }
        if (ServerError) {
          console.log('ServerError:', ServerError)
        }
      }
    }
  }
}
