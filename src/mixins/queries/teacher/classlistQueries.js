import { GET_TEACHER_STUDENTS } from '@/graphql/admin/queries'
export default {
  apollo: {
    teacherStudents: {
      query: GET_TEACHER_STUDENTS,
      variables () {
        return {
          user_id: this.user ? this.user.id : null,
          school_year_id: !this.academic_year_id ? null : this.academic_year_id,
          program_id: !this.program_id ? null : this.program_id,
          year_level_id: !this.year_level_id ? null : this.year_level_id,
          semester_id: !this.semester_id ? null : this.semester_id,
          class_code_from: !this.class_code ? 0 : this.class_code[0],
          class_code_to: !this.class_code ? 0 : this.class_code[1],
          keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
          skip: 0,
          take: this.take
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          this.count = Number(data.teacherStudents.count)
          global._.forEach(data.teacherStudents.class, function (val, key) {
            let sortLecTime = val.lec_schedules_sorted.map(item => {
              return `${item.time_from_meridian} - ${item.time_to_meridian}`
            })
            let sortLabTime = val.lab_schedules_sorted.map(item => {
              return `${item.time_from_meridian} - ${item.time_to_meridian}`
            })
            const sectionLec = val.lec_schedules_sorted.map(item => {
              if (item.section) return item.section.section
              return ''
            })
            const sectionLab = val.lab_schedules_sorted.map(item => {
              if (item.section) return item.section.section
              return ''
            })
            const daysLab = val.lab_schedules_sorted.map(item => item.day)
            const daysLec = val.lec_schedules_sorted.map(item => item.day)
            const roomLec = val.lec_schedules_sorted.map(item => {
              if (item.room) return item.room.number
              return ''
            })
            const roomLab = val.lab_schedules_sorted.map(item => {
              if (item.room) return item.room.number
              return ''
            })
            const instructorLab = val.lab_schedules_sorted.map(item => item.teacher_fullname)
            const instructorLec = val.lec_schedules_sorted.map(item => item.teacher_fullname)
            items.push({
              ...val,
              class_code: val.class_code,
              course_code: global._.isNull(val.course) ? '' : val.course.course_code,
              description: global._.isNull(val.course) ? '' : val.course.course_description,
              course_id: val.course.id,
              year_level_id: val.year_level_id,
              semester_id: val.semester_id,
              no_data: '',
              curriculum_id: val.curriculum_id,
              program_id: val.program_id,
              section: !global._.isNull(val.lec_hours) ? `${global._.uniq(sectionLec)}` : `${global._.uniq(sectionLab)}`,
              program_code: global._.isNull(val.program) ? '' : val.program.program_code,
              time: global._.isEmpty(String(global._.uniq(sortLabTime))) ? String(global._.uniq(sortLecTime)) : `LEC: ${String(global._.uniq(sortLecTime))}, LAB: ${String(global._.uniq(sortLabTime))}`,
              days: global._.isEmpty(String(global._.uniq(daysLab))) ? String(global._.uniq(daysLec)) : `LEC: ${String(global._.uniq(daysLec))}, LAB: ${String(global._.uniq(daysLab))}`,
              room: global._.isEmpty(String(global._.uniq(roomLab))) ? String(global._.uniq(roomLec)) : `LEC: ${String(global._.uniq(roomLec))}, LAB: ${String(global._.uniq(roomLab))}`,
              lec_time: global._.uniq(sortLecTime),
              lab_time: global._.uniq(sortLabTime),
              lec_days: global._.uniq(daysLec),
              lab_days: global._.uniq(daysLab),
              lec_room: global._.uniq(roomLec),
              lab_room: global._.uniq(roomLab),
              lec_instructor: global._.uniq(instructorLec),
              lab_instructor: global._.uniq(instructorLab),
              type: global._.isEmpty(val.lab_schedules) && global._.isEmpty(val.lec_schedules) ? 'OJT' : 'Lec/Lab'
            })
          })
          this.items = items
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      },
      skip () {
        return !this.user.id
      },
      error ({ graphQLErrors, networkError, ServerError }) {
        if (graphQLErrors) {
          console.log('graphQLErrors:', graphQLErrors)
        }
        if (networkError) {
          console.log('networkError:', networkError)
        }
        if (ServerError) {
          console.log('ServerError:', ServerError)
        }
      }
    }
  }
}
