import { GET_SUBMISSION_INFO, GET_ENROLLMENT_SUBMISSION_INFO } from '@/graphql/admin/queries'
import { mapGetters, mapMutations } from 'vuex'
export default {
  computed: {
    ...mapGetters('user', ['user']),
  },
  methods: {
    ...mapMutations('form', ['setIsEntranceSubmit', 'setIsEnrollSubmit'])
  },
  apollo: {
    submitApplicationUser: {
      query: GET_SUBMISSION_INFO,
      variables () {
        return {
          user_id: this.user.id
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.submitApplicationUser)) {
            if (data.submitApplicationUser.submission_type === 'Entrance Exam') {
              if (data.submitApplicationUser.submission_status === 'Pending for Submission') {
                this.setIsEntranceSubmit(true)
              } else if (data.submitApplicationUser.submission_status === 'Under Assessment') {
                this.setIsEntranceSubmit(false)
              }
            }
          } else {
            this.setIsEntranceSubmit(true)
          }
        }
      },
      skip () {
        return this.skipApplicationQuery
      }
    },
    submitApplicationEnrollmentUser: {
      query: GET_ENROLLMENT_SUBMISSION_INFO,
      variables () {
        return {
          user_id: this.user.id
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          if (!global._.isNull(data.submitApplicationEnrollmentUser)) {
            if (data.submitApplicationEnrollmentUser.submission_type === 'Enrollment') {
              if (data.submitApplicationEnrollmentUser.submission_status === 'Pending for Submission') {
                this.setIsEnrollSubmit(true)
              } else if (data.submitApplicationEnrollmentUser.submission_status === 'Under Assessment') {
                this.setIsEnrollSubmit(false)
              }
            }
          } else {
            this.setIsEnrollSubmit(true)
          }
        }
      },
      skip () {
        return this.skipEnrollApplicationQuery
      }
    }
  }
}