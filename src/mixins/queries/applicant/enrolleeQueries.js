
import { GET_APPLICANTS_ENROLLMENT, GET_SECTION_BY_IDS  } from '@/graphql/admin/queries'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  apollo: {
    sections: {
      query: GET_SECTION_BY_IDS,
      variables () {
        return {
          program_id: this.assign_program_id,
          year_level_id: this.assign_year_id,
          keyword: this.setKeyword
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let items = []
          global._.forEach(data.sections, function (value) {
            items.push({
              section_id: Number(value.id),
              section: value.section
            })
          })
          this.sections = items
        }
      },
      watchLoading (isLoading) {
        this.sectionLoading = isLoading
      },
      skip () {
        return this.skipAssignQuery
      }
    },
    filterEnrollmentMIS: {
      query: GET_APPLICANTS_ENROLLMENT,
      variables () {
        return {
          input: {
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            school_year_id: global._.isUndefined(this.academic_year_id) ? 0 : this.academic_year_id,
            program_id: global._.isUndefined(this.program_id) ? 0 : this.program_id,
            year_level_id: global._.isUndefined(this.year_level_id) ? 0 : this.year_level_id,
            semester_id: global._.isUndefined(this.semester_id) ? 0 : this.semester_id,
            status: this.status ? this.status : '',
            section_id: global._.isUndefined(this.section) ? 0 : this.section,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let self = this
          this.count = Number(data.filterEnrollmentMIS.count)
          let applicants = []
          global._.forEach(data.filterEnrollmentMIS.enrollment, function (value) {
            const sortItems = global._.omit(value, 'user_id', 'user_info')
            const sortUserItem = global._.omit(value.user_info, 'birth_date')
            sortUserItem.birth_date = self.formatDate(value.user_info.birth_date)
            const sorticons = global._.omit(sortItems.applicant_approve, 'semester_id')
            const firstname = value.user_info.first_name ? self.textCapitalize(value.user_info.first_name) : ''
            const lastname = value.user_info.last_name ? self.textCapitalize(value.user_info.last_name) : ''
            const middlename = value.user_info.middle_name ? self.initialLetter(value.user_info.middle_name) : ''
            const extension = value.user_info.name_extension && global._.lowerCase(value.user_info.name_extension) !== 'n a' ? self.initialLetter(value.user_info.name_extension) : ''
            applicants.push({
              ...sortItems,
              ...sorticons,
              ...sortItems.program,
              ...sortUserItem,
              scheme: value.curriculum ? value.curriculum.scheme : null,
              user_info: sortUserItem,
              id: Number(sortItems.id),
              section: global._.isNull(sortItems.section) ? '' : sortItems.section.section,
              year_level: global._.isNull(sortItems.yearlevel) ? '' : sortItems.yearlevel.year_level,
              semester: global._.isNull(sortItems.semester) ? '' : sortItems.semester.semester,
              school_year: global._.isNull(sortItems.school_year) ? '' : `${sortItems.school_year.school_year_from} - ${sortItems.school_year.school_year_to}`,
              applicant_status: sortItems.applicant_status,
              submission_type: sortItems.submission_type,
              user_status: sortItems.status,
              aplication_no: Number(sortItems.id),
              name: global._.isEmpty(value.user_info) ? '' : `${lastname}, ${firstname} ${middlename} ${extension}`
            })
          })
          this.items = applicants
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      },
      skip () {
        return this.skipEnrolleesQuery
      }
    }
  }
}
