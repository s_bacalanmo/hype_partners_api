
import { GET_APPLICANTS_ENTRANCE_EXAM, GET_DEPARTMENT_ENTRANCE_APPLICANTS  } from '@/graphql/admin/queries'
export default {
  apollo: {
    filterEntranceExamMIS: {
      query: GET_APPLICANTS_ENTRANCE_EXAM,
      variables () {
        return {
          input: {
            academic_year_id: global._.isUndefined(this.academic_year_id) ? null : Number(this.academic_year_id),
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            program_id: global._.isUndefined(this.program_id) ? null : this.program_id,
            status: this.status ? this.status : '',
            email_status: global._.isUndefined(this.entrance_status) ? null : this.entrance_status,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let self = this
          this.items = []
          let applicants = []
          this.count = Number(data.filterEntranceExamMIS.count)
          global._.forEach(data.filterEntranceExamMIS.entrance_exam, function (value) {
            const sortItems = global._.omit(value, 'user_id', 'user_info')
            const sortUserItem = global._.omit(value.user_info, 'birth_date')
            sortUserItem.birth_date = self.formatDate(value.user_info.birth_date)
            const firstname = value.user_info.first_name ? self.textCapitalize(value.user_info.first_name) : ''
            const lastname = value.user_info.last_name ? self.textCapitalize(value.user_info.last_name) : ''
            const middlename = value.user_info.middle_name ? self.initialLetter(value.user_info.middle_name) : ''
            const extension = value.user_info.name_extension && global._.lowerCase(value.user_info.name_extension) !== 'n a' ? self.initialLetter(value.user_info.name_extension) : ''
            applicants.push({
              ...sortItems,
              ...sortItems.applicant_approve,
              ...sortItems.program,
              ...sortUserItem,
              user_info: sortUserItem,
              id: Number(sortItems.id),
              applicant_status: sortItems.applicant_status,
              isTransferee: sortItems.applicant_status === 'Freshman' ? 'No' : sortItems.applicant_status === 'Transferee' ? 'Yes' : null,
              submission_type: sortItems.submission_type,
              user_status: sortItems.status,
              aplication_no: Number(sortItems.id),
              name: global._.isSet(value.user_info) ? '' : `${lastname}, ${firstname} ${middlename} ${extension}`
            })
          })
          this.items = applicants
        }
      },
      watchLoading (isLoading) {
        this.entranceLoading = isLoading
      },
      skip () {
        return this.skipEntranceQuery
      }
    },
    deanApplicantsEntranceExam: {
      query: GET_DEPARTMENT_ENTRANCE_APPLICANTS,
      variables () {
        return {
          input: {
            academic_year_id: global._.isUndefined(this.academic_year_id) ? null : Number(this.academic_year_id),
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            program_id: global._.isUndefined(this.program_id) ? null : this.program_id,
            dean_program_id: this.user ? this.user.department_id : null,
            status: this.status ? this.status : '',
            email_status: global._.isUndefined(this.entrance_status) ? null : this.entrance_status,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let self = this
          this.departmentItems = []
          let applicants = []
          this.count = Number(data.deanApplicantsEntranceExam.count)
          global._.forEach(data.deanApplicantsEntranceExam.entrance_exam, function (value) {
            const sortItems = global._.omit(value, 'user_id')
            const sortUserItem = global._.omit(value.user_info, 'birth_date')
            sortUserItem.birth_date = self.formatDate(value.user_info.birth_date)
            const firstname = value.user_info.first_name ? self.textCapitalize(value.user_info.first_name) : ''
            const lastname = value.user_info.last_name ? self.textCapitalize(value.user_info.last_name) : ''
            const middlename = value.user_info.middle_name ? self.initialLetter(value.user_info.middle_name) : ''
            const extension = value.user_info.name_extension && global._.lowerCase(value.user_info.name_extension) !== 'n a' ? self.initialLetter(value.user_info.name_extension) : ''
            let applicantStatus = sortItems.applicant_status === 'Freshman' ? 'No' : sortItems.applicant_status === 'Transferee' ? 'Yes' : null
            if (applicantStatus === 'Yes') {
              applicants.push({
                ...sortItems,
                ...sortItems.applicant_approve,
                ...sortItems.program,
                ...sortUserItem,
                user_info: sortUserItem,
                id: Number(sortItems.id),
                applicant_status: sortItems.applicant_status,
                isTransferee: applicantStatus,
                submission_type: sortItems.submission_type,
                user_status: sortItems.status,
                aplication_no: Number(sortItems.id),
                name: global._.isSet(sortItems.user_info) ? '' : `${lastname}, ${firstname} ${middlename} ${extension} `
              })
            }
          })
          this.departmentItems = applicants
        }
      },
      watchLoading (isLoading) {
        this.departmentLoading = isLoading
      },
      skip () {
        return this.skipDeansQuery
      }
    }
  },
  methods: {
    getEntranceApplicants (skip,take) {
      this.$apollo.query({
        query: GET_APPLICANTS_ENTRANCE_EXAM,
        variables: {
          input: {
            academic_year_id: global._.isUndefined(this.academic_year_id) ? null : Number(this.academic_year_id),
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            program_id: global._.isUndefined(this.program_id) ? null : this.program_id,
            status: this.status ? this.status : '',
            email_status: global._.isUndefined(this.entrance_status) ? null : this.entrance_status,
            skip: skip,
            take: take
          }
        }
      }).then(({ data, loading }) => {
        this.loading = loading
        if (!global._.isUndefined(data)) {
          let self = this
          this.items = []
          let applicants = []
          this.count = Number(data.filterEntranceExamMIS.count)
          global._.forEach(data.filterEntranceExamMIS.entrance_exam, function (value) {
            const sortItems = global._.omit(value, 'user_id', 'user_info')
            const sortUserItem = global._.omit(value.user_info, 'birth_date')
            sortUserItem.birth_date = self.formatDate(value.user_info.birth_date)
            applicants.push({
              ...sortItems,
              ...sortItems.applicant_approve,
              ...sortItems.program,
              ...sortUserItem,
              user_info: sortUserItem,
              id: Number(sortItems.id),
              applicant_status: sortItems.applicant_status,
              isTransferee: sortItems.applicant_status === 'Freshman' ? 'No' : sortItems.applicant_status === 'Transferee' ? 'Yes' : null,
              submission_type: sortItems.submission_type,
              user_status: sortItems.status,
              aplication_no: Number(sortItems.id),
              name: global._.isSet(value.user_info) ? '' : `${global._.capitalize(value.user_info.first_name)} ${self.initialLetter(value.user_info.middle_name)}${global._.capitalize(value.user_info.last_name)}`
            })
          })
          this.items = applicants
        }
      })
      .catch(error => {
        if (error.graphQLErrors) {
          console.log('graphQLErrors:', error.graphQLErrors)
        }
        if (error.networkError) {
          console.log('networkError:', error.networkError)
        }
        if (error.ServerError) {
          console.log('ServerError:', error.ServerError)
        }
      })
    },
    getDeanEntranceApplicants (skip,take) {
      this.$apollo.query({
        query: GET_DEPARTMENT_ENTRANCE_APPLICANTS,
        variables: {
          input: {
            academic_year_id: global._.isUndefined(this.academic_year_id) ? null : Number(this.academic_year_id),
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            program_id: global._.isUndefined(this.program_id) ? null : this.program_id,
            dean_program_id: this.user ? this.user.department_id : null,
            status: this.status ? this.status : '',
            email_status: global._.isUndefined(this.entrance_status) ? null : this.entrance_status,
            skip: skip,
            take: take
          }
        }
      }).then(({ data, loading }) => {
        this.loading = loading
        if (!global._.isUndefined(data)) {
          let self = this
          this.departmentItems = []
          let items = []
          this.count = Number(data.deanApplicantsEntranceExam.count)
          global._.forEach(data.deanApplicantsEntranceExam.entrance_exam, function (value) {
            const sortItems = global._.omit(value, 'user_id')
            let applicantStatus = sortItems.applicant_status === 'Freshman' ? 'No' : sortItems.applicant_status === 'Transferee' ? 'Yes' : null
            if (applicantStatus === 'Yes') {
              items.push({
                ...sortItems,
                ...sortItems.applicant_approve,
                ...sortItems.program,
                ...sortItems.user_info,
                id: Number(sortItems.id),
                applicant_status: sortItems.applicant_status,
                isTransferee: applicantStatus,
                submission_type: sortItems.submission_type,
                user_status: sortItems.status,
                aplication_no: Number(sortItems.id),
                name: global._.isSet(sortItems.user_info) ? '' : `${global._.capitalize(sortItems.user_info.first_name)} ${self.initialLetter(sortItems.user_info.middle_name)}${global._.capitalize(sortItems.user_info.last_name)}`
              })
            }
          })
          this.departmentItems = items
        }
      })
      .catch(error => {
        if (error.graphQLErrors) {
          console.log('graphQLErrors:', error.graphQLErrors)
        }
        if (error.networkError) {
          console.log('networkError:', error.networkError)
        }
        if (error.ServerError) {
          console.log('ServerError:', error.ServerError)
        }
      })
    }
  }
}
