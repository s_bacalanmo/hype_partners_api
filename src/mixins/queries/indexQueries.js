import { GET_YEAR_LEVEL, GET_SEMESTER } from '@/graphql/admin/queries'
export default {
  apollo: {
    yearlevel: {
      query: GET_YEAR_LEVEL,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let self = this
          global._.forEach(data.yearlevel, function (val) {
            self.yearlevels.push({
              year_level_id: String(val.id),
              last_year_level_attended: val.year_level
            })
          })
        }
      },
      skip () {
        return this.skipYearLevelQuery
      }
    },
    semester: {
      query: GET_SEMESTER,
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let self = this
          global._.forEach(data.semester, function (val) {
            self.semestersItems.push({
              semester_id: Number(val.id),
              semester_name: val.semester
            })
          })
        }
      },
      skip () {
        return this.skipSemesterQuery
      }
    }
  }
}