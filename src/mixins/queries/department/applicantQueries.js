import { GET_TRANSFEREE_BY_ID } from '@/graphql/admin/queries'
import { mapGetters } from 'vuex'
import defaultMixins from '@/mixins/index'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('user', ['user'])
  },
  apollo: {
    deanApplicants: {
      query: GET_TRANSFEREE_BY_ID,
      variables () {
        return {
          input: {
            dean_program_id: this.user.department_id,
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            school_year_id: global._.isUndefined(this.academic_year_id) ? 0 : this.academic_year_id,
            program_id: global._.isUndefined(this.program_id) ? 0 : this.program_id,
            year_level_id: global._.isUndefined(this.year_level_id) ? 0 : this.year_level_id,
            semester_id: global._.isUndefined(this.semester_id) ? 0 : this.semester_id,
            status: this.status,
            skip: this.skip,
            take: this.take
          }
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let self = this
          let applicants = []
          this.count = Number(data.deanApplicants.count)
          global._.forEach(data.deanApplicants.enrollment, function (value) {
            const sortItems = global._.omit(value, 'user_id', 'user_info', 'section')
            const sortUserItem = global._.omit(value.user_info, 'birth_date')
            sortUserItem.birth_date = self.formatDate(value.user_info.birth_date)
            const firstname = value.user_info.first_name ? self.textCapitalize(value.user_info.first_name) : ''
            const lastname = value.user_info.last_name ? self.textCapitalize(value.user_info.last_name) : ''
            const middlename = value.user_info.middle_name ? self.initialLetter(value.user_info.middle_name) : ''
            const extension = value.user_info.name_extension && global._.lowerCase(value.user_info.name_extension) !== 'n a' ? self.initialLetter(value.user_info.name_extension) : ''
            applicants.push({
              ...sortItems,
              ...sortItems.applicant_approve,
              ...sortItems.program,
              ...sortItems.yearlevel,
              ...sortItems.semester,
              ...sortUserItem,
              user_info: sortUserItem,
              id: Number(sortItems.id),
              section: value.section ? value.section.section : '',
              applicant_status: sortItems.applicant_status,
              submission_type: sortItems.submission_type,
              user_status: sortItems.status,
              aplication_no: Number(sortItems.id),
              school_year: `${sortItems.school_year.school_year_from} - ${sortItems.school_year.school_year_to}`,
              name: global._.isEmpty(value.user_info) ? '' : `${lastname}, ${firstname} ${middlename} ${extension}`
            })
          })
          this.items = applicants
        }
      },
      watchLoading (isLoading) {
        this.loading = isLoading
      }
    }
  },
  methods: {
    getDeanApplicants (skip,take) {
      this.$apollo.query({
        query: GET_TRANSFEREE_BY_ID,
        variables: {
          input: {
            dean_program_id: this.user.department_id,
            keyword: global._.isEmpty(this.setSearch) ? '' : this.setSearch,
            school_year_id: global._.isUndefined(this.academic_year_id) ? 0 : this.academic_year_id,
            program_id: global._.isUndefined(this.program_id) ? 0 : this.program_id,
            year_level_id: global._.isUndefined(this.year_level_id) ? 0 : this.year_level_id,
            semester_id: global._.isUndefined(this.semester_id) ? 0 : this.semester_id,
            status: this.status,
            skip: skip,
            take: take
          }
        }
      }).then(({ data, loading }) => {
        this.loading = loading
        if (!global._.isUndefined(data)) {
          let self = this
          let applicants = []
          this.count = Number(data.deanApplicants.count)
          global._.forEach(data.deanApplicants.enrollment, function (value) {
            const sortItems = global._.omit(value, 'user_id', 'user_info', 'section')
            const sortUserItem = global._.omit(value.user_info, 'birth_date')
            sortUserItem.birth_date = self.formatDate(value.user_info.birth_date)
            applicants.push({
              ...sortItems,
              ...sortItems.applicant_approve,
              ...sortItems.program,
              ...sortItems.yearlevel,
              ...sortItems.semester,
              ...sortUserItem,
              user_info: sortUserItem,
              id: Number(sortItems.id),
              section: value.section ? value.section.section : '',
              applicant_status: sortItems.applicant_status,
              submission_type: sortItems.submission_type,
              user_status: sortItems.status,
              aplication_no: Number(sortItems.id),
              school_year: `${sortItems.school_year.school_year_from} - ${sortItems.school_year.school_year_to}`,
              name: global._.isEmpty(value.user_info) ? '' : `${global._.capitalize(value.user_info.first_name)} ${self.initialLetter(value.user_info.middle_name)}${global._.capitalize(value.user_info.last_name)}`
            })
          })
          this.items = applicants
        }
      })
      .catch(error => {
        if (error.graphQLErrors) {
          console.log('graphQLErrors:', error.graphQLErrors)
        }
        if (error.networkError) {
          console.log('networkError:', error.networkError)
        }
        if (error.ServerError) {
          console.log('ServerError:', error.ServerError)
        }
      })
    }
  }
}
