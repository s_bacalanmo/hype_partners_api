import { GET_EXAM_SCHEDULE, GET_STUDENT_DISCOUNTS, GET_SEM_BY_CURRICULUM, GET_SY, GET_DISCOUNTS, GET_FEE_CODES, GET_FEE_TYPES, GET_ACCOUNTS, GET_TUITION_AND_OTHER_FEES } from '@/graphql/admin/queries'
import defaultMixins from '@/mixins/index'
import { mapGetters } from 'vuex'
import { format, getDay, getMonth, getYear } from 'date-fns'
export default {
  mixins: [
    defaultMixins
  ],
  computed: {
    ...mapGetters('filters', ['academic_year_id', 'semester_id', 'fee_type_id'])
  },
  apollo: {
    syLists: {
      query: GET_SY,
      result ({ data }) {
        const school_years = []
        if (data) {
          global._.forEach(data.syLists, function (value) {
            school_years.push({
              school_year_id: value.id,
              school_year: `${value.school_year_from} - ${value.school_year_to}`
            })
          })
          this.years = school_years
        }
      }
    },
    semesterCurriculum: {
      query: GET_SEM_BY_CURRICULUM,
      variables () {
        return {
          school_year_id: this.formData.school_year_id
        }
      },
      result ({ data }) {
        let curr_semester = []
        if (!global._.isUndefined(data)) {
          global._.forEach(data.semesterCurriculum, function (value) {
            curr_semester.push({
              semester_id: value.id,
              semester: value.semester
            })
          })
          this.currSemesterItems = curr_semester
        }
      },
      skip () {
        return this.sySemesterQuery
      }
    },
    studentExamSchedQuery: {
      query: GET_EXAM_SCHEDULE,
      variables () {
        return {
          skip: this.skip,
          take: this.take,
          school_year_id: global._.isUndefined(this.academic_year_id) ? null : this.academic_year_id
        }
      },
      result ({ data }) {
        if (!global._.isUndefined(data)) {
          let { formatExamDate } = this
          if (!global._.isNull(data.studentExamSchedQuery)) {
            let syItems = []
            this.yearsCount = Number(data.studentExamSchedQuery.count)
            global._.forEach(data.studentExamSchedQuery.studentSchedExam, function (val) {
              syItems.push({
                ...val,
                semester: val.sem ? val.sem.semester : '',
                school_year: val.sy ? `${val.sy.school_year_from} - ${val.sy.school_year_to}` : '',
                exam_type: val.period ? global._.capitalize(val.period.name) : '' ,
                exam_date: val.exam_date_from && val.exam_date_to ? formatExamDate(val.exam_date_from, val.exam_date_to) : '',
                payment_schedule: val.payment_date ? formatExamDate(val.payment_date, '') : ''
              })
            })
            let schoolYear = global._.sortedUniqBy(syItems, 'school_year')
            this.sortUniqSchoolYear(schoolYear)
            this.asyItems = syItems
          }
        }
      },
      skip () {
        return this.skipExamSchedQuery
      },
      watchLoading (isLoading) {
        this.examLoading = isLoading
      }
    }
  },
  methods: {
    sortUniqSchoolYear (val) {
      let item = val.find(item => item.school_year)
      this.schoolYearItem = item
    },
    formatExamDate(from, to) {
      if (from && to) {
        let timeFrom = new Date(from)
        let formatTimeFrom = timeFrom.getTime()
        let getDayFrom = format(new Date(from), 'd')
        let getMonthFrom = getMonth(new Date(from))
        let formatMonth = format(new Date(from), 'LLL')
        let formatYear = getYear(new Date(from))
        let timeTo = new Date(to)
        let formatTimeTo = timeTo.getTime()
        let getDayTo = format(new Date(to), 'd')
        let getMonthTo = getMonth(new Date(to))
        let formatMonthTo = format(new Date(to), 'LLL')
        let formatYearTo = getYear(new Date(to))
        if (getMonthFrom === getMonthTo) {
          if (getDayFrom !== getDayTo) {
            if (Number(getDayFrom) < Number(getDayTo)) {
              return `${formatMonth}. ${getDayFrom} - ${getDayTo}, ${formatYear}`
            } else {
              return 'Invalid Day (from -To)'
            }
          } else {
            return `${formatMonth}. ${getDayFrom}, ${formatYear}`
          }
        } else {
          if (formatTimeFrom < formatTimeTo) {
            return `${formatMonth}. ${getDayFrom}, ${formatYear} - ${formatMonthTo}. ${getDayTo}, ${formatYearTo}`
          } else {
            return 'Invalid Date (from -To)'
          }
        }
      } else if (from && !to) {
        let getDayFrom = format(new Date(from), 'd')
        let formatMonth = format(new Date(from), 'LLL')
        let formatYear = getYear(new Date(from))
        return `${formatMonth}. ${getDayFrom}, ${formatYear}`
      } else {
        let formatMonthTo = format(new Date(to), 'LLL')
        let getDayTo = format(new Date(to), 'd')
        let formatYearTo = getYear(new Date(to))
        return `${formatMonthTo}. ${getDayTo}, ${formatYearTo}`
      }
    }
  }
}
