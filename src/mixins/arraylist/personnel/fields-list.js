export default {
  data () {
    return {
      fields: [
        {
          key: 'first_name',
          readonly: true,
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'middle_name',
          readonly: true,
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'last_name',
          readonly: true,
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'name_extension',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'gender',
          readonly: true,
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'birth_date',
          readonly: true,
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'email',
          readonly: true,
          classNames: 'xs12 md6 px-2',
          required: true
        },
        {
          key: 'isTransferee',
          label: 'Is the Student Transferee ?',
          classNames: 'xs12 md6 px-2',
          required: true,
          readonly: true
        }
      ],
      personalfields: [
        {
          key: 'address',
          hint: 'House/Block/Lot. No., Street, Subd./Village, Barangay, City/Municipality, Province',
          readonly: true,
          classNames: 'xs12 md6 px-2',
          required: true
        },
        {
          key: 'civil_status',
          readonly: true,
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'religion',
          readonly: true,
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'contact',
          readonly: true,
          type: 'number',
          mask: '###-###-####',
          prefix: "+63",
          classNames: 'xs12 md6 px-2',
          required: true
        },
        {
          key: 'social_account',
          readonly: true,
          label: 'Facebook / Messenger Account',
          prependIcon: 'facebook',
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'student_id_number',
          readonly: true,
          label: 'Student No.',
          classNames: 'xs12 md3 px-2'
        }
      ],
      fathersFields: [
        {
          key: 'fathers_name',
          label: 'Name',
          readonly: true,
          hint: 'Surname, First Name, Middle Name',
          classNames: 'xs12 px-2'
        },
        {
          key: 'fathers_age',
          label: 'Age',
          readonly: true,
          classNames: 'xs12 px-2'
        },
        {
          key: 'fathers_contact_no',
          label: 'Contact Number',
          type: 'number',
          mask: '###-###-####',
          prefix: "+63",
          readonly: true,
          classNames: 'xs12 px-2'
        },
        {
          key: 'fathers_occupation',
          label: 'Occupation',
          readonly: true,
          classNames: 'xs12 px-2'
        },
        {
          key: 'father_works_abroad',
          label: 'Working Abroad',
          readonly: true,
          classNames: 'xs12 px-2'
        }
      ],
      mothersFields: [
        {
          key: 'mothers_name',
          label: 'Name',
          readonly: true,
          hint: 'Surname, First Name, Middle Name',
          classNames: 'xs12 px-2'
        },
        {
          key: 'mothers_age',
          label: 'Age',
          readonly: true,
          classNames: 'xs12 px-2'
        },
        {
          key: 'mothers_contact_no',
          label: 'Contact Number',
          type: 'number',
          mask: '###-###-####',
          prefix: "+63",
          readonly: true,
          classNames: 'xs12 px-2'
        },
        {
          key: 'mothers_occupation',
          label: 'Occupation',
          readonly: true,
          classNames: 'xs12 px-2'
        },
        {
          key: 'mother_works_abroad',
          label: 'Working Abroad',
          readonly: true,
          classNames: 'xs12 px-2'
        }
      ],
      guardianFields: [
        {
          key: 'guardians_name',
          label: 'Name',
          readonly: true,
          hint: 'Surname, First Name, Middle Name',
          classNames: 'xs12 md6 px-2'
        },
        {
          key: 'relation',
          readonly: true,
          classNames: 'xs12 md6 px-2'
        },
        {
          key: 'contact_no',
          type: 'number',
          mask: '###-###-####',
          prefix: "+63",
          readonly: true,
          classNames: 'xs12 md6 px-2'
        },
        {
          key: 'address',
          hint: 'House/Block/Lot. No., Street, Subd./Village, Barangay, City/Municipality, Province',
          readonly: true,
          classNames: 'xs12 md6 px-2'
        },
        {
          key: 'annual_income',
          label: 'Annual Income of Family(Check one below)',
          readonly: true,
          classNames: 'xs12 px-2'
        }
      ],
      careerFields: [
        {
          key: 'first_choice',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'second_choice',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'third_choice',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'major',
          label: 'My course major is chosen by my (choose below)',
          classNames: 'xs12 md3 px-2',
          readonly: true,
          required: true
        }
      ],
      checklistFields: [
        {
          key: 'confidence',
          checked: true,
          fieldTitle: true,
          label: 'Confidence',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'study_habits',
          checked: true,
          fieldTitle: true,
          label: 'Study Habits',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'work_schedule',
          checked: true,
          fieldTitle: true,
          label: 'Work Schedule',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'diet',
          checked: true,
          fieldTitle: true,
          label: 'Diet/ Drugs/ Smoking/ Drinking',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'family',
          checked: true,
          fieldTitle: true,
          label: 'Parents/ Brothers/ Sister',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'health',
          checked: true,
          fieldTitle: true,
          label: 'Health',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'finances',
          checked: true,
          fieldTitle: true,
          label: 'Finances',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'not_interested',
          checked: true,
          fieldTitle: true,
          label: 'Not interested in program',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'appearance',
          checked: true,
          fieldTitle: true,
          label: 'My Appearance',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'class_schedule',
          checked: true,
          fieldTitle: true,
          label: 'Class Schedule',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'undecided',
          checked: true,
          fieldTitle: true,
          label: 'Undecided regarding program',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'concentration',
          checked: true,
          fieldTitle: true,
          label: 'Concentration',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'friends',
          checked: true,
          fieldTitle: true,
          label: 'Friends/ Relationships/ Love',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'privacy',
          checked: true,
          fieldTitle: true,
          label: 'Privacy/ Freedom',
          readonly: true,
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'others',
          checked: true,
          fieldTitle: true,
          readonly: true,
            label: 'Others ( If you check others, please type the   problem or fficulty on the space below )',
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'other_field',
          textarea: true,
          fieldTitle: true,
          label: '',
          rows: 2,
          readonly: true,
          classNames: 'xs12 md3 px-2'
        }
      ],
      educationFields: [
        {
          key: 'elementary_school',
          readonly: true,
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'elem_year_graduated',
          readonly: true,
          label: 'Year Graduated',
          type: 'number',
          classNames: 'xs12 md3  px-2',
          required: true
        },
        {
          key: 'high_school',
          readonly: true,
          classNames: 'xs12 md3  px-2',
          required: true
        },
        {
          key: 'high_school_year_graduated',
          readonly: true,
          label: 'Year Graduated',
          type: 'number',
          classNames: 'xs12 md3 px-2',
          required: true
        },
        {
          key: 'last_school_attended',
          readonly: true,
          label: 'Last School Attended(For Transferees)',
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'last_sy_attended',
          readonly: true,
          label: 'Last S. Y. Attended(For Transferees)',
          classNames: 'xs12 md3 px-2',
          hint: 'Ex. 2020-2021'
        },
        {
          key: 'last_year_level_attended',
          readonly: true,
          label: 'Last Year Level Attended(For Transferees)',
          itemValue: 'year_level_id',
          itemText: 'last_year_level_attended',
          classNames: 'xs12 md3 px-2'
        },
        {
          key: 'last_sem_attended',
          readonly: true,
          label: 'Last Semester Attended(For Transferees)',
          classNames: 'xs12 md3 px-2',
          hint: 'Ex. 1st Semester'
        }
      ],
      personalItem: {
        civil_status: '',
        address: '',
        religion: '',
        contact: '',
        social_account: ''
      },
      fathersItem: {
        fathers_name: '',
        fathers_age: '',
        fathers_contact_no: '',
        fathers_occupation: '',
        father_works_abroad: ''
      },
      mothersItem: {
        mothers_name: '',
        mothers_age: '',
        mothers_contact_no: '',
        mothers_occupation: '',
        mother_works_abroad: ''
      },
      guardiansItem: {
        guardians_name: '',
        relation: '',
        contact_no: '',
        address: '',
        annual_income: ''
      },
      careerItem: {
        first_choice: '',
        second_choice: '',
        third_choice: '',
        major: ''
      },
      checklistItem: {
        confidence: '',
        study_habits: '',
        work_schedule: '',
        diet: '',
        family: '',
        health: '',
        finances: '',
        not_interested: '',
        appearance: '',
        class_schedule: '',
        undecided: '',
        concentration: '',
        friends: '',
        privacy: '',
        others: '',
        other_field: ''
      },
      educationItem: {
        elementary_school: '',
        elem_year_graduated: '',
        high_school: '',
        high_school_year_graduated: '',
        last_school_attended: '',
        last_sy_attended: '',
        last_year_level_attended: '',
        last_sem_attended: ''
      }
    }
  },
  computed: {
    choices () {
      return {
        gender: ['Male', 'Female'],
        applicantStatus: ['Yes', 'No']
      }
    }
  }
}
