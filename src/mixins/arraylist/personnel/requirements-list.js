export default {
  data () {
    return {
      maritimeItems: [
        {
          label: 'Failed Ishihara Test/ Colorblind Test'
        },
        {
          label: 'Failed Hearing Test'
        },
        {
          label: 'Cleft Lip/ Cleft Palete'
        },
        {
          label: 'Dextroscoliosis (Scoliosis)'
        },
        {
          label: 'With history of Asthma'
        },
        {
          label: 'Hepatitis B Antegen Positive'
        },
        {
          label: 'PTB Active'
        },
        {
          label: 'Crossed Eye'
        }
      ]
    }
  }
}
