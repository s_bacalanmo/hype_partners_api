export default {
  data: () => ({
    medical_reqs: [
      'Chest X Ray',
      'Electrocardiogram',
      'Complete Blood Count',
      'Blood Typing',
      'Urinalysis',
      'Stool Examination',
      'Hepatitis B Antegen',
      'Hepatitis B Antibodies (Optional)',
      'Ishihara Test',
      'Hearing Test',
      'Drug Test',
      'Electrocardiogram',
      'Physical Exam',
      'Medical Certificate',
      'Visual Acuity'
    ],
    admission_reqs: [
      'Entrance Exam Result',
      'Transcript Of Records',
      'Certificate Of Honorable Dismissal (original)',
      'High School Report Card (original)',
      'Good Moral Character Certificate (original)',
      'PSA Authenticated Birth Certificate (original)',
      'APRO Exam Result'
    ]
  })
}
