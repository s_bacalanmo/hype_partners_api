export default {
  data: () => ({
    checklist: [
      {
        title: 'Full Name',
        disabled: false
      },
      {
        title: 'Email Address',
        disabled: false
      },
      {
        title: 'Date of Birth',
        disabled: false
      },
      {
        title: 'Contact Number',
        disabled: false
      },
      {
        title: 'Freshman or Transferee',
        disabled: false
      },
      {
        title: 'Program',
        disabled: false
      },
      {
        title: 'Send Digital ID Picture',
        disabled: false
      },
      {
        title: 'Picture/screenshot of payment slip with COMPLETE NAME at the very top of the payment slip',
        disabled: false
      },
      {
        title: 'Others',
        disabled: true
      }
    ],
    historyHeaders: [
      {
        text: 'Date',
        value: 'created_at',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      },
      {
        text: 'Status',
        value: 'status',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      },
      {
        text: 'Office',
        value: 'office',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      },
      {
        text: 'Reason',
        value: 'files',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      }
    ]
  })
}
