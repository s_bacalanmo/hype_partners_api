export default {
  data: () => ({
    historyHeaders: [
      {
        text: 'Date',
        value: 'created_at',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      },
      {
        text: 'Status',
        value: 'status',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      },
      {
        text: 'Office',
        value: 'office',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      },
      {
        text: 'Files',
        value: 'files',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      },
      {
        text: 'Reason',
        value: 'reason',
        sortable: true,
        align: 'center',
        title: '',
        name: ''
      }
    ]
  })
}
