// Mixin for parsing and coverting backend error response
// into an accessible data that can be used to display
// error messages in the form

export default {
  data () {
    return {
      errorData: {}
    }
  },
  methods: {
    graphQLErrorMessages (errorsFromCatch) {
      console.log(errorsFromCatch)
      const errors = errorsFromCatch.graphQLErrors
      const messages = []

      if (errors.hasOwnProperty('functionError')) {
        const customErrors = JSON.parse(errors.functionError)
        messages.push(...customErrors.errors)
      } else {
        messages.push(errors.message)
      }

      return messages
    },
    clearErrorData () {
      this.errorData = {}
    },
    getErrorMessages (fieldName) {
      /*
        Returns array of error messages based on the given `key`.
      */
      if (!fieldName) {
        return []
      }
      let error = this.errorData
      let errors = []
      console.log(error)
      fieldName = 'first_name'
      for (const keyName of fieldName.split('.')) {
        if (!error) {
          break
        }
        console.log(fieldName)
        for (const key of error) {
          console.log(global._.includes(key.message, `${fieldName}`))
          if (global._.includes(key.message, `${fieldName}`)) {
            const keyName = fieldName.replace(/_/g, ' ')
            const splitKey = keyName.split(' ')
            const upFirstCharKey = splitKey.map((word) => {
              return word[0].toUpperCase() + word.slice(1)
            })
            const upKey = upFirstCharKey.join(' ')
            const message = key.message.replace('Field', `${upKey}`)
            const sortMessage = message.replace(`at value.${fieldName}`, ' ')
            const customErrors = sortMessage.replace(`.`, ' ')
            JSON.stringify(`${fieldName}`)
            errors.push(customErrors)
            console.log(errors)
          }
        }
      }

      if (errors) {
        if (Array.isArray(errors)) {
          console.log('isArray:', errors)
          return errors
        } else if (typeof errors === 'string') {
          console.log('string:', [errors])
          return [errors]
        } else if (typeof errors === 'object' && 'message' in errors) {
          console.log('object / string:', Array.isArray(errors.message) ? errors.message : [errors.message])
          return Array.isArray(errors.message) ? errors.message : errors.message
        }
        console.log('default:', errors)
      }
      return []
    },
    setErrorData (err) {
      if (err) {
        this.errorData = err.graphQLErrors
        console.log(this.errorData)
      } else {
        this.clearErrorData()
      }
    },
    showCommonErrorMessage () {
      const message = this.getErrorMessages('non_field_errors')
      if (message.length > 0) {
        // global.toastr['error'](message[0], 'Error')
      }
    }
  }
}
