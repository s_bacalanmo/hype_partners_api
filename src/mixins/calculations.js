export default {
  methods: {
    calculateGwa (val) {
      let sumofGrades = Number(val.prelim) + Number(val.midterm) + Number(val.semi_finals) + Number(val.finals)
      let totals = sumofGrades / 4
      return totals
    },
    totalAmount (items) {
      if (!global._.isEmpty(items)) {
        let total = global._.reduce(global._.mapValues(items, function (o) { return o.fee_amount }), function (sum, num) {
         return Number(sum) + Number(num)
        })
        return global._.round(total, 2)
      } else {
        return 0.00
      }
    }
  }
}
