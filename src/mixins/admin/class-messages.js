export default {
  computed: {
    days () {
      return [{
        title: 'Monday',
        value: 'M'
      },
      {
        title: 'Tuesday',
        value: 'T'
      },
      {
        title: 'Wednesday',
        value: 'W'
      },
      {
        title: 'Thursday',
        value: 'Th'
      },
      {
        title: 'Friday',
        value: 'F'
      },
      {
        title: 'Saturday',
        value: 'Sa'
      },
      {
        title: 'Sunday',
        value: 'Su'
      }]
    },
    lecHours () {
      if (!global._.isEmpty(this.courseItems)) {
        const item = this.courseItems.find(item => Number(item.course_id) === Number(this.formData[0].course_id))
        if (!global._.isUndefined(item)) return Number(item.lec_hours)
        return ''
      }
    },
    labHours () {
      if (!global._.isEmpty(this.courseItems)) {
        const item = this.courseItems.find(item => Number(item.course_id) === Number(this.formData[0].course_id))
        if (!global._.isUndefined(item)) return Number(item.lab_hours)
        return ''
      }
    },
    isOjT () {
      if (!global._.isEmpty(this.courseItems)) {
        const item = this.courseItems.find(item => Number(item.course_id) === Number(this.formData[0].course_id))
        if (!global._.isUndefined(item)) return (Number(item.lab_hours) === 0 && Number(item.lec_hours) === 0)
        return ''
      }
    },
    promptMessage () {
      let hasLec = Boolean(global._.isEmpty(this.dayItems) && this.lecHours > 0) // empty fields but has lec hours
      let hasLab = Boolean(global._.isEmpty(this.dayLabItems) && this.labHours > 0) // empty fields but has lab hours

      let hasTime = this.dayItems.filter(item => item.from === '' || item.to === '')
      let hasTimeLab = this.dayLabItems.filter(item => item.from === '' || item.to === '')

      let hasLecTime = Boolean(!global._.isEmpty(hasTime) && this.lecHours > 0) // has fields and has lec hours
      let hasLabTime = Boolean(!global._.isEmpty(hasTimeLab) && this.labHours > 0) // has fields and has lab hours
      if (hasLec && hasLab) {
        return 'Must have Lec and Lab Schedule/s.'
      } else if (hasLecTime && hasLabTime) {
        return `Must have Lec and Lab Schedule/s.`
      } else if (hasLec && hasLabTime) {
        return 'Must have Lec and Lab Schedule/s.'
      } else if (hasLab && hasLecTime) {
        return 'Must have Lec and Lab Schedule/s.'
      } else if (!hasLec && hasLab) {
        return 'Must have Lab Schedule/s.'
      } else if (!hasLecTime && hasLabTime) {
        return 'Must have Lab Schedule/s.'
      } else if (hasLec && !hasLab) {
        return 'Must have Lec Schedule/s.'
      } else if (hasLecTime && !hasLabTime) {
        return 'Must have Lec Schedule/s.'
      } else {
        return 'Do you want to save changes ?'
      }
    },
    lecSize () {
      if (this.selectedLec) {
        return Number(this.selectedLec.length)
      }
    },
    labSize () {
      if (this.selectedLab) {
        return Number(this.selectedLab.length)
      }
    }
  }
}
