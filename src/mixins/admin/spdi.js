
import { mapGetters } from 'vuex'
export default {
  computed: {
    ...mapGetters('user', ['registrar', 'user']),
    userTypeId () {
      if (this.registrar) {
        if (!global._.isUndefined(this.registrar.student_user)) {
          return this.registrar.student_user.user_type_id
        } else {
          return this.registrar.user_info.user_type_id
        }
      }
    },
    isMarine () {
      if (!global._.isEmpty(this.registrar)) {
        let name = this.registrar.program.program_name
        if (name) {
          let lowerCaseName = name.toLowerCase()
          let isMarine = lowerCaseName.includes('marine')
          return isMarine
        }
      }
    },
    type () {
      if (this.registrar) return this.registrar.submission_type
      return ''
    }
  },
  methods: {
    resetFamilyErrors () {
      this.$refs.spdi[0].$refs.fathers[0].errors = {}
      this.$refs.spdi[0].$refs.mothers[0].errors = {}
      this.$refs.spdi[0].$refs.guardians[0].errors = {}
    },
    resetPersonalErrors () {
      this.personalErrors = {}
      this.$refs.spdi[0].$refs.career[0].errors = {}
      this.$refs.spdi[0].$refs.checklist[0].errors = {}
    },
    resetEducErrors () {
      this.educErrors = {}
    },
    setUpdatedby (iteratee, item) {
      if (!global._.isUndefined(iteratee)) {
        let lists = iteratee.split(',')
        if (!global._.includes(lists, item)) {
          let arr = global._.union(lists, global._.toArray(item))
          return String(global._.uniq(arr.map(item => item)))
        } else {
          return iteratee
        }
      } else {
        return item
      }
    },
    isStudent () {
      // Applied for Student Type Only
      if (this.userTypeId === 4) {
        let item = {
          password: '',
          isTransferee: global._.isUndefined(this.registrar.isTransferee) ? (this.registrar.applicant_status === 'Transferee' ? 'Yes' : 'No') : this.registrar.isTransferee,
          status: this.registrar.status,
          student_status: this.registrar.student_status,
          gender: !global._.isUndefined(this.registrar.student_user) ? this.textCapitalize(this.registrar.student_user.gender) : this.textCapitalize(this.registrar.user_info.gender)
        }
        this.form[0] = {...global._.omit(this.registrar.user_info, 'status'), ...global._.omit(this.registrar.student_user, 'status', 'gender'), ...item}
        if (!global._.isUndefined(this.registrar.student_user)) {
          this.addStudentFields()
        }
      } else {
        let item = {
          password: '',
          isTransferee: this.registrar.applicant_status === 'Transferee' ? 'Yes' : 'No',
          status: this.registrar.status,
          gender: this.textCapitalize(this.registrar.user_info.gender)
        }
        this.form[0] = {...global._.omit(this.registrar.user_info, 'status'), ...item }
      }
    },
    addStudentFields () {
      let fieldsExist = this.fields.filter(item => item.status === 'status' || item.key === 'student_status' || item.key === 'password')
      if (global._.isEmpty(fieldsExist)) {
        if (this.userType === 8 || this.userType === 9) {
          this.fields.push(
            {
              key: 'status',
              label: 'Type',
              choices: 'types',
              classNames: 'xs12 md3 px-2',
              required: true
            },
            {
              key: 'student_status',
              label: 'Status',
              choices: 'student_statuses',
              classNames: 'xs12 md3 px-2',
              required: true
            },
            {
              key: 'password',
              label: 'Update Password',
              password: true,
              classNames: 'xs12 md6 px-2',
              counter: 8,
              rule: [v => (v && v.length) >= 8 || 'password must be atleast 8 characters']
            }
          )
        } else {
          this.fields.push(
            {
              key: 'status',
              label: 'Type',
              choices: 'types',
              classNames: 'xs12 md3 px-2',
              required: true,
              readonly: true,
              clearable: false
            },
            {
              key: 'student_status',
              label: 'Status',
              choices: 'student_statuses',
              classNames: 'xs12 md3 px-2',
              required: true,
              readonly: true,
              clearable: false
            }
          )
        }
      }
    },
    updatedFunc (title) {
      let items = this.items[0].tabs
      let isUpdate = items.find(item => item.title === title)
      return isUpdate
    },
    setEducData (id, data) {
      if (data) {
        data.id = data.user_id
        this.educationForm[0] = data
        let isUpdated = this.updatedFunc('Educational Background')
        isUpdated.update = global._.isUndefined(data.updated_by) ? '' : data.updated_by.split(',')
      } else {
        this.educationForm[0] = { ...id, ...this.educationItem }
      }
    },
    setFamData (id, data) {
      if (data) {
        data.id = data.user_id
        this.fathersForm[0] = global._.pick(data, 'icon', 'updated_by', 'id', 'fathers_name', 'fathers_age', 'fathers_contact_no', 'fathers_occupation', 'father_works_abroad')
        this.mothersForm[0] = global._.pick(data, 'icon', 'id', 'mothers_name', 'mothers_age', 'mothers_contact_no', 'mothers_occupation', 'mother_works_abroad')
        this.guardiansForm[0] = global._.pick(data, 'icon', 'id', 'guardians_name', 'relation', 'contact_no', 'address', 'annual_income')

        let isUpdated = this.updatedFunc('Family Information')
        isUpdated.update = global._.isUndefined(data.updated_by) ? '' : data.updated_by.split(',')
      } else {
        this.fathersForm[0] = { ...id, ...this.fathersItem }
        this.mothersForm[0] = { ...id, ...this.mothersItem }
        this.guardiansForm[0] = { ...id, ...this.guardiansItem }
      }
    },
    setPersonalData (id, data) {
      if (data) {
        data.id = data.user_id
        let items = global._.pick(data, 'student_id_number', 'id','updated_by', 'social_account', 'icon', 'address', 'civil_status', 'contact', 'religion')
        if (global._.size(items) <= 2) {
          this.personalForm[0] = {...items, ...id, ...this.personalItem}
        } else {
          this.personalForm[0] = items
        }
        this.careerForm[0] =  global._.pick(data, 'icon', 'id',  'first_choice', 'second_choice', 'third_choice', 'major')
        this.checklistForm[0] = global._.omit(data,  'icon', 'id', 'first_choice', 'second_choice', 'third_choice', 'major', 'social_account', 'address', 'civil_status', 'contact', 'religion')

        let isUpdated = this.updatedFunc('Personal Information')
         isUpdated.update = global._.isUndefined(data.updated_by) ? '' : data.updated_by.split(',')
      } else {
        this.personalForm[0] = { ...id, ...this.personalItem, student_id_number: '' }
        this.careerForm[0] = { ...id, ...this.careerItem }
        this.checklistForm[0] = { ...id, ...this.checklistItem }
      }
    },
    findTab (items, name) {
      let tabExist = this.items.find(item => item.title === name)
      if (global._.isUndefined(tabExist)) {
        items.push(
          {
            title: name
          }
        )
        return items
      }
    },
    setTabs () {
      if (this.registrar.user_type_id === 4) {
        if (this.registrar.school_year) {
            // graduating student
          if (this.registrar.year_level === '4th Year') {
            this.findTab(this.items, 'Subject Overload')
          }
          // transferee student
          if (this.registrar.isTransferee === 'Yes') {
            this.findTab(this.items, 'Subject Equivalency')
          }
          if (Number(global._.trim(this.user.user_type_id)) === 8 || Number(global._.trim(this.user.user_type_id)) === 9 || (Number(global._.trim(this.user.user_type_id)) === 5 && this.registrar.isTransferee === 'Yes')) {
            let requirementsExist = this.items.find(item => item.title === 'Requirements')
            if (global._.isUndefined(requirementsExist)) {
              this.items.push(
                {
                  title: 'Requirements'
                }
              )
            }
          }
          // student
          if (this.type === 'Student') {
            let tabsExist = this.items.filter(item => item.title === 'Courses' || item.title === 'Adding & Dropping')
            if (global._.isEmpty(tabsExist)) {
              this.items.push(
                {
                  title: 'Adding & Dropping'
                }
              )
            }
          } else {
          // re-enroll student
            if (this.type === 'Enrollment') {
              this.findTab(this.items, 'Courses')
            }
          }
          // old student
          this.findTab(this.items, 'Student Evaluation')
          if (Number(global._.trim(this.user.user_type_id)) === 8) {
            this.findTab(this.items, 'Resources')
          }
          if (Number(global._.trim(this.user.user_type_id)) === 9) {
            let tabsExist = this.items.filter(item => item.title === 'Resources' || item.title === 'Assessment')
            if (global._.isEmpty(tabsExist)) {
              this.items.push(
                {
                  title: 'Assessment'
                },
                {
                  title: 'Resources'
                }
              )
            }
          }
          // old student
          if (this.paramType === 'Old' || this.paramType === 'Returnee') {
            this.findTab(this.items, 'Enrollment History')
          }
        } else {
          // old student
            this.findTab(this.items, 'Student Evaluation')
        }
      } else {
        if (Number(global._.trim(this.user.user_type_id)) === 8 || Number(global._.trim(this.user.user_type_id)) === 9 || (Number(global._.trim(this.user.user_type_id)) === 5 && this.registrar.isTransferee === 'Yes')) {
          this.findTab(this.items, 'Requirements')
        }
        // new enrollee
        if (global._.trim(this.registrar.applicant_status) === 'Transferee') {
          let tabsExist = this.items.filter(item => item.title === 'Subject Equivalency' || item.title === 'Student Evaluation' || item.title === 'Courses')
          if (global._.isEmpty(tabsExist)) {
            this.items.push(
              {
                title: 'Subject Equivalency'
              },
              {
                title: 'Student Evaluation'
              },
              {
                title: 'Courses'
              }
            )
          }
          if (Number(global._.trim(this.user.user_type_id)) === 8) {
            this.findTab(this.items, 'Resources')
          }
          if (Number(global._.trim(this.user.user_type_id)) === 9) {
            let tabsExist = this.items.filter(item => item.title === 'Resources' || item.title === 'Assessment')
            if (global._.isEmpty(tabsExist)) {
              this.items.push(
                {
                  title: 'Assessment'
                },
                {
                  title: 'Resources'
                }
              )
            }
          }
        } else {
          this.findTab(this.items, 'Courses')
        }
      }
    },
    setFinanceTabs () {
     if (this.registrar.school_year) {
        if (this.registrar.type !== 'Entrance Exam') {
          let tabsExist = this.items.filter(item => item.title === 'Charges/Adjustments' || item.title === 'Discount' || item.title === 'Assessment' || item.title === 'Payment' || item.title === 'Account History')
          if (global._.isEmpty(tabsExist)) {
            this.items.push(
              {
                title: 'Charges/Adjustments'
              },
              {
                title: 'Discount'
              },
              {
                title: 'Assessment'
              },
              {
                title: 'Payment'
              },
              {
                title: 'Account History'
              }
            )
          }
        } else {
          this.findTab(this.items, 'Payment')
        }
      }
    }
  }
}