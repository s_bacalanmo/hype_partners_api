export default {
  methods: {
    resetChecklist () {
      let resetChecklist = this.checklist.map((item, index) => {
        if (item.title !== 'Others') {
          if (item.disabled === true) {
            return {
              title: item.title,
              disabled: false
            }
          } else {
            return {
              title: item.title,
              disabled: item.disabled
            }
          }
        } else {
          return {
            title: item.title,
            disabled: true
          }
        }
      })
      this.checklist = resetChecklist
    },
    checkEmailItem (data, index) {
      if (index !== -1) {
        if (this.approvalId === 1) {
          this.checklist[index].title = data.title
          this.checklist[index].disabled = data.disabled ? false : true
        }
      }
      let size = this.checklist.filter(item => item.title !== 'Others' && item.disabled === false).length
      if (size < 8) {
        this.isCheckedAll = true
      } else {
        this.isCheckedAll = false
      }
    }
  }
}
