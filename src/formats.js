import { format } from 'date-fns'

export const capitalize = function (value) {
  if (!value) return ''
  value = global._.capitalize(value)
  let str = value.split(' ')
   for (var i = 0; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1)
  }
  return str.join(' ')
}

export const initials = function (value) {
  if (!value) return ''
  return `${value.charAt(0).toUpperCase()}`
}

export const uppercase = function (value) {
  if (!value) return ''
  return `${value.toUpperCase()}`
}
export const date = function (value) {
  if (!value) return ''
  return format(new Date(value), 'MMMM dd, yyyy @ hh:mm a')
}
