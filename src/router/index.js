import Vue from 'vue'
import VueRouter from 'vue-router'
import { createStore } from '@/store'

Vue.use(VueRouter)

// ----------------- common --------------------------------------------------------------------------
const Login = () => import(/* webpackChunkName: "login" */ '@/components/common/Login.vue')
const SignUp = () => import(/* webpackChunkName: "sign-up" */ '@/components/common/SignUp.vue')
const ForgotPassword = () => import(/* webpackChunkName: "forgot-password" */ '@/components/common/ForgotPassword.vue')
const Reset = () => import(/* webpackChunkName: "reset" */ '@/components/common/Reset.vue')
const ResetForm = () => import(/* webpackChunkName: "reset-form" */ '@/components/common/ResetForm.vue')
const Profile = () => import(/* webpackChunkName: "profile" */ '@/components/common/Profile.vue')
// ----------------- pages --------------------------------------------------------------------------
const AdminPage = () => import(/* webpackChunkName: "admin-page" */ '@/pages/AdminPage.vue')
const AdmissionPage = () => import(/* webpackChunkName: "admission-page" */ '@/pages/AdmissionPage.vue')
const ApplicantPage = () => import(/* webpackChunkName: "applicant-page" */ '@/pages/ApplicantPage.vue')
const ClinicPage = () => import(/* webpackChunkName: "clinic-page" */ '@/pages/ClinicPage.vue')
const DepartmentPage = () => import(/* webpackChunkName: "department-page" */ '@/pages/DepartmentPage.vue')
const FinancePage = () => import(/* webpackChunkName: "finance-page" */ '@/pages/FinancePage.vue')
const GuidancePage = () => import(/* webpackChunkName: "guidance-page" */ '@/pages/GuidancePage.vue')
const RegistrarPage = () => import(/* webpackChunkName: "registrar-page" */ '@/pages/RegistrarPage.vue')
const StudentPage = () => import(/* webpackChunkName: "student-page" */ '@/pages/StudentPage.vue')
const TeacherPage = () => import(/* webpackChunkName: "teacher-page" */ '@/pages/TeacherPage.vue')
// ----------------- admin page ---------------------------------------------------------------------
const Class = () => import(/* webpackChunkName: "class" */ '@/components/admin/class/Class.vue')
const RoomTable = () => import(/* webpackChunkName: "room-table" */ '@/components/admin/class/RoomTable.vue')
const SectionTable = () => import(/* webpackChunkName: "section-table" */ '@/components/admin/class/SectionTable.vue')
const TeacherTable = () => import(/* webpackChunkName: "teacher-table" */ '@/components/admin/class/TeacherTable.vue')
const Course = () => import(/* webpackChunkName: "course" */ '@/components/admin/curriculum/Course.vue')
const CourseMenu = () => import(/* webpackChunkName: "course-menu" */ '@/components/admin/course/CourseMenu.vue')
const NavProgram = () => import(/* webpackChunkName: "program-page" */ '@/components/admin/program/Program.vue')
const User = () => import(/* webpackChunkName: "user" */ '@/components/admin/user/User.vue')
const AdminTabs = () => import(/* webpackChunkName: "admin-tabs" */ '@/components/admin/AdminTabs.vue')
const ProgramCurriculum = () => import(/* webpackChunkName: "program-curriculum" */ '@/components/admin/program/ProgramCurriculum.vue')
const Revisions = () => import(/* webpackChunkName: "revisions" */ '@/components/admin/program/ProgramRevisions.vue')
const SchoolYear = () => import(/* webpackChunkName: "school-year" */ '@/components/admin/schoolYear/SchoolYear.vue')
// ----------------- global ---------------------------------------------------------------------
const AdminApplicants = () => import(/* webpackChunkName: "admin-applicants" */ '@/components/admin/AdminApplicants.vue')
const AdminApplicantsEntranceExam = () => import(/* webpackChunkName: "admin-applicants-entrance-exam" */ '@/components/admin/AdminApplicantsEntranceExam.vue')
const GuidanceEntranceApplicants = () => import(/* webpackChunkName: "guidance-entrance-applicants" */ '@/components/guidance/GuidanceEntranceApplicants.vue')
const FinanceEntranceApplicants = () => import(/* webpackChunkName: "finance-entrance-applicants" */ '@/components/finance/FinanceEntranceApplicants.vue')
const AdmissionApplicants = () => import(/* webpackChunkName: "admission-applicants" */ '@/components/admission/AdmissionApplicants.vue')
const ClinicApplicants = () => import(/* webpackChunkName: "clinic-applicants" */ '@/components/clinic/ClinicApplicants.vue')
const DepartmentApplicants = () => import(/* webpackChunkName: "department-applicants" */ '@/components/department/DepartmentApplicants.vue')
const FinanceApplicants = () => import(/* webpackChunkName: "finance-applicants" */ '@/components/finance/FinanceApplicants.vue')
const GuidanceApplicants = () => import(/* webpackChunkName: "guidance-applicants" */ '@/components/guidance/GuidanceApplicants.vue')
const RegistrarApplicants = () => import(/* webpackChunkName: "registrar-applicants" */ '@/components/registrar/RegistrarApplicants.vue')
const Students = () => import(/* webpackChunkName: "students" */ '@/components/students/Students.vue')
const AdminStudents = () => import(/* webpackChunkName: "admin-students" */ '@/components/admin/AdminStudents.vue')
const RegistrarStudents = () => import(/* webpackChunkName: "registrar-students" */ '@/components/registrar/RegistrarStudents.vue')
const AdmissionStudents = () => import(/* webpackChunkName: "admission-students" */ '@/components/admission/AdmissionStudents.vue')
const ClinicStudents = () => import(/* webpackChunkName: "clinic-students" */ '@/components/clinic/ClinicStudents.vue')
const DepartmentStudents = () => import(/* webpackChunkName: "department-students" */ '@/components/department/DepartmentStudents.vue')
const GuidanceStudents = () => import(/* webpackChunkName: "guidance-students" */ '@/components/guidance/GuidanceStudents.vue')
const FinanceStudents = () => import(/* webpackChunkName: "finance-students" */ '@/components/finance/FinanceStudents.vue')
// ----------------- filters ---------------------------------------------------------------------
const AcademicYear = () => import(/* webpackChunkName: "academic-year" */ '@/components/filters/admin/AcademicYear.vue')
const Program = () => import(/* webpackChunkName: "program" */ '@/components/filters/admin/Program.vue')
const CourseFilter = () => import(/* webpackChunkName: "class" */ '@/components/filters/admin/class/Course.vue')
const ClassCodeFilter = () => import(/* webpackChunkName: "class-code" */ '@/components/filters/admin/class/ClassCode.vue')
const DateApplied = () => import(/* webpackChunkName: "date-applied" */ '@/components/filters/admin/applicant/DateApplied.vue')
const EmailStatus = () => import(/* webpackChunkName: "email-status" */ '@/components/filters/admin/applicant/EmailStatus.vue')
const EntranceStatus = () => import(/* webpackChunkName: "entrance-status" */ '@/components/filters/admin/applicant/EntranceStatus.vue')
const UnitFilter = () => import(/* webpackChunkName: "unit" */ '@/components/filters/admin/class/Unit.vue')
const TimeFilter = () => import(/* webpackChunkName: "time" */ '@/components/filters/admin/class/Time.vue')
const DayFilter = () => import(/* webpackChunkName: "day" */ '@/components/filters/admin/class/Day.vue')
const RoomFilter = () => import(/* webpackChunkName: "room" */ '@/components/filters/admin/class/Room.vue')
const SectionFilter = () => import(/* webpackChunkName: "section" */ '@/components/filters/admin/class/Section.vue')
const InstructorFilter = () => import(/* webpackChunkName: "instructor" */ '@/components/filters/admin/class/Instructor.vue')
const DepartmentFilter = () => import(/* webpackChunkName: "department" */ '@/components/filters/admin/user/Department.vue')
const PositionFilter = () => import(/* webpackChunkName: "position" */ '@/components/filters/admin/user/Position.vue')
const SemesterFilter = () => import(/* webpackChunkName: "semester" */ '@/components/filters/admin/student/Semester.vue')
const StatusFilter = () => import(/* webpackChunkName: "status" */ '@/components/filters/admin/student/Status.vue')
const YearLevelFilter = () => import(/* webpackChunkName: "year-level" */ '@/components/filters/admin/student/YearLevel.vue')
const AppStatusFilter = () => import(/* webpackChunkName: "applicant-status" */ '@/components/filters/admin/applicant/AppStatus.vue')
const DiscountTypes = () => import(/* webpackChunkName: "discount-types" */ '@/components/filters/finance/DiscountTypes.vue')
const PaymentMode = () => import(/* webpackChunkName: "payment-mode" */ '@/components/filters/finance/PaymentMode.vue')
const FeeTypes = () => import(/* webpackChunkName: "fee-type" */ '@/components/filters/finance/FeeTypes.vue')
const AccountTypes = () => import(/* webpackChunkName: "fee-type" */ '@/components/filters/finance/AccountTypes.vue')
// ----------------- applicant page ---------------------------------------------------------------------
const Dashboard = () => import(/* webpackChunkName: "dashboard" */ '@/components/applicants/dashboard/Dashboard.vue')
const ViewCurriculum = () => import(/* webpackChunkName: "view-curriculum" */ '@/components/applicants/dashboard/ViewCurriculum.vue')
const Enrollment = () => import(/* webpackChunkName: "enrollment" */ '@/components/applicants/enrollment/Enrollment.vue')
const EnrollmentForm = () => import(/* webpackChunkName: "enrollment-form" */ '@/components/applicants/enrollment/EnrollmentForm.vue')
const PersonalInfo = () => import(/* webpackChunkName: "personal-info" */ '@/components/applicants/enrollment/forms/PersonalInfo.vue')
const EducationalInfo = () => import(/* webpackChunkName: "educational-info" */ '@/components/applicants/enrollment/forms/EducationalInfo.vue')
const FamilyInfo = () => import(/* webpackChunkName: "family-info" */ '@/components/applicants/enrollment/forms/FamilyInfo.vue')
const Payment = () => import(/* webpackChunkName: "payment" */ '@/components/applicants/enrollment/forms/Payment.vue')
const ApplicationInfo = () => import(/* webpackChunkName: "application-info" */ '@/components/applicants/enrollment/forms/ApplicationInfo.vue')
const Requirements = () => import(/* webpackChunkName: "requirements" */ '@/components/applicants/enrollment/forms/Requirements.vue')
const MisDashboard = () => import(/* webpackChunkName: "mis-dashboard" */ '@/components/dashboard/admin/Dashboard.vue')
const Courses = () => import(/* webpackChunkName: "coureses" */ '@/components/applicants/enrollment/forms/Courses.vue')
// ----------------- teacher page ---------------------------------------------------------------------
const ClassList = () => import(/* webpackChunkName: "teacher" */ '@/components/teacher/ClassList.vue')
const ClassListStudents = () => import(/* webpackChunkName: "class-list-students" */ '@/components/teacher/ClassListStudents.vue')
// ----------------- student page ---------------------------------------------------------------------
const StudentAccount = () => import(/* webpackChunkName: "student-account" */ '@/components/students/StudentAccount.vue')
const StudentForm = () => import(/* webpackChunkName: "student-form" */ '@/components/students/StudentForm.vue')
const StudentCourses = () => import(/* webpackChunkName: "student-courses" */ '@/components/students/StudentCourses.vue')
const StudentDeficiencies = () => import(/* webpackChunkName: "student-deficiencies" */ '@/components/students/StudentDeficiencies.vue')
const StudentGrades = () => import(/* webpackChunkName: "student-grades" */ '@/components/students/StudentGrades.vue')
const StudentSchedule = () => import(/* webpackChunkName: "student-schedule" */ '@/components/students/StudentSchedule.vue')
const EnrollOnline = () => import(/* webpackChunkName: "enroll-online" */ '@/components/students/EnrollOnline.vue')
const StudentEnrollmentForm = () => import(/* webpackChunkName: "student-enrollment-form" */ '@/components/students/StudentEnrollmentForm.vue')
const StudentApplicationInfo = () => import(/* webpackChunkName: "student-application-info" */ '@/components/students/form/StudentApplicationInfo.vue')
const StudentPersonalInfo = () => import(/* webpackChunkName: "student-personal-info" */ '@/components/students/form/StudentPersonalInfo.vue')
const StudentEducationalInfo = () => import(/* webpackChunkName: "student-educational-info" */ '@/components/students/form/StudentEducationalInfo.vue')
const StudentFamilyInfo = () => import(/* webpackChunkName: "student-family-info" */ '@/components/students/form/StudentFamilyInfo.vue')
const StudentRequirements = () => import(/* webpackChunkName: "student-requirements" */ '@/components/students/form/StudentRequirements.vue')
const StudentCourseForm = () => import(/* webpackChunkName: "student-course-form" */ '@/components/students/form/StudentCourses.vue')
const StudentPayment = () => import(/* webpackChunkName: "student-payment" */ '@/components/students/form/StudentPayment.vue')
// ----------------- registrar page ---------------------------------------------------------------------
const RegistrarTabs = () => import(/* webpackChunkName: "registrar-tabs" */ '@/components/registrar/RegistrarTabs.vue')
const RegistrarClass = () => import(/* webpackChunkName: "registrar-class" */ '@/components/registrar/RegistrarClass.vue')
const RegistrarSectionTable = () => import(/* webpackChunkName: "registrar-section-table" */ '@/components/registrar/RegistrarSectionTable.vue')
const RegistrarRoomTable = () => import(/* webpackChunkName: "registrar-room-table" */ '@/components/registrar/RegistrarRoomTable.vue')
const RegistrarTeacherTable = () => import(/* webpackChunkName: "registrar-teacher-table" */ '@/components/registrar/RegistrarTeacherTable.vue')
const ExamSchedule = () => import(/* webpackChunkName: "registrar-exam-schedule" */ '@/components/registrar/examination/ExamSchedule.vue')
// ----------------- admission page ---------------------------------------------------------------------
const AdmissionTabs = () => import(/* webpackChunkName: "admission-tabs" */ '@/components/admission/AdmissionTabs.vue')
// ----------------- clinic page ---------------------------------------------------------------------
const ClinicTabs = () => import(/* webpackChunkName: "clinic-tabs" */ '@/components/clinic/ClinicTabs.vue')
// ----------------- department page ---------------------------------------------------------------------
const DepartmentTabs = () => import(/* webpackChunkName: "department-tabs" */ '@/components/department/DepartmentTabs.vue')
const DepartmentCurriculum = () => import(/* webpackChunkName: "dept-curriculum" */ '@/components/department/DepartmentCurriculum.vue')
const DepartmentCourse = () => import(/* webpackChunkName: "dept-course" */ '@/components/department/DepartmentCourse.vue')
const DepartmentEntranceApplicants = () => import(/* webpackChunkName: "dept-entrance-applicants" */ '@/components/department/DepartmentEntranceApplicants.vue')
const DepartmentRevisions = () => import(/* webpackChunkName: "dept-revisions" */ '@/components/department/DepartmentRevisions.vue')
// ----------------- guidance page ---------------------------------------------------------------------
const GuidanceTabs = () => import(/* webpackChunkName: "guidance-tabs" */ '@/components/guidance/GuidanceTabs.vue')
// ----------------- finance page ---------------------------------------------------------------------
const FinanceTabs = () => import(/* webpackChunkName: "finance-tabs" */ '@/components/finance/FinanceTabs.vue')
const FinanceTuitionFee = () => import(/* webpackChunkName: "finance-tuition-fee" */ '@/components/finance/fees/FinanceTuitionFee.vue')
const FinanceFee = () => import(/* webpackChunkName: "finance-fee" */ '@/components/finance/fees/FinanceFee.vue')
const FinanceDiscount = () => import(/* webpackChunkName: "finance-discount" */ '@/components/finance/FinanceDiscount.vue')
const FinanceAccountCodes = () => import(/* webpackChunkName: "finance-account" */ '@/components/finance/FinanceAccountCodes.vue')
const FinanceAccountHistory = () => import(/* webpackChunkName: "finance-account-history" */ '@/components/finance/FinanceAccountHistory.vue')
const FinancePrograms = () => import(/* webpackChunkName: "finance-programs" */ '@/components/finance/tuition/FinancePrograms.vue')
const FinanceSchoolYear = () => import(/* webpackChunkName: "finance-school-year" */ '@/components/finance/tuition/FinanceSchoolYear.vue')
// ------------------ broken page ------------------------------------------------------------
const ComingSoon = () => import(/* webpackChunkName: "coming-soon" */ '@/components/ComingSoon')
const PageNotFound = () => import(/* webpackChunkName: "page-not-found" */ '@/components/PageNotFound')
const PageUnderMaintenance = () => import(/* webpackChunkName: "page-under-maintenance" */ '@/components/PageUnderMaintenance')

export function createRouter () {
  const routes = [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      component: SignUp,
      props: true
    },
    {
      path: '/forgot-password',
      name: 'forgot-password',
      component: ForgotPassword
    },
    {
      path: '/reset',
      name: 'reset',
      component: Reset
    },
    {
      path: '/reset-form/:tokenId',
      name: 'reset-form',
      component: ResetForm
    },
    {
      path: '/home',
      component: AdminPage,
      props: true,
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'MIS' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      children: [
        {
          path: '/',
          name: 'mis-dashboard',
          component: MisDashboard
        },
        {
          path: 'students',
          component: AdminStudents,
          children: [
            {
              path: '',
              name: 'admin-stud-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'admin-stud-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'admin-stud-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'admin-stud-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'admin-stud-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'program',
          name: 'program',
          component: NavProgram
        },
        {
          path: 'program-curriculum',
          name: 'program-curriculum',
          component: ProgramCurriculum
        },
        {
          path: 'program-curriculum-revisions',
          name: 'program-curriculum-revisions',
          component: Revisions
        },
        {
          path: 'courses',
          name: 'courses-menu',
          component: CourseMenu
        },
        {
          path: 'applicants',
          component: AdminApplicants,
          props: true,
          children: [
            {
              path: '/',
              name: 'admin-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'admin-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'admin-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'section',
              name: 'admin-section-filter',
              component: SectionFilter
            },
            {
              path: 'semester',
              name: 'admin-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'admin-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'entrance',
          component: AdminApplicantsEntranceExam,
          props: true,
          children: [
            {
              path: '/',
              name: 'admin-entrance-academic-filter',
              component: AcademicYear
            },
            {
              path: 'entrance-program',
              name: 'admin-entrance-program-filter',
              component: Program
            },
            {
              path: 'entrance-year-level',
              name: 'admin-entrance-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'entrance-status',
              name: 'admin-entrance-status-filter',
              component: StatusFilter
            },
            {
              path: 'entrance-exam-status',
              name: 'admin-entrance-exam-status-filter',
              component: EntranceStatus
            }
          ]
        },
        {
          path: 'user',
          component: User,
          children: [
            {
              path: '/',
              name: 'position-filter',
              component: PositionFilter
            }
          ]
        },
        {
          path: 'class',
          component: Class,
          children: [
            {
              path: '/',
              name: 'class-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'class-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'class-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'class-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'class-code',
              name: 'class-code',
              component: ClassCodeFilter
            },
            {
              path: 'course',
              name: 'course-filter',
              component: CourseFilter
            },
            {
              path: 'unit',
              name: 'unit-filter',
              component: UnitFilter
            },
            {
              path: 'time',
              name: 'time-filter',
              component: TimeFilter
            },
            {
              path: 'day',
              name: 'day-filter',
              component: DayFilter
            },
            {
              path: 'room',
              name: 'room-filter',
              component: RoomFilter
            },
            {
              path: 'section',
              name: 'section-filter',
              component: SectionFilter
            },
            {
              path: 'instructor',
              name: 'instructor-filter',
              component: InstructorFilter
            }
          ]
        },
        {
          path: 'sections',
          component: SectionTable,
          children: [
            {
              path: '/',
              name: 'section-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'section-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'section-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'section',
              name: 'class-sections-filter',
              component: SectionFilter
            }
          ]
        },
        {
          path: 'rooms',
          component: RoomTable,
          children: [
            {
              path: '/',
              name: 'rooms-year-filter',
              component: AcademicYear
            },
            {
              path: 'semester',
              name: 'rooms-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'room',
              name: 'class-room-filter',
              component: RoomFilter
            }
          ]
        },
        {
          path: 'teacher',
          component: TeacherTable,
          children: [
            {
              path: '/',
              name: 'class-teacher-year-filter',
              component: AcademicYear
            },
            {
              path: 'semester',
              name: 'class-teacher-semester-filter',
              component: SemesterFilter
            }
          ]
        },
        {
          path: 'admin-exam-schedule',
          component: ExamSchedule,
          props: true,
          children: [
            {
              path: '/',
              name: 'exam-year-filter',
              component: AcademicYear
            }
          ]
        },
        {
          path: 'academic-school-year',
          component: SchoolYear,
          children: [
            {
              path: '/',
              name: 'academic-year',
              component: AcademicYear
            },
            {
              path: 'program-filter',
              name: 'program-filter',
              component: Program
            }
          ]
        },
        {
          path: 'curriculum-course',
          component: Course,
          children: [
            {
              path: '/',
              name: 'course-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'course-semester-filter',
              component: SemesterFilter
            },
          ]
        },
        {
          path: ':type/enrollment-form',
          name: 'admin-applicant-form',
          component: AdminTabs,
          props: true
        },
        {
          path: ':type/enrollment-form',
          name: 'admin-student-form',
          component: AdminTabs,
          props: true
        },
        {
          path: 'my-profile',
          name: 'my-admin-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/admission',
      component: AdmissionPage,
      props: true,
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'Admission' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/',
          component: AdmissionApplicants,
          props: true,
          children: [
            {
              path: '/',
              name: 'admission-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'admission-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'admission-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'admission-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'section',
              name: 'admission-section-filter',
              component: SectionFilter
            },
            {
              path: 'status',
              name: 'admission-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'students',
          component: AdmissionStudents,
          children: [
            {
              path: '/',
              name: 'admission-stud-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'admission-stud-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'admission-stud-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'admission-stud-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'admission-stud-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: ':type/enrollment-form',
          name: 'admission-applicant-form',
          component: AdmissionTabs,
          props: true
        },
        {
          path: ':type/enrollment-form',
          name: 'admission-student-form',
          component: AdmissionTabs,
          props: true
        },
        {
          path: 'my-profile',
          name: 'my-admission-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/applicant',
      component: ApplicantPage,
      props: true,
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'Applicant' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: 'view-curriculum',
          name: 'view-curriculum',
          component: ViewCurriculum
        },
        {
          path: 'enroll-online',
          name: 'enroll-online',
          component: Enrollment
        },
        {
          path: 'enrollment-form/:status',
          component: EnrollmentForm,
          children: [
            {
              path: '/',
              name: 'application-info',
              component: ApplicationInfo
            },
            {
              path: 'personal-info',
              name: 'personal-info',
              component: PersonalInfo
            },
            {
              path: 'educational-info',
              name: 'educational-info',
              component: EducationalInfo
            },
            {
              path: 'family-info',
              name: 'family-info',
              component: FamilyInfo
            },
            {
              path: 'requirements',
              name: 'requirements',
              component: Requirements
            },
            {
              path: 'requirements',
              name: 'transferee-requirement',
              component: Requirements
            },
            {
              path: 'courses',
              name: 'courses',
              component: Courses
            },
            {
              path: 'payment',
              name: 'payment',
              component: Payment
            },
            {
              path: 'payment',
              name: 'entrance-payment',
              component: Payment
            }
          ]
        },
        {
          path: 'my-profile',
          name: 'my-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/clinic',
      component: ClinicPage,
      props: true,
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'College Clinic' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/',
          component: ClinicApplicants,
          children: [
            {
              path: '/',
              name: 'clinic-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'clinic-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'clinic-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'clinic-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'section',
              name: 'clinic-section-filter',
              component: SectionFilter
            },
            {
              path: 'status',
              name: 'clinic-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'students',
          component: ClinicStudents,
          children: [
            {
              path: '/',
              name: 'clinic-stud-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'clinic-stud-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'clinic-stud-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'clinic-stud-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'clinic-stud-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: ':type/enrollment-form',
          name: 'clinic-applicant-form',
          component: ClinicTabs,
          props: true
        },
        {
          path: ':type/enrollment-form',
          name: 'clinic-student-form',
          component: ClinicTabs,
          props: true
        },
        {
          path: 'my-profile',
          name: 'my-clinic-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/department',
      component: DepartmentPage,
      props: true,
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'Dean' && global._.trim(status) === 'Active' || global._.trim(user) === 'Assistant Dean' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/',
          component: DepartmentApplicants,
          children: [
            {
              path: '/',
              name: 'deans-year-filter',
              component: AcademicYear
            },
            {
              path: 'year-level',
              name: 'deans-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'deans-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'deans-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'deans-entrance',
          component: DepartmentEntranceApplicants,
          props: true,
          children: [
            {
              path: '/',
              name: 'deans-entrance-academic-filter',
              component: AcademicYear
            },
            {
              path: 'entrance-program',
              name: 'deans-entrance-program-filter',
              component: Program
            },
            {
              path: 'entrance-year-level',
              name: 'deans-entrance-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'entrance-status',
              name: 'deans-entrance-status-filter',
              component: StatusFilter
            },
            {
              path: 'entrance-exam-status',
              name: 'deans-entrance-exam-status-filter',
              component: EntranceStatus
            }
          ]
        },
        {
          path: 'students',
          component: DepartmentStudents,
          children: [
            {
              path: '/',
              name: 'deans-stud-year-filter',
              component: AcademicYear
            },
            {
              path: 'year-level',
              name: 'deans-stud-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'deans-stud-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'deans-stud-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: ':type/enrollment-form',
          name: 'dean-applicant-form',
          component: DepartmentTabs,
          props: true
        },
        {
          path: ':type/enrollment-form',
          name: 'dean-student-form',
          component: DepartmentTabs,
          props: true
        },
        {
          path: 'my-profile',
          name: 'my-deans-profile',
          component: Profile
        },
        {
          path: 'curriculum',
          name: 'department-academic-year',
          component: DepartmentCurriculum
        },
        {
          path: 'department-curriculum-course',
          component: DepartmentCourse,
          children: [
            {
              path: '/',
              name: 'department-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'curriculum-semester',
              name: 'department-semester-filter',
              component: SemesterFilter
            }
          ]
        },
        {
          path: 'department-revisions',
          name: 'department-revisions',
          component: DepartmentRevisions
        }
      ]
    },
    {
      path: '/finance',
      component: FinancePage,
      props: true,
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'Finance' && global._.trim(status) === 'Active') {
          // next({ name: 'page-under-maintenance' })
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/',
          component: FinanceApplicants,
          children: [
            {
              path: '/',
              name: 'finance-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'finance-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'finance-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'finance-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'section',
              name: 'finance-section-filter',
              component: SectionFilter
            },
            {
              path: 'status',
              name: 'finance-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'finance-entrance',
          component: FinanceEntranceApplicants,
          props: true,
          children: [
            {
              path: '/',
              name: 'finance-entrance-academic-filter',
              component: AcademicYear
            },
            {
              path: 'entrance-program',
              name: 'finance-entrance-program-filter',
              component: Program
            },
            {
              path: 'entrance-year-level',
              name: 'finance-entrance-status-filter',
              component: StatusFilter
            },
            {
              path: 'entrance-exam-status',
              name: 'finance-entrance-exam-status-filter',
              component: EntranceStatus
            }
          ]
        },
        {
          path: 'students',
          component: FinanceStudents,
          children: [
            {
              path: '/',
              name: 'finance-stud-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'finance-stud-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'finance-stud-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'finance-stud-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'finance-stud-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'tuition-and-miscellaneous',
          component: FinanceTuitionFee,
          children: [
            {
              path: '/',
              name: 'finance-tuition-and-misc-type-filter',
              component: FeeTypes
            }
          ]
        },
        {
          path: 'course-fees',
          component: FinanceFee,
          children: [
            {
              path: '/',
              name: 'finance-fee-type-filter',
              component: FeeTypes
            }
          ]
        },
        {
          path: 'discount',
          component: FinanceDiscount,
          children: [
            {
              path: '/',
              name: 'finance-discount-types-filter',
              component: DiscountTypes
            }
          ]
        },
        {
          path: 'account-codes',
          component: FinanceAccountCodes,
          props: true,
          children: [
            {
              path: '/',
              name: 'finance-account-type-filter',
              component: AccountTypes
            }
          ]
        },
        {
          path: 'account-history',
          component: FinanceAccountHistory,
          props: true,
          children: [
            {
              path: '/',
              name: 'finance-account-history-year-filter',
              component: AcademicYear
            },
            {
              path: 'semester',
              name: 'finance-account-history-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'date-applied',
              name: 'finance-date-applied-filter',
              component: DateApplied
            }
          ]
        },
        {
          path: ':type/enrollment-form',
          name: 'finance-applicant-form',
          component: FinanceTabs,
          props: true
        },
        {
          path: ':type/enrollment-form',
          component: FinanceTabs,
          props: true,
          children: [
            {
              path: '/',
              name: 'finance-payment-year-filter',
              component: AcademicYear
            },
            {
              path: 'semester',
              name: 'finance-payment-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'discount',
              name: 'finance-discount-tab-types-filter',
              component: DiscountTypes
            },
            {
              path: 'payment-mode/:mode',
              name: 'finance-payment-mode-filter',
              component: PaymentMode
            }
          ]
        },
        {
          path: 'finance-programs',
          name: 'finance-programs',
          component: FinancePrograms
        },
        {
          path: 'finance-programs-school-year',
          name: 'finance-programs-school-year',
          component: FinanceSchoolYear
        },
        {
          path: 'my-profile',
          name: 'my-finance-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/guidance',
      component: GuidancePage,
      props: true,
      meta: {
        requiresAuth: true
      },
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'Guidance Center' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      children: [
        {
          path: '/',
          component: GuidanceApplicants,
          children: [
            {
              path: '/',
              name: 'guidance-enroll-academic-filter',
              component: AcademicYear
            },
            {
              path: 'applicant-program',
              name: 'guidance-enroll-program-filter',
              component: Program
            },
            {
              path: 'applicant-year-level',
              name: 'guidance-enroll-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'applicant-semester',
              name: 'guidance-enroll-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'applicant-section',
              name: 'guidance-section-filter',
              component: SectionFilter
            },
            {
              path: 'applicant-status',
              name: 'guidance-enroll-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'guidance-entrance',
          component: GuidanceEntranceApplicants,
          props: true,
          children: [
            {
              path: '/',
              name: 'guidance-entrance-academic-filter',
              component: AcademicYear
            },
            {
              path: 'entrance-program',
              name: 'guidance-entrance-program-filter',
              component: Program
            },
            {
              path: 'entrance-year-level',
              name: 'guidance-entrance-status-filter',
              component: StatusFilter
            },
            {
              path: 'entrance-exam-status',
              name: 'guidance-entrance-exam-status-filter',
              component: EntranceStatus
            }
          ]
        },
        {
          path: 'students',
          component: GuidanceStudents,
          children: [
            {
              path: '/',
              name: 'guidance-student-academic-filter',
              component: AcademicYear
            },
            {
              path: 'students-program',
              name: 'guidance-student-program-filter',
              component: Program
            },
            {
              path: 'students-year-level',
              name: 'guidance-student-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'guidance-student-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'section',
              name: 'guidance-student-filter',
              component: SectionFilter
            },
            {
              path: 'students-status',
              name: 'guidance-student-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: ':type/enrollment-form',
          name: 'guidance-applicant-form',
          component: GuidanceTabs,
          props: true
        },
        {
          path: ':type/enrollment-form',
          name: 'guidance-student-form',
          component: GuidanceTabs,
          props: true
        },
        {
          path: 'my-profile',
          name: 'my-guidance-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/registrar',
      component: RegistrarPage,
      props: true,
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'Registrar' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/',
          component: RegistrarApplicants,
          children: [
            {
              path: '/',
              name: 'registrar-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'registrar-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'registrar-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'registrar-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'section',
              name: 'registrar-section-filter',
              component: SectionFilter
            },
            {
              path: 'status',
              name: 'registrar-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: ':type/enrollment-form',
          name: 'registrar-applicant-form',
          component: RegistrarTabs,
          props: true
        },
        {
          path: 'students',
          component: RegistrarStudents,
          children: [
            {
              path: '/',
              name: 'registrar-stud-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'registrar-stud-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'registrar-stud-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'registrar-stud-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'registrar-stud-status-filter',
              component: StatusFilter
            }
          ]
        },
        {
          path: 'class',
          component: RegistrarClass,
          children: [
            {
              path: '/',
              name: 'registrar-class-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'registrar-class-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'registrar-class-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'registrar-class-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'class-code',
              name: 'registrar-class-code',
              component: ClassCodeFilter
            },
            {
              path: 'course',
              name: 'registrar-class-course-filter',
              component: CourseFilter
            },
            {
              path: 'unit',
              name: 'registrar-class-unit-filter',
              component: UnitFilter
            },
            {
              path: 'time',
              name: 'registrar-class-time-filter',
              component: TimeFilter
            },
            {
              path: 'day',
              name: 'registrar-class-day-filter',
              component: DayFilter
            },
            {
              path: 'room',
              name: 'registrar-class-room-filter',
              component: RoomFilter
            },
            {
              path: 'section',
              name: 'registrar-class-section-filter',
              component: SectionFilter
            },
            {
              path: 'instructor',
              name: 'registrar-class-instructor-filter',
              component: InstructorFilter
            }
          ]
        },
        {
          path: 'sections',
          component: RegistrarSectionTable,
          children: [
            {
              path: '/',
              name: 'registrar-section-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'registrar-section-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'registrar-section-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'registrar-section-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'section',
              name: 'registrar-class-sections-filter',
              component: SectionFilter
            }
          ]
        },
        {
          path: 'rooms',
          component: RegistrarRoomTable,
          children: [
            {
              path: '/',
              name: 'registrar-rooms-year-filter',
              component: AcademicYear
            },
            {
              path: 'semester',
              name: 'registrar-rooms-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'room',
              name: 'registrar-room-filter',
              component: RoomFilter
            }
          ]
        },
        {
          path: 'teacher',
          component: RegistrarTeacherTable,
          children: [
            {
              path: '/',
              name: 'registrar-class-teacher-year-filter',
              component: AcademicYear
            },
            {
              path: 'semester',
              name: 'registrar-class-teacher-semester-filter',
              component: SemesterFilter
            }
          ]
        },
        {
          path: ':type/enrollment-form',
          name: 'registrar-student-form',
          component: RegistrarTabs,
          props: true
        },
        {
          path: 'exam-schedule',
          component: ExamSchedule,
          props: true,
          children: [
            {
              path: '/',
              name: 'registrar-exam-year-filter',
              component: AcademicYear
            }
          ]
        },
        {
          path: 'my-profile',
          name: 'my-registrar-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/student',
      component: StudentPage,
      props: true,
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'Student' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/',
          name: 'student-enroll-online',
          component: EnrollOnline
        },
        {
          path: 'enroll-online',
          component: StudentEnrollmentForm,
          children: [
            {
              path: '/',
              name: 'student-application-info',
              component: StudentApplicationInfo
            },
            {
              path: 'personal-info',
              name: 'student-personal-info',
              component: StudentPersonalInfo
            },
            {
              path: 'educational-info',
              name: 'student-educational-info',
              component: StudentEducationalInfo
            },
            {
              path: 'family-info',
              name: 'student-family-info',
              component: StudentFamilyInfo
            },
            {
              path: 'requirements',
              name: 'student-requirements',
              component: StudentRequirements
            },
            {
              path: 'courses',
              name: 'student-courses',
              component: StudentCourseForm
            },
            {
              path: 'payment',
              name: 'student-payment',
              component: StudentPayment
            }
          ]
        },
        {
          path: 'adding-dropping',
          name: 'adding-dropping',
          component: StudentCourses
        },
        {
          path: 'my-grades',
          name: 'my-grades',
          component: ComingSoon
          // children: [
          //   {
          //     path: '/',
          //     name: 'student-grade-year-filter',
          //     component: AcademicYear
          //   },
          //   {
          //     path: 'semester',
          //     name: 'student-grade-semester-filter',
          //     component: SemesterFilter
          //   }
          // ]
        },
        {
          path: 'class-schedule',
          component: StudentSchedule,
          children: [
            {
              path: '/',
              name: 'student-class-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'student-class-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'student-class-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'student-class-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'class-code',
              name: 'student-class-code',
              component: ClassCodeFilter
            }
          ]
        },
        {
          path: 'deficiencies',
          name: 'deficiencies',
          component: ComingSoon
        },
        {
          path: 'account-balance',
          name: 'account-balance',
          component: ComingSoon,
          // children: [
          //   {
          //     path: '/',
          //     name: 'student-balance-year-filter',
          //     component: AcademicYear
          //   },
          //   {
          //     path: 'semester',
          //     name: 'student-balance-semester-filter',
          //     component: AcademicYear
          //   }
          // ]
        },
        {
          path: 'my-profile',
          name: 'my-stud-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/teacher',
      component: TeacherPage,
      props: true,
      meta: {
        requiresAuth: true
      },
      beforeEnter (to, from, next) {
        const store = createStore()
        const user = store.state.user.user.user_type
        const status = store.state.user.user.status
        if (global._.trim(user) === 'Teacher' && global._.trim(status) === 'Active') {
          next()
        } else {
          next({ name: 'page-not-found' })
        }
      },
      children: [
        {
          path: '/',
          component: ClassList,
          children: [
            {
              path: '/',
              name: 'teacher-year-filter',
              component: AcademicYear
            },
            {
              path: 'program',
              name: 'teacher-program-filter',
              component: Program
            },
            {
              path: 'year-level',
              name: 'teacher-level-filter',
              component: YearLevelFilter
            },
            {
              path: 'semester',
              name: 'teacher-semester-filter',
              component: SemesterFilter
            },
            {
              path: 'status',
              name: 'teacher-status-filter',
              component: StatusFilter
            },
            {
              path: 'class-code',
              name: 'teacher-class-code',
              component: ClassCodeFilter
            }
          ]
        },
        {
          path: 'class-record/Students',
          name: 'class-record-students',
          component: ClassListStudents
        },
        {
          path: 'my-profile',
          name: 'my-teacher-profile',
          component: Profile
        }
      ]
    },
    {
      path: '/page-not-found',
      name: 'page-not-found',
      component: PageNotFound
    },
    {
      path: '/page-under-maintenance',
      name: 'page-under-maintenance',
      component: PageUnderMaintenance
    }
  ]

  const router = new VueRouter({
    mode: 'history',
    routes
  })

  router.beforeEach((to, from, next) => {
    window.scrollTo(0, 0)
    const lastRouteName = localStorage.getItem('LS_ROUTE_KEY')
    const token = window.localStorage.getItem('apollo-token')
    if (!token && to.matched.some(m => m.meta.requiresAuth)) {
      // Redirect to login when unauthenticated user
      if (to.matched.some(m => m.meta.requiresAuth)) {
        return next({ name: 'login' })
      }
    } else {
      if (to.name) {
        if (to.name === 'login' && token) next({ name: lastRouteName })
        next()
      } else {
        next({name: 'page-not-found' })
      }
    }
  })

  router.afterEach(to => {
    localStorage.setItem('LS_ROUTE_KEY', to.name)
  })

  return router
}
