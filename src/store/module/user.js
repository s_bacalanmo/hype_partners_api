import { getApi, asyncPostApi,  asyncDeleteApi } from '@/http'

const initialState = () => {
  return {
    student_data: {},
    user: {},
    registrar: {},
    prospectus: {},
    active: null,
    clear_item: ''
  }
}

const state = initialState()

const getters = {
  student_data: state => state.student_data,
  user: state => state.user,
  registrar: state => state.registrar,
  prospectus: state => state.prospectus,
  active: state => state.active,
  clear_item: state => state.clear_item
}

const mutations = {
  setUserDP: (state, payload) => {
    state.user.avatar = payload.avatar
    state.user.update = payload.update
  },
  setUser: (state, payload) => {
    state.user = payload
  },
  setStudentData: (state, payload) => {
    state.student_data = payload
  },
  setRegistrar: (state, payload) => {
    state.registrar = payload
  },
  setProspectus: (state, payload) => {
    state.prospectus = payload
  },
  setClearItem: (state, payload) => {
    state.clear_item = payload
  },
  SET_ACTIVE: (state, payload) => {
    state.active = payload
  },
  RESET_USER (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
  async LOGIN ({ commit }, payload) {
    const response = await asyncPostApi(`api/login`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_USER ({ commit }, payload) {
    const response = await asyncPostApi('api/create/user', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async UPDATE_DP ({ commit }, payload) {
    const response = await asyncPostApi('api/admin/update/user/profileImage', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_USER ({ commit }, payload) {
    const response = await asyncPostApi('api/admin/update/user/account', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async UPDATE_PASS ({ commit }, payload) {
    const response = await asyncPostApi('api/admin/update/user/password', payload)
    return response
  },
  GET_CARDS ({ commit }, payload) {
    const response = getApi('api/admin/fetch/user/cards', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_USER ({ commit }, id) {
    const response = await asyncPostApi(`api/admin/delete/user/${id}`)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async FORGOT_PASSWORD ({ commit }, payload) {
    const response = await asyncPostApi('api/forgot/password/', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw err.response.data
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async RESET_PASSWORD ({ commit }, payload) {
    const response = await asyncPostApi('api/reset/password/', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  }
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
