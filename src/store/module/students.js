import { asyncPostApi, asyncDeleteApi } from '@/http'

const initialState = () => {
  return {
    refetchedData: {},
    enroll_student: {},
    classData: {},
    curriculum: {},
    revision: {}
  }
}

const state = initialState()

const getters = {
  refetchedData: state => state.refetchedData,
  enroll_student: state => state.enroll_student,
  classData: state => state.classData,
  curriculum: state => state.curriculum,
  revision: state => state.revision
}

const mutations = {
  setRefetchedData: (state, payload) => {
    state.refetchedData = payload
  },
  setEnrollStudent: (state, payload) => {
    state.enroll_student = payload
  },
  setClassData: (state, payload) => {
    state.classData = payload
  },
  setCurriculum: (state, payload) => {
    state.curriculum = payload
  },
  setRevision: (state, payload) => {
    state.revision = payload
  },
  RESET_STUDENTS (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
  async ENROLL_STUDENT ({ commit }, payload) {
    const response = await asyncPostApi('api/applicant/submit/application/enrollment/student', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_STUDENT_PERSONAL ({ commit }, payload) {
    const response = await asyncPostApi('api/applicant/update/create/SpdiPersonalInfo', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_STUDENT_EDUCATION ({ commit }, payload) {
    const response = await asyncPostApi('api/applicant/update/create/SpdiEducationalInfo', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_STUDENT_FAMILY ({ commit }, payload) {
    const response = await asyncPostApi('api/applicant/update/create/SpdiFamilyInfo', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_STUDENT ({ commit }, payload) {
    const response = await asyncPostApi(`api/student/update`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_STUDENT_GRADE ({ commit }, payload) {
    const response = await asyncPostApi(`api/student/create/grade`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_STUDENT_GRADE ({ commit }, payload) {
    const response = await asyncDeleteApi(`api/student/delete/grade/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_CLASS_RECORD ({ commit }, payload) {
    const response = await asyncPostApi(`api/student/submit/teacher_grade`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_ASSESSMENT ({ commit }, payload) {
    const response = await asyncPostApi(`api/finance/create/student/assessment`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_STUDENT_GRADE_BY_ADMIN ({ commit }, payload) {
    const response = await asyncPostApi(`api/student/create/evaluation`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  }
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
