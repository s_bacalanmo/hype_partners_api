import { asyncPostApi, deleteApi } from '@/http'

const initialState = () => {
  return {
    student_applicant: {},
    education: {},
    personal: {},
    family: {},
    requirements: {},
    payment: {},
    courses: {},
    program_name: '',
    semester: '',
    status: '',
    isTitled: '',
    enrollData: {},
    submissionType: '',
    entranceItem: {}
  }
}

const state = initialState()

const getters = {
  student_applicant: state => state.student_applicant,
  education: state => state.education,
  personal: state => state.personal,
  family: state => state.family,
  requirements: state => state.requirements,
  payment: state => state.payment,
  courses: state => state.courses,
  program_name: state => state.program_name,
  semester: state => state.semester,
  status: state => state.status,
  isTitled: state => state.isTitled,
  enrollData: state => state.enrollData,
  submissionType: state => state.submissionType,
  entranceItem: state => state.entranceItem
}

const mutations = {
  setStudentApplicant: (state, payload) => {
    state.student_applicant = payload
  },
  setEducation: (state, payload) => {
    state.education = payload
  },
  setPersonal: (state, payload) => {
    state.personal = payload
  },
  setFamily: (state, payload) => {
    state.family = payload
  },
  setRequirements: (state, payload) => {
    state.requirements = payload
  },
  setPayment: (state, payload) => {
    state.payment = payload
  },
  setCourses: (state, payload) => {
    state.courses = payload
  },
  setProgram: (state, payload) => {
    state.program_name = payload
  },
  setSemester: (state, payload) => {
    state.semester = payload
  },
  setStatus: (state, payload) => {
    state.status = payload
  },
  setTitle: (state, payload) => {
    state.isTitled = payload
  },
  setEnrollData: (state, payload) => {
    state.enrollData = payload
  },
  setSubmissionType: (state, payload) => {
    state.submissionType = payload
  },
  SET_ENTRANCE_ITEM: (state, payload) => {
    state.entranceItem = payload
  },
  RESET_APPLICANTS (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
  async MAKE_PAYMENT ({commit}, payload) {
    const response = await asyncPostApi('api/applicant/payment', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_EMAIL ({ commit }, payload) {
    const response = await asyncPostApi('api/email/entrance/exam/applicant', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_ENROLLMENT_SUBMISSION ({ commit }, payload) {
    const response = await asyncPostApi('api/applicant/submit/application/enrollment', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_SECTION_ASSIGNING ({ commit }, payload) {
    const response = await asyncPostApi('api/admin/assign/section', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_DROP_CLASS ({ commit }, payload) {
    const response = await asyncPostApi(`api/student/dropping/schedule/${payload.class_code}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_ADD_CLASS ({ commit }, payload) {
    const response = await asyncPostApi(`api/student/adding/schedule`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_APPROVALS ({ commit }, payload) {
    const response = await asyncPostApi(`api/admin/applicant/evaluate`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_ENROLEES ({ commit }, payload) {
    const response = await asyncPostApi(`api/admin/assign/enroll`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_REQUIREMENTS ({ commit }, payload) {
    const response = await asyncPostApi(`api/applicant/upload/requirement`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_APPLICATION_INFO ({ commit }, payload) {
    const response = await asyncPostApi(`api/applicant/enroll`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  }
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
