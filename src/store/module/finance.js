import { asyncPostApi } from '@/http'
const initialState = () => {
  return {
  }
}

const state = initialState()

const getters = {
}

const mutations = {
}

const actions = {
  async POST_PAYMENT ({ commit }, payload) {
    const response = await asyncPostApi(`api/finance/create/student/${payload.mode}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_PAYMENT ({ commit }, payload) {
    const response = await asyncPostApi(`api/finance/update/student/${payload.mode}/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_PAYMENT ({ commit }, payload) {
    const response = await asyncDeleteApi(`api/finance/delete/student/${payload.mode}/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  }
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
