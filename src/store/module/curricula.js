import { asyncPostApi, asyncDeleteApi } from '@/http'

const initialState = () => {
  return {
    curricula: {},
    curriculum_fcns: '',
    per_program: {},
    revisions: {},
    old_sections: {}
  }
}

const state = initialState()

const getters = {
  curricula: state => state.curricula,
  curriculum_fcns: state => state.curriculum_fcns,
  per_program: state => state.per_program,
  revisions: state => state.revisions,
  old_sections: state => state.old_sections
}

const mutations = {
  SET_FUNC: (state, payload) => {
    state.curriculum_fcns = payload
  },
  SET_CURRICULA: (state, payload) => {
    state.curricula = payload
  },
  SET_PER_PROGRAM: (state, payload) => {
    state.per_program = payload
  },
  SET_REVISIONS: (state, payload) => {
    state.revisions = payload
  },
  SET_OLD_SECTIONS: (state, payload) => {
    state.old_sections = payload
  },
  RESET_CURRICULA (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
  async POST_CLASS ({ commit }, payload) {
    const response = await asyncPostApi('api/class/create/now', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_CLASS ({ commit }, payload) {
    const response = await asyncPostApi(`api/class/create/${global._.trim(payload.class_code)}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_CLASS ({ commit }, payload) {
    const response = await asyncDeleteApi(`api/class/deleteclass/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_CLASS_CODE ({ commit }, payload) {
    const response = await asyncPostApi(`api/class/delete/${payload.class_code}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_COURSE_EQUIVALENCY ({ commit }, payload) {
    const response = await asyncPostApi(`api/student/create/se`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_COURSE_EQUIVALENCY ({ commit }, payload) {
    const response = await asyncDeleteApi(`api/student/delete/se/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_ASSIGN_PROGRAM ({ commit }, payload) {
    const response = await asyncPostApi(`api/curricula/create/program/curriculum`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_TEACHERS_GRADING ({ commit }, payload) {
    const response = await asyncPostApi(`api/imports/excel/student_grading/${payload.curriculum_id}/${payload.semester_id}/${payload.teacher_id}/${payload.class_code}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_COURSE_DETAILS ({ commit }, payload) {
    const response = await asyncPostApi('api/curricula/create/obtdetails', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_COURSE_DETAILS ({ commit }, payload) {
    const response = await asyncPostApi('api/curricula/update/obtdetails', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_COURSE_DETAILS ({ commit }, payload) {
    const response = await asyncDeleteApi(`api/curricula/delete/obtdetails/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_COURSE_NOTES ({ commit }, payload) {
    const response = await asyncPostApi('api/curricula/create/obtnotes', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_COURSE_NOTES ({ commit }, payload) {
    const response = await asyncPostApi('api/curricula/update/obtnotes', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_COURSE_NOTES ({ commit }, payload) {
    const response = await asyncDeleteApi(`api/curricula/delete/obtnotes/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async CLOSE_PROGRAM ({ commit }, payload) {
    const response = await asyncDeleteApi(`api/curricula/delete/sy/program/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async CLONE_COURSES ({ commit }, payload) {
    const response = await asyncPostApi('api/curricula/course/revision', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_STUDENT_DISCOUNT ({ commit }, payload) {
    const response = await asyncPostApi('api/finance/create/student/discount', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_STUDENT_DISCOUNT ({ commit }, payload) {
    const response = await asyncPostApi('api/finance/update/student/discount', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_STUDENT_DISCOUNT ({ commit }, payload) {
    const response = await asyncPostApi(`api/finance/delete/student/discount/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async POST_STUDENT_CHARGES ({ commit }, payload) {
    const response = await asyncPostApi('api/finance/create/student/charges', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_STUDENT_CHARGES ({ commit }, payload) {
    const response = await asyncPostApi('api/finance/update/student/charges', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async DELETE_STUDENT_CHARGES ({ commit }, payload) {
    const response = await asyncDeleteApi(`api/finance/delete/student/charges/${payload.id}`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_STUDENT_CURRICULUM ({ commit }, payload) {
    const response = await asyncPostApi(`api/applicant/update/curriculum/revision`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async PATCH_STUDENT_REVISION ({ commit }, payload) {
    const response = await asyncPostApi(`api/applicant/update/revision`, payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
