import { getApi } from '@/http'
const initialState = () => {
  return {
  }
}

const state = initialState()

const getters = {
}

const mutations = {
  RESET_EXPORTS (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
  EXPORT_CURRICULUM: ({ commit }, payload) => {
    const url = `api/excel/curriculum_prospectus/${payload.effective_school_year_from}/${payload.effective_school_year_to}/${payload.program_id}`
    return getApi(url)
  }
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
