import { asyncPostApi } from '@/http'
const initialState = () => {
  return {
  }
}

const state = initialState()

const getters = {
}

const mutations = {
}

const actions = {
  async INCREASE_PERCENTAGE ({ commit }, payload) {
    const response = await asyncPostApi('api/finance/increase/amount', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  },
  async COPY_FEES ({ commit }, payload) {
    const response = await asyncPostApi('api/finance/copy/sy', payload)
    .catch(err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`)
      }
      if (err.response.status === 422) {
        throw err.response.data
      }
      throw err.response.data
    })
    return response
  }
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
