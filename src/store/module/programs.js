import { asyncPostApi, asyncDeleteApi } from '@/http'

const initialState = () => {
  return {
    programs: {},
    semester: {}
  }
}

const state = initialState()

const getters = {
  programs: state => state.programs,
  semester: state => state.semester
}

const mutations = {
  SET_PROGRAMS: (state, payload) => {
    state.programs = payload
  },
  SET_SEMESTER: (state, payload) => {
    state.semester = payload
  }
}

const actions = {
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
