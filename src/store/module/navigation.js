
const initialState = () => {
  return {
    drawer: true
  }
}

const state = initialState()

const getters = {
  drawer: state => state.drawer
}

const mutations = {
  setDrawer: (state, payload) => {
    state.drawer = payload
  },
  RESET_NAVIGATION (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
