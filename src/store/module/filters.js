
const initialState = () => {
  return {
    academic_year_id: null,
    program_id: null,
    year_level_id: null,
    semester_id: null,
    room: [],
    course: [],
    units: [],
    teacher: [],
    class_code: [0, 0],
    time_from: '',
    time_to: '',
    day: [],
    section: [],
    status: [],
    position: [],
    search: null,
    email_status: '',
    date_applied: ['', ''],
    keyword_section: '',
    entrance_status: [],
    discount_type: [],
    payment_mode: '',
    fee_type_id: '',
    account_type: ''
  }
}

const state = initialState()

const getters = {
  academic_year_id: state => state.academic_year_id,
  program_id: state => state.program_id,
  year_level_id: state => state.year_level_id,
  semester_id: state => state.semester_id,
  room: state => state.room,
  course: state => state.course,
  units: state => state.units,
  teacher: state => state.teacher,
  class_code: state => state.class_code,
  time_from: state => state.time_from,
  time_to: state => state.time_to,
  day: state => state.day,
  section: state => state.section,
  status: state => state.status,
  position: state => state.position,
  search: state => state.search,
  email_status: state => state.email_status,
  date_applied: state => state.date_applied,
  keyword_section: state => state.keyword_section,
  entrance_status: state => state.entrance_status,
  discount_type: state => state.discount_type,
  payment_mode: state => state.payment_mode,
  fee_type_id: state => state.fee_type_id,
  account_type: state => state.account_type
}

const mutations = {
  SET_ACADEMIC_ID: (state, payload) => {
    state.academic_year_id = payload
  },
  SET_PROGRAM_ID: (state, payload) => {
    state.program_id = payload
  },
  SET_YEAR_LEVEL_ID: (state, payload) => {
    state.year_level_id = payload
  },
  SET_SEMESTER_ID: (state, payload) => {
    state.semester_id = payload
  },
  SET_ROOM: (state, payload) => {
    state.room = payload
  },
  SET_COURSE: (state, payload) => {
    state.course = payload
  },
  SET_UNITS: (state, payload) => {
    state.units = payload
  },
  SET_TEACHER: (state, payload) => {
    state.teacher = payload
  },
  SET_CLASS_CODE: (state, payload) => {
    state.class_code = payload
  },
  SET_TIME_FROM: (state, payload) => {
    state.time_from = payload
  },
  SET_TIME_TO: (state, payload) => {
    state.time_to = payload
  },
  SET_DAY: (state, payload) => {
    state.day = payload
  },
  SET_SECTION: (state, payload) => {
    state.section = payload
  },
  SET_STATUS: (state, payload) => {
    state.status = payload
  },
  SET_POSITION: (state, payload) => {
    state.position = payload
  },
  SET_SEARCH: (state, payload) => {
    state.search = payload
  },
  SET_EMAIL_STATUS: (state, payload) => {
    state.email_status = payload
  },
  SET_DATE_APPLIED: (state, payload) => {
    state.date_applied = payload
  },
  SET_KEYWORD_SECTION: (state, payload) => {
    state.keyword_section = payload
  },
  SET_ENTRANCE_STATUS: (state, payload) => {
    state.entrance_status = payload
  },
  SET_DISCOUNT_TYPE: (state, payload) => {
    state.discount_type = payload
  },
  SET_PAYMENT_MODE: (state, payload) => {
    state.payment_mode = payload
  },
  SET_FEE_TYPE: (state, payload) => {
    state.fee_type_id = payload
  },
  SET_ACCOUNT_TYPE: (state, payload) => {
    state.account_type = payload
  },
  RESET_FILTERS (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
