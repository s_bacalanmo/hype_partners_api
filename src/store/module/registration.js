const initialState = () => {
  return {
    entrance: {},
    enrollment: {},
    forgotPassword: {}
  }
}

const state = initialState()

const getters = {
  entrance: state => state.entrance,
  enrollment: state => state.enrollment,
  forgotPassword: state => state.forgotPassword
}

const mutations = {
  setEntrance: (state, payload) => {
    state.entrance = payload
  },
  setEnrollment: (state, payload) => {
    state.enrollment = payload
  },
  setForgotPassword: (state, payload) => {
    state.forgotPassword = payload
  },
  RESET_REGISTRATION (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
