import Vue from 'vue'

const initialState = () => {
  return {
    curriculum: {},
    program: {},
    icons: {
      personal_info: '',
      educational_info: '',
      family_info: '',
      payment: '',
      application_info: '',
      requirements: '',
      courses: '',
      transferee_requirement: '',
      entrance_payment: ''
    },
    student_icons: {
      student_application_info: '',
      student_personal_info: '',
      student_educational_info: '',
      student_family_info: '',
      student_requirements: '',
      student_courses: ''
    },
    isEntranceSubmit: false,
    isEnrollSubmit: false,
    isViewForm: true,
    course_count: 0,
    courseId: null,
    isViewChecked: false
  }
}

const state = initialState()

const getters = {
  curriculum: state => state.curriculum,
  program: state => state.program,
  icons: state => state.icons,
  isEntranceSubmit: state => state.isEntranceSubmit,
  isEnrollSubmit: state => state.isEnrollSubmit,
  isViewForm: state => state.isViewForm,
  course_count: state => state.course_count,
  courseId: state => state.courseId,
  student_icons: state => state.student_icons,
  isViewChecked: state => state.isViewChecked
}

const mutations = {
  setCurriculum: (state, payload) => {
    state.curriculum = payload
  },
  setProgram: (state, payload) => {
    state.program = payload
  },
  setIcons: (state, payload) => {
    Vue.set(state.icons, payload.key, payload.value)
  },
  setStudentIcons: (state, payload) => {
    Vue.set(state.student_icons, payload.key, payload.value)
  },
  setIsEntranceSubmit: (state, payload) => {
    state.isEntranceSubmit = payload
  },
  setIsEnrollSubmit: (state, payload) => {
    state.isEnrollSubmit = payload
  },
  setIsViewForm: (state, payload) => {
    state.isViewForm = payload
  },
  setCourseCount: (state, payload) => {
    state.course_count = payload
  },
  setCourseId: (state, payload) => {
    state.courseId = payload
  },
  setIsViewChecked: (state, payload) => {
    state.isViewChecked = payload
  },
  RESET_FORM (state) {
    Object.assign(state, initialState())
  }
}

const actions = {
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
