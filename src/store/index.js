import Vue from 'vue'
import Vuex from 'vuex'
// store vuex in local storage
import applicants from './module/applicants'
import filters from './module/filters'
import navigation from './module/navigation'
import user from './module/user'
import fee from './module/fee'
import finance from './module/finance'
import form from './module/form'
import exports from './module/exports'
import registration from './module/registration'
import curricula from './module/curricula'
import programs from './module/programs'
import students from './module/students'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    modules: {
      applicants,
      filters,
      navigation,
      user,
      form,
      fee,
      finance,
      exports,
      curricula,
      programs,
      registration,
      students
    },
    plugins: [createPersistedState()]
  })
}
