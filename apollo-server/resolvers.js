import shortid from 'shortid'

export default {

  Query: {
    login: (root, args, { db }) => db.get('login').value(),
    forgotPass: (root, args, { db }) => db.get('forgotPass').value(),
    forgotPass: (root, args, { db }) => db.get('forgotPass').value()
  },

  Mutation: {
    addUser: (root, { input }, { pubsub, db }) => {
      const payload = {
        id: shortid.generate(),
        text: input.text,
        first_name: input.first_name,
        last_name: input.last_name,
        middle_name: input.middle_name,
        date_of_birth: input.date_of_birth,
        gender: input.gender,
        email_address: input.email_address,
        password: input.password,
        confirm_password: input.confirm_password
      }

      db
        .get('user')
        .push(payload)
        .last()
        .write()

      return payload
    },

    updateImage: (root, { input }, { db }) => {
      const payload = {
        id: shortid.generate(),
        file: input.file,
        user_id: input.user_id
      }
      db
        .get('user')
        .push(payload)
        .last()
        .write()

      return payload
    },
    createCurriculum: (root, { input }, { db }) => {
      const payload = {
        program_id: input.program_id,
        effective_school_year_from: input.effective_school_year_from,
        effective_school_year_to: input.effective_school_year_to,
        scheme: input.scheme
      }
      db
        .push(payload)
        .last()
        .write()

      return payload
    }
  }
}
